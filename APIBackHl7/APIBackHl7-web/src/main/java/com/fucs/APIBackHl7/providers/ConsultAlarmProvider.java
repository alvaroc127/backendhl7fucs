package com.fucs.APIBackHl7.providers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hl7.fhir.r4.model.Observation;

import org.jboss.logging.Logger;

import com.fucs.APIBackHl7.dao.DAOSignal;

import com.fucs.commo.web.pojo.Signal;

import ca.uhn.fhir.rest.annotation.RequiredParam;

import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;

public class ConsultAlarmProvider implements IResourceProvider
{
	
	private static Logger log=Logger.getLogger(ConsultAlarmProvider.class);
	
	@Override
	public Class<Observation> getResourceType() {
		// TODO Auto-generated method stub
		return Observation.class;
	}
	
	@Search(type = Observation.class)
	public List<Observation> consultSignalTime(
			@RequiredParam(name=Observation.SP_IDENTIFIER) String iduser,
			@RequiredParam(name=Observation.SP_DATE)DateRangeParam range) 
	{
		log.debug("se recibe el id");
		log.debug("i " +iduser);
		DAOSignal daosignal=new DAOSignal();
		Signal frecresp=null;
		java.util.List<Signal> yellRed=null;
		java.util.List<Observation> out=new ArrayList<Observation>();
		Signal sp2=null;
		Signal ecg=null;
	
		
		
		Date inic=range.getLowerBoundAsInstant();
		Date fin=range.getUpperBoundAsInstant();
		
		Calendar calfin=Calendar.getInstance();
		calfin.setTimeInMillis(fin.getTime());
		
		
		Calendar calini=Calendar.getInstance();
		calini.setTimeInMillis(inic.getTime());
		
		calini.add(Calendar.MINUTE,-1);
		
		calfin.add(Calendar.SECOND,10);
		calini.add(Calendar.SECOND,-1);
		log.debug("fecha 1"+calini.toString());
		log.debug("fecha 2"+calfin.toString());
		try
		{
		frecresp=daosignal.consultaFrecResp(Integer.valueOf(iduser), calini, calfin);
		sp2=daosignal.consultaSPO2(Integer.valueOf(iduser), calini, calfin);
		yellRed=daosignal.consultaYellowRed(Integer.valueOf(iduser), calini, calfin);
		ecg=daosignal.consultaAlrm_ECG(Integer.valueOf(iduser), calini, calfin);
		}catch (Exception e) 
		{
			log.debug("ocurrio un error interno",e);
		}
		if(null!=frecresp && null != sp2 && null != yellRed) 
		{
			out.add(frecresp.getObservation());
			out.add(sp2.getObservation());
			for (Signal sig:yellRed) 
			{
			out.add(sig.getObservation());
			}
			out.add(ecg.getObservation());
		}
		if(out.isEmpty()) 
		{
			throw new ResourceNotFoundException("no se encontraron registros sorry men");
		                
		 }
		return out;
	}
	
	

	

}
