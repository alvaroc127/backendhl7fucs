package com.fucs.APIBackHl7.providers;

import java.util.Calendar;
import java.util.Date;

import org.hl7.fhir.instance.model.api.IBaseResource;

import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.DateRangeParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;

import org.hl7.fhir.r4.model.Condition;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.SampledData;
import org.jboss.logging.Logger;

import com.fucs.APIBackHl7.dao.DAOAlarm;
import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.Alertas;
import com.fucs.commo.web.util.Constans;

public class ConditionResource implements IResourceProvider {

	@Override
	public Class<? extends IBaseResource> getResourceType() {
		// TODO Auto-generated method stub
		return Condition.class;
	}
	
	private static Logger log=Logger.getLogger(ConditionResource.class);
	
	
	@Search(type = Condition.class)
	public java.util.List<Condition> consultAlerts(
			@RequiredParam(name=Observation.SP_IDENTIFIER) String iduser,
			@RequiredParam(name=Observation.SP_DATE)DateRangeParam range)
	{
		log.debug("se procede a consultar las alertas del idusu "+iduser);
		java.util.List<Condition> alarms=new java.util.ArrayList<Condition>();
		log.debug(" inicio de consulta en base de datos");
		DAOAlarm dao=new DAOAlarm();
		Date inic=range.getLowerBoundAsInstant();
		Date fin=range.getUpperBoundAsInstant();
		
		Calendar calfin=Calendar.getInstance();
		calfin.setTimeInMillis(fin.getTime());
		
		
		Calendar calini=Calendar.getInstance();
		calini.setTimeInMillis(inic.getTime());
		calini.add(Calendar.MINUTE, -5);//validar configurar propiedades
		calfin.add(Calendar.SECOND, 10);
		log.debug(calini);
		log.debug(calfin);
		try 
		{
		java.util.List<Alertas> alrs=dao.consultAlerts(Integer.parseInt(iduser), calini, calfin);
			for(Alertas al:alrs) 
			{
				Condition con=new Condition();
				SampledData samdescrip=new SampledData();
				SampledData samdhora=new SampledData();
				SampledData samdseveridad=new SampledData();
				con.setId(iduser);
				samdescrip.setData(al.getDescripcion());
				con.addEvidence().addExtension().setValue(samdescrip);
				samdhora.setData(al.getHoraAlerta());
				con.addEvidence().addExtension().setValue(samdhora);
				samdseveridad.setData(String.valueOf(al.getSeveridad()));
				con.addEvidence().addExtension().setValue(samdseveridad);
				alarms.add(con);
			}
		}catch (Exception e) 
		{
			log.debug("error en el consult alerts",e);
			PercistenExcepcion per=new PercistenExcepcion();
			per.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			per.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw new ResourceNotFoundException("algo salio mal");
		}
		if(alarms.isEmpty()) 
		{
			throw new ResourceNotFoundException("no se encontraron registros sorry men");
		                
		 }
		return alarms;
	}

}
