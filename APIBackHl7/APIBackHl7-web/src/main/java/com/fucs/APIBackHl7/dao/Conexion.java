package com.fucs.APIBackHl7.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.jboss.logging.Logger;

import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.rosource.ResourceInternal;
import com.fucs.commo.web.util.Constans;

public class Conexion implements Serializable
{
	
	
	private static final Logger logger = Logger.getLogger(Conexion.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = -2956065104798679060L;

	
	
	public static Connection getConnectionDefault()
	throws PercistenExcepcion 
	{
		logger.debug("inici solicitando conexion");
		ResourceInternal resource=null;
		Connection con=null;
		resource=ResourceInternal.getInstance();
		if(null==resource)
		{
			logger.debug("no se puedo instanciar el resources");
			PercistenExcepcion perx=new PercistenExcepcion();
			perx.setCodigo(Constans.ERROR.ERROR_NEGOCION_CONFIG.getCodigo());
			perx.setMensaje(Constans.ERROR.ERROR_NEGOCION_CONFIG.getMensaje());
			throw perx;
		}
		String jndi=resource.getProperty(Constans.KEY_BASE, Constans.KEY_BASE_JNDI_DEFAULT);
		try 
		{
		InitialContext init=new InitialContext();
		if(null!=init) 
			{
			DataSource ds=(DataSource)init.lookup(jndi);
			con= ds.getConnection();
			}
		}catch (Exception e) 
		{
			PercistenExcepcion pers=new PercistenExcepcion();
			pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw pers;
		}
		return con;
	}
	
	
	
	public static Connection getConnectionfucs()
			throws PercistenExcepcion
	{
			ResourceInternal resource=null;
			Connection con=null;
			resource=ResourceInternal.getInstance();
			logger.debug(" esta solicitando el jndi ");
			if(null==resource)
			{
				PercistenExcepcion perx=new PercistenExcepcion();
				perx.setCodigo(Constans.ERROR.ERROR_NEGOCION_CONFIG.getCodigo());
				perx.setMensaje(Constans.ERROR.ERROR_NEGOCION_CONFIG.getMensaje());
				throw perx;
			}
			String jndi=resource.getProperty(Constans.KEY_BASE, Constans.KEY_BASE_JNDI_DEFAULT);
			logger.debug(" solicita ste jndi "+jndi);
			try 
			{
			InitialContext init=new InitialContext();
			if(null!=init) 
				{
				logger.debug(" el init no es null ");
				DataSource ds=(DataSource)init.lookup(jndi);
				con= ds.getConnection();
				}
			}catch (Exception e) 
			{
				logger.debug("el error en la conexion ");
				logger.debug("el error en la conexion ",e);
				PercistenExcepcion pers=new PercistenExcepcion();
				pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
				pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
				throw pers;
			}
			return con;
	}
	
	
	
	public static void close(PreparedStatement stat) 
	{
		if(null == stat ) {return;}
		try 
		{
			stat.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static void close(ResultSet stat) 
	{
		if(null == stat ) {return;}
		try 
		{
			stat.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static void close(Connection stat) 
	{
		if(null == stat ) {return;}
		try 
		{
			stat.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	

}
