package com.fucs.APIBackHl7.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Util 
{
	
	
	
	public static String byteArrToString(byte[] arr) 
	{
		if(null==arr) return null;
		StringBuilder bud=new StringBuilder();
		for(byte a:arr) 
		{
			bud.append(Byte.toUnsignedInt(a))
			.append(' ');
		}
		return bud.toString();
	}
	
	
	public static String getDateInString(Calendar date) 
	{
		java.util.Date dat=new java.util.Date(date.getTimeInMillis());
		SimpleDateFormat simpl=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		return simpl.format(dat);
	}


}
