package com.fucs.APIBackHl7.servlet;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import org.jboss.logging.Logger;

import com.fucs.APIBackHl7.dao.DAOPermiso;
import com.fucs.APIBackHl7.dao.DAORol;
import com.fucs.APIBackHl7.dao.DAOTipoDocumento;
import com.fucs.APIBackHl7.dao.DAOUsuario;
import com.fucs.APIBackHl7.jms.interfac.PublisherJmsAlertasRemote;
import com.fucs.APIBackHl7.providers.AlarmProvider;
import com.fucs.APIBackHl7.providers.ConditionResource;
import com.fucs.APIBackHl7.providers.ConsultAlarmProvider;
import com.fucs.commo.web.rosource.ResourceInternal;
import com.fucs.commo.web.util.Constans;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.server.HardcodedServerAddressStrategy;
import ca.uhn.fhir.rest.server.RestfulServer;

@WebServlet("/*")
public class RestServlet  extends RestfulServer
{

	
	/**
	 * 
	 */
	@EJB(lookup = "ejb:APIBackHl7/APIBackHl7-ejb/PublisherJmsAlertas!com.fucs.APIBackHl7.jms.interfac.PublisherJmsAlertasRemote")
	private PublisherJmsAlertasRemote publish;
	
	private static final long serialVersionUID = 8276594707038850173L;
	
	private ResourceInternal resour;
	
	private static final Logger logger = Logger.getLogger(RestServlet.class);
	
	
	public RestServlet() 
	{
		super();
		try 
		{
		DAOTipoDocumento.getInstance();
		DAORol.getInstance();
		DAOPermiso.getDAOPermiso();
		}catch (Exception e) 
		{
			logger.debug("error en la carga de base de datos",e);
			// TODO: handle exception
		}
		
	}
	
	
	@Override
	protected void initialize() throws ServletException 
	{
	   this.resour=ResourceInternal.getInstance();
	   /*String serverBaseUrl = resour.getProperty(Constans.KEY_BEAN ,Constans.LOCALBASEURL);
	   setServerAddressStrategy(new HardcodedServerAddressStrategy(serverBaseUrl));*/
		
		
		
		setFhirContext(FhirContext.forR4());
		registerProvider(new AlarmProvider(publish));
		
		registerProvider(new ConsultAlarmProvider());
		registerProvider(new ConditionResource());
	
	}

}
