package com.fucs.APIBackHl7.providers;




import java.util.ArrayList;

import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Patient;
import org.jboss.logging.Logger;

import com.fucs.APIBackHl7.dao.DAOMonitor;
import com.fucs.APIBackHl7.dao.DAOUsuario;
import com.fucs.APIBackHl7.jms.interfac.PublisherJmsAlertasRemote;
import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.Usuario;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.annotation.Operation;
import ca.uhn.fhir.rest.annotation.OperationParam;
import ca.uhn.fhir.rest.annotation.ResourceParam;

public class AlarmProvider 
{
	
	
	private PublisherJmsAlertasRemote publish;
	
	private static Logger log=Logger.getLogger(AlarmProvider.class);
	
	public AlarmProvider() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	 public AlarmProvider(PublisherJmsAlertasRemote publish) {
		super();
		this.publish = publish;
	}




	@Operation(name="$process-message")
	   public Bundle loadMessage
	   (
	    @ResourceParam Bundle bund,
	    @OperationParam(name="asyn") String asyn,
		@OperationParam(name="response-url")String urlres) 
	   {
		   log.debug("se da inicio de la carga de mensaje");
		   FhirContext contex=FhirContext.forR4();
		   boolean a=false;
		   boolean cutfor=false;
		   java.util.ArrayList<BundleEntryComponent> com=(ArrayList<Bundle.BundleEntryComponent>)bund.getEntry();
		   a=null == com?false:true;
		   String ip=null;
		   Integer idUsu=null;
		   for(int i=0;a&&i<com.size()&&!cutfor;i++) 
		   {
			   BundleEntryComponent bun=com.get(i);
			if (bun.getResource() instanceof Patient) 
			{
				Patient pat=(Patient)bun.getResource();
				ip=pat.getName().get(1).getFamily();
				cutfor=true;
			}      
		   }
		   DAOMonitor daom=new DAOMonitor();
		   DAOUsuario daou=new DAOUsuario();
		  try 
		  {
			  java.util.Map<String,Integer>mon=daom.findMonitor(ip);
			  a=null==mon?false:true;
			  if(a) {idUsu=daom.findMontBind(mon.get(ip));}
			  a=null==idUsu?false:true;
			  cutfor=false;
			  if(a) 
			  {
				  Usuario us=daou.findbyID(idUsu);
				  for(int i=0;a&&i<com.size()&&!cutfor;i++) 
				   {
					   BundleEntryComponent bun=com.get(i);
					if (bun.getResource() instanceof Patient) 
					{
						Patient pat=(Patient)bun.getResource();
						pat.setId(String.valueOf(us.getIdusu()));
						pat.addName(new HumanName().setFamily(us.getTipoDoc().getIdtipdoc()+us.getNumerodoc()));
						bun.setResource(pat);
						com.set(1, bun);
						cutfor=true;
					}      
				   }
				  
			  }
			  
		  }catch (Exception e) 
		  {
			  log.debug("error con sultando el monitor",e);
			if(e instanceof PercistenExcepcion) 
			{
				log.debug("error en bd " + ((PercistenExcepcion) e).getCodigo());
				log.debug("error en bd " + ((PercistenExcepcion) e).getMensaje());
			}
		  }
		   
		   //antes de convertir
		   //buscar el cliente vinculado al monitor y cargar el objeto paciente
		   if(a)
		   {

			  try	 
		   		{
			   IParser parser=(IParser) contex.newJsonParser();
			   String mees=parser.setPrettyPrint(true).encodeResourceToString(bund);
			   log.debug("se envia el mensaje "+mees);
			   publish.publisMessage(mees);
		   		}
			  catch (Exception e) 
			  {
				  log.debug("se genero un error publicando el mensaje ",e);
			  }
		 }
		   return bund;
	   }

	
	
}
