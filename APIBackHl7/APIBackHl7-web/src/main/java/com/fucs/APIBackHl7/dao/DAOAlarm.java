package com.fucs.APIBackHl7.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.hl7.fhir.r4.model.SampledData;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Observation.ObservationComponentComponent;
import org.jboss.logging.Logger;

import com.fucs.APIBackHl7.util.Util;
import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.Alertas;
import com.fucs.commo.web.util.Constans;

public class DAOAlarm extends BaseDAO 
{

	private static final Logger logger = Logger.getLogger(DAOAlarm.class);
	
	public DAOAlarm() 
	{
		// TODO Auto-generated constructor stub
	}
	
	
	public java.util.List<Alertas> consultAlerts(Integer idUsuario,java.util.Calendar inidate,java.util.Calendar findate)
	throws PercistenExcepcion
	{
		Connection con=Conexion.getConnectionfucs();
		PreparedStatement stat=null;
		ResultSet rs=null;
		java.util.List<Alertas> alerts=new java.util.ArrayList<Alertas>(); 
		try {
			stat=con.prepareStatement(DAOSignal.CONSULTALARMECG);
			stat.setInt(1, idUsuario);
			stat.setString(2,Util.getDateInString(inidate));
			stat.setString(3,Util.getDateInString(findate));
			rs=stat.executeQuery();
			while(rs.next()) 
			{
				Alertas alrt=new Alertas();
				Observation obs=new Observation();
				SampledData samdesc=new SampledData();
				SampledData samsevri=new SampledData();
				SampledData samhora=new SampledData();
				
				ObservationComponentComponent comdesc=new ObservationComponentComponent();
				ObservationComponentComponent comserv=new ObservationComponentComponent();
				ObservationComponentComponent comhoralarm=new ObservationComponentComponent();
				
				comdesc.setId("descripcion");
				comdesc.setValue(samdesc.setData(String.valueOf(rs.getString("descripcion"))));
				alrt.setDescripcion(samdesc.getData());
				
				comserv.setId("severidad");
				comserv.setValue(samsevri.setData(String.valueOf(rs.getString("severidad"))));
				alrt.setSeveridad(Integer.valueOf(samsevri.getData()));
				
				comhoralarm.setId("hora_alarm");
				comhoralarm.setValue(samhora.setData(rs.getString("HoraAlarma")));
				alrt.setHoraAlerta(samhora.getData());
				
				obs=obs.addComponent(comdesc);
				obs=obs.addComponent(comserv);
				obs=obs.addComponent(comhoralarm);
				alrt.setObservation(obs);
				alerts.add(alrt);
			}
			return alerts;			

		}catch (Exception e) 
		{
			logger.debug(" se genero el error sobre la excepcion",e);
			PercistenExcepcion per=new PercistenExcepcion();
			per.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			per.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw per;
		}finally 
		{
			this.close(rs);
			this.close(stat);
			this.close(con);
		}
		
	}
	
	


}
