package com.fucs.APIBackHl7.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.fucs.commo.web.excepcion.PercistenExcepcion;

public class BaseDAO 
{
	
	
	public BaseDAO() 
	{
		
	}
	
	
	public Connection getConection(Integer source)
	throws PercistenExcepcion
	{
		Connection con=null;
		switch (source)
		{
		case 1:
			con = Conexion.getConnectionfucs();
		break;
		
		case 2:
			con =Conexion.getConnectionDefault();
		break;
		
		default:
			con=null;
		break;
			
		}
		return con;
	}
	
	
	public void close (ResultSet rs) 
	{
		
		Conexion.close(rs);
	}
	public void close (PreparedStatement rs) 
	{
		Conexion.close(rs);
	}
	public void close (Connection rs) 
	{
		Conexion.close(rs);		
	}

}
