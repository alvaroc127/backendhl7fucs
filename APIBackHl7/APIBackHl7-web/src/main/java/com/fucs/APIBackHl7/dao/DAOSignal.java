package com.fucs.APIBackHl7.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.concurrent.Semaphore;

import javax.enterprise.inject.Stereotype;

import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Observation.ObservationComponentComponent;
import org.hl7.fhir.r4.model.SampledData;
import org.jboss.logging.Logger;

import com.fucs.APIBackHl7.util.Util;
import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.Alertas;
import com.fucs.commo.web.pojo.SenialRojaAmarilla;
import com.fucs.commo.web.pojo.Signal;
import com.fucs.commo.web.util.Constans;

public class DAOSignal extends BaseDAO
{
	
	private static final Logger logger = Logger.getLogger(DAOSignal.class);
	
	private static final String CONSULTFRERESP=
		" SELECT fr.impedancia ,fr.Senal,fr.id_signal,fr.ID  FROM dbo.Frec_Respiratoria fr  " +
		" INNER JOIN dbo.USUARIOXMONITOR uxm  ON fr.id =  uxm.ID  "+
		" WHERE uxm.ID_USU  = ? " +
		" AND fr.HoraSenal >= ? AND fr.HoraSenal <= ? ";
	
	private static final String CONSULTSPO2=
		" SELECT s.desconocido, s.frecuencia, s.SenalSPO2,s.id_signal,s.id FROM dbo.SPO2 s  "+
		" INNER JOIN dbo.USUARIOXMONITOR uxm  ON s.id =  uxm.ID  " +
		" WHERE uxm.ID_USU  = ? " +
		" AND s.HoraSenal >= ? AND s.HoraSenal <= ? ";
	
	private static final String CONSULTYELLRED=
		" SELECT sra.Maximo ,sra.minimo ,sra.parentesis, sra.Senal,sra.TipoSenal, sra.id_signal, sra.ID FROM dbo.Senal_Roja_Amarilla sra "+
		" INNER JOIN dbo.USUARIOXMONITOR uxm ON sra.ID =uxm.ID  "+
		" WHERE uxm.ID_USU  = ? "+
		" AND sra.HoraSenal >= ? AND sra.HoraSenal <= ? ";
	
	
	public static final String CONSULTALARMECG=
			" SELECT a.Descripcion,a.severidad,a.aVF ,a.aVl ,a.aVR, a.CVPs ,a.Frec_Cardi,a.I,a.II,a.III,a.V,a.ECG1, "+
			" a.ECG2 ,a.ECG3,a.Frec_Cardi,a.HoraAlarma   " +
			" FROM dbo.Alarm a " +
			" INNER JOIN dbo.USUARIOXMONITOR uxm  ON a.id =  uxm.ID   "+
			" WHERE uxm.ID_USU  = ?   " +
			" AND a.HoraAlarma >= ? AND a.HoraAlarma <= ? ";
	
	
	public DAOSignal() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
	public Signal consultaFrecResp(Integer idUsuario,java.util.Calendar inidate,java.util.Calendar findate) 
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.FUCSCONECT);
		PreparedStatement stat=null;
		ResultSet rs=null;
		Signal sig=null;
		Observation obser=null;
		boolean flagcut=false;
		logger.debug("se da inicio a la consulta ");
		logger.debug("se da inicio a la consulta "+Util.getDateInString(inidate));
		logger.debug("se da inicio a la consulta "+Util.getDateInString(findate));
		try 
		{
		stat=con.prepareCall(CONSULTFRERESP);
		stat.setInt(1, idUsuario);
		stat.setString(2,Util.getDateInString(inidate));
		stat.setString(3,Util.getDateInString(findate));
		logger.debug(" hola hola la fecha iniciop" + Util.getDateInString(inidate));
		logger.debug(" hola hola la fecha fin " + Util.getDateInString(findate));
		rs=stat.executeQuery();
		while(rs.next()&&!flagcut) 
		{
			logger.debug("se da se encontraront registros");
			sig=new Signal();
			obser=new Observation();
			SampledData sem=new SampledData();
			SampledData sem1=new SampledData();
			Integer idSignal=rs.getInt("id_signal");
			Integer idmon=rs.getInt("ID");
			
			byte[]arr=rs.getBytes("Senal");
			sem.setData(Util.byteArrToString(arr));
			Double val= rs.getDouble("impedancia");
			ObservationComponentComponent comp=new ObservationComponentComponent();
			ObservationComponentComponent compidMon=new ObservationComponentComponent();
			SampledData semidmon=new SampledData();
			
			
			
			sem1.setData(String.valueOf(val));
			comp.setId("impedancia");
			comp.setValue(sem1);
			obser.addComponent(comp);
			obser.setValue(sem.setData(Util.byteArrToString(arr)));
			obser.setId(String.valueOf(idSignal));
			semidmon.setData(String.valueOf(idmon));
			compidMon.setValue(semidmon);
			compidMon.setId("idmon");
			obser.addComponent(compidMon);
			sig.setObservation(obser);
			flagcut=true;
		}
			
		}catch (Exception e) 
		{
			logger.debug("otro error mas ",e);
		}
		this.close(rs);
		this.close(stat);
		this.close(con);
		return sig;
	}

	
	
	public java.util.List<Signal> consultaYellowRed(Integer idUsuario,java.util.Calendar inidate,java.util.Calendar findate) 
			throws PercistenExcepcion
			{
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				ResultSet rs=null;
				SenialRojaAmarilla sig=null;
				java.util.List<Signal> signals=new java.util.ArrayList<>();
				Observation obser=null;
				boolean flagcut=false;
				logger.debug("se da inicio a la consulta ");
				try 
				{
				stat=con.prepareCall(CONSULTYELLRED);
				stat.setInt(1, idUsuario);
				stat.setString(2,Util.getDateInString(inidate));
				stat.setString(3,Util.getDateInString(findate));
				rs=stat.executeQuery();
				logger.debug(" hola hola la fecha iniciop" + Util.getDateInString(inidate));
				logger.debug(" hola hola la fecha fin " + Util.getDateInString(findate));
				while(rs.next()) 
				{ 
					sig=new SenialRojaAmarilla();
					obser=new Observation();
					SampledData signal=new SampledData();
					SampledData maxsem=new SampledData();
					SampledData minsem=new SampledData();
					SampledData paresem=new SampledData();
					SampledData idmonsem=new SampledData();
					SampledData tipoSigsem=new SampledData();
					Double max=rs.getDouble("Maximo");
					Double min=rs.getDouble("minimo");
					Double parente=rs.getDouble("parentesis");
					byte[]arr=rs.getBytes("Senal");
					Integer tipo=rs.getInt("TipoSenal");
					Integer idmon=rs.getInt("ID");
					Integer idSignal=rs.getInt("id_signal");
					
					maxsem.setData(String.valueOf(max));
					minsem.setData(String.valueOf(min));
					paresem.setData(String.valueOf(parente));
					signal.setData(Util.byteArrToString(arr));
					idmonsem.setData(String.valueOf(idmon));
					tipoSigsem.setData(String.valueOf(tipo));
					
					
					ObservationComponentComponent comp=new ObservationComponentComponent();
					ObservationComponentComponent comp1=new ObservationComponentComponent();
					ObservationComponentComponent comp2=new ObservationComponentComponent();
					ObservationComponentComponent compmon=new ObservationComponentComponent();
					ObservationComponentComponent comptiposig=new ObservationComponentComponent();
					
				
					comp.setId("Maximo");
					comp.setValue(maxsem);
					comp1.setId("minimo");
					comp1.setValue(minsem);
					comp2.setId("parentesis");
					comp2.setValue(paresem);
					compmon.setId("idmon");
					compmon.setValue(idmonsem);
					comptiposig.setId("tiposignal");
					comptiposig.setValue(tipoSigsem);
					
					
					obser=obser.addComponent(comp);
					obser=obser.addComponent(comp1);
					obser=obser.addComponent(comp2);
					obser=obser.addComponent(compmon);
					obser=obser.addComponent(comptiposig);
					obser.setValue(signal.setData(Util.byteArrToString(arr)));
					obser.setId(String.valueOf(idSignal));
					sig.setObservation(obser);
					sig.setTipoSignal(tipo);
					signals.add(sig);
				}
					
				}catch (Exception e) 
				{
					logger.debug("otro error mas ",e);
				}
				this.close(rs);
				this.close(stat);
				this.close(con);
				return signals;
			}
	
	
	public Signal consultaSPO2(Integer idUsuario,java.util.Calendar inidate,java.util.Calendar findate) 
			throws PercistenExcepcion
			{
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				ResultSet rs=null;
				SenialRojaAmarilla sig=null;
				Observation obser=null;
				boolean flagcut=false;
				logger.debug("se da inicio a la consulta ");
				try 
				{
				stat=con.prepareCall(CONSULTSPO2);
				stat.setInt(1, idUsuario);
				stat.setString(2,Util.getDateInString(inidate));
				stat.setString(3,Util.getDateInString(findate));
				rs=stat.executeQuery();
				logger.debug(" hola hola la fecha iniciop" + Util.getDateInString(inidate));
				logger.debug(" hola hola la fecha fin " + Util.getDateInString(findate));
				while(rs.next()&&!flagcut) 
				{ 
					sig=new SenialRojaAmarilla();
					obser=new Observation();
					SampledData signal=new SampledData();
					SampledData descsem=new SampledData();
					SampledData frecsem=new SampledData();
					SampledData obsermosem=new SampledData();

					Double desc=rs.getDouble("desconocido");
					Double frec=rs.getDouble("frecuencia");
					byte[]arr=rs.getBytes("SenalSPO2");
					Integer idmon=rs.getInt("id");
					Integer idSignal=rs.getInt("id_signal");

					
					descsem.setData(String.valueOf(desc));
					frecsem.setData(String.valueOf(frec));
					signal.setData(Util.byteArrToString(arr));
					obsermosem.setData(String.valueOf(idmon));
					
					
					
					ObservationComponentComponent comp=new ObservationComponentComponent();
					ObservationComponentComponent comp1=new ObservationComponentComponent();
					ObservationComponentComponent obsermon=new ObservationComponentComponent();

					comp.setId("desconocido");
					comp.setValue(descsem);
					comp1.setId("frecuencia");
					comp1.setValue(frecsem);
					obsermon.setId("idmon");
					obsermon.setValue(obsermosem);
					
					obser=obser.addComponent(comp);
					obser=obser.addComponent(comp1);
					obser=obser.addComponent(obsermon);
					obser.setValue(signal.setData(Util.byteArrToString(arr)));
					obser.setId(String.valueOf(idSignal));
					sig.setObservation(obser);
					flagcut=true;
				}
					
				}catch (Exception e) 
				{
					logger.debug("otro error mas ",e);
				}
				this.close(rs);
				this.close(stat);
				this.close(con);
				return sig;
			}
	
	
	public Signal consultaAlrm_ECG(Integer idus,java.util.Calendar inidate,java.util.Calendar findate) 
	throws PercistenExcepcion
	{
		Connection con=null;
		PreparedStatement stat=null;
		ResultSet rs=null;
		logger.debug(" hola hola la fecha iniciop" + Util.getDateInString(inidate));
		logger.debug(" hola hola la fecha fin " + Util.getDateInString(findate));
		try 
		{
			con=this.getConection(Constans.FUCSCONECT);
			stat=con.prepareStatement(CONSULTALARMECG);
			Signal sig=null;
			Observation obs=null;
			stat.setInt(1, idus);
			stat.setString(2,Util.getDateInString(inidate));
			stat.setString(3,Util.getDateInString(findate));
			rs=stat.executeQuery();
			while(rs.next()) 
			{
				sig=new Alertas();
				obs=new Observation();
				obs.setId(String.valueOf(idus));
				SampledData samdesc=new SampledData();
				SampledData samsevri=new SampledData();
				SampledData samavf=new SampledData();
				SampledData samavl=new SampledData();
				SampledData samavr=new SampledData();
				SampledData samcvp=new SampledData();
				SampledData samfrecres=new SampledData();
				SampledData samI=new SampledData();
				SampledData samII=new SampledData();
				SampledData samIII=new SampledData();
				SampledData samV=new SampledData();
				SampledData samECG1=new SampledData();
				SampledData samECG2=new SampledData();
				SampledData samECG3=new SampledData();
				
				ObservationComponentComponent comdesc=new ObservationComponentComponent();
				ObservationComponentComponent comserv=new ObservationComponentComponent();
				ObservationComponentComponent comavf=new ObservationComponentComponent();
				ObservationComponentComponent comavl=new ObservationComponentComponent();
				ObservationComponentComponent comavr=new ObservationComponentComponent();
				ObservationComponentComponent comcvp=new ObservationComponentComponent();
				ObservationComponentComponent comfrecresp=new ObservationComponentComponent();
				ObservationComponentComponent comI=new ObservationComponentComponent();
				ObservationComponentComponent comII=new ObservationComponentComponent();
				ObservationComponentComponent comIII=new ObservationComponentComponent();
				ObservationComponentComponent commV=new ObservationComponentComponent();
				ObservationComponentComponent comECG1=new ObservationComponentComponent();
				ObservationComponentComponent comECG2=new ObservationComponentComponent();
				ObservationComponentComponent comECG3=new ObservationComponentComponent();
				
				
				comdesc.setId("descripcion");
				comdesc.setValue(samdesc.setData(String.valueOf(rs.getString("descripcion"))));
				
				comserv.setId("severidad");
				comserv.setValue(samsevri.setData(String.valueOf(rs.getString("severidad"))));
				
				
				
				comavf.setId("aVF");
				comavf.setValue(samavf.setData(String.valueOf(rs.getDouble("aVF"))));
				
				comavl.setId("aVl");
				comavl.setValue(samavl.setData(String.valueOf(rs.getDouble("aVl"))));
				
				
				comavr.setId("aVR");
				comavr.setValue(samavr.setData(String.valueOf(rs.getDouble("aVR"))));
				
				comcvp.setId("CVPs");
				comcvp.setValue(samcvp.setData(String.valueOf(rs.getDouble("CVPs"))));
				
				comI.setId("I");
				comI.setValue(samI.setData(String.valueOf(rs.getDouble("I"))));
				
				comII.setId("II");
				comII.setValue(samII.setData(String.valueOf(rs.getDouble("II"))));
				
				comIII.setId("III");
				comIII.setValue(samIII.setData(String.valueOf(rs.getDouble("III"))));
				
				commV.setId("V");
				commV.setValue(samV.setData(String.valueOf(rs.getDouble("V"))));
				
				byte[] ECG1=rs.getBytes("ECG1");
				comECG1.setId("ECG1");
				comECG1.setValue(samECG1.setData(Util.byteArrToString(ECG1)));
				
				byte[] ECG2=rs.getBytes("ECG2");
				comECG2.setId("ECG2");
				comECG2.setValue(samECG2.setData(Util.byteArrToString(ECG2)));
				
				byte[] ECG3=rs.getBytes("ECG3");
				comECG3.setId("ECG3");
				comECG3.setValue(samECG3.setData(Util.byteArrToString(ECG3)));
				
				
				comfrecresp.setId("Frec_Cardi");
				comfrecresp.setValue(samfrecres.setData(String.valueOf(rs.getDouble("Frec_Cardi"))));
				
				obs=obs.addComponent(comavf);
				obs=obs.addComponent(comavl);
				obs=obs.addComponent(comavr);
				obs=obs.addComponent(comcvp);
				obs=obs.addComponent(comI);
				obs=obs.addComponent(comII);
				obs=obs.addComponent(comIII);
				obs=obs.addComponent(commV);
				obs=obs.addComponent(comECG1);
				obs=obs.addComponent(comECG2);
				obs=obs.addComponent(comECG3);
				obs=obs.addComponent(comfrecresp);
				obs=obs.addComponent(comdesc);
				obs=obs.addComponent(comserv);
				sig.setObservation(obs);
			}
			return sig;
		}catch (Exception e) 
		{
			logger.debug("error en base de datos ",e);
			PercistenExcepcion per=new PercistenExcepcion();
			per.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			per.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw per;
		}finally 
		{
			this.close(rs);
			this.close(stat);
			this.close(con);
		}
	}
	
	

}
