package com.fucs.APIBackHl7.jms;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.jboss.logging.Logger;

import com.fucs.APIBackHl7.jms.interfac.ConsumerJmsQueueLocal;
import com.fucs.APIBackHl7.jms.interfac.ConsumerJmsQueueRemote;
import com.fucs.commo.web.excepcion.BussinesExcepcion;

/**
 * Session Bean implementation class ConsumerJmsQueue
 */
@Stateless
@LocalBean
public class ConsumerJmsQueue implements ConsumerJmsQueueRemote, ConsumerJmsQueueLocal 
{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 654443160919732459L;


	private static Logger log=Logger.getLogger(ConsumerJmsQueue.class);
	
	
	private CofigurationQueue config;
    /**
     * Default constructor. 
     */
    public ConsumerJmsQueue() 
    {
    	log.debug("se da inicio al jms de alertas");
		config=CofigurationQueue.getConfig();
		log.debug("finaliza la configuracion");
    }

	
	@Override
	public String consumeMessage() 
	throws BussinesExcepcion
	{
		String out=null;
		if(null!= config && config.isConfig()) 
		{
			log.debug("entre al publicador");
			Session alertsession=config.getSesionalertas();
			Queue colaAlertas=config.getQuealertas();
			try 
			{
			MessageConsumer consumer=alertsession.createConsumer(colaAlertas);
			config.getCon().start();
			TextMessage mess=(TextMessage)consumer.receive();
			out=mess.getText();
			config.getCon().stop();
			}catch (Exception e) 
			{	
				log.debug("se genero un error consumiendo la cola ");
				throw new BussinesExcepcion();
			}
		}	
		return out;
	}

}
