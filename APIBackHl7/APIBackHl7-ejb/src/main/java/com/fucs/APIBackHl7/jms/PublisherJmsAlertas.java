package com.fucs.APIBackHl7.jms;

import javax.ejb.Stateless;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.jboss.logging.Logger;
import com.fucs.APIBackHl7.jms.interfac.PublisherJmsAlertasLocal;
import com.fucs.APIBackHl7.jms.interfac.PublisherJmsAlertasRemote;
import com.fucs.commo.web.excepcion.BussinesExcepcion;

@Stateless
public class PublisherJmsAlertas implements PublisherJmsAlertasRemote, PublisherJmsAlertasLocal
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9034558390483094258L;

	private static Logger log=Logger.getLogger(PublisherJmsAlertas.class);

	CofigurationQueue config;
	
	
	
	
	public PublisherJmsAlertas() 
	{
		log.debug("se da inicio al jms de alertas");
		config=CofigurationQueue.getConfig();
		log.debug("finaliza la configuracion");
	}
	
	
	public void publisMessage(String message)
	throws BussinesExcepcion
	{
		config=CofigurationQueue.getConfig();
		log.debug(" entre al metodo pero el config es null");
		if(null!= config && config.isConfig()) 
		{
			log.debug("entre al publicador");
		Session alertsession=config.getSesionalertas();
		Queue colaAlertas=config.getQuealertas();
			try 
			{
			MessageProducer producer=alertsession.createProducer(colaAlertas);
			config.getCon().start();
			TextMessage messg=alertsession.createTextMessage(message);
			producer.send(messg);
			producer.close();
			//config.getCon().stop();
			//config.getSesionalertas().close();
			}
			catch (Exception e) 
			{
			log.debug("se genero una excepcion en negocilo ");
			log.debug("error ",e);
			this.config.close();
			}
		}
	}
	
	
	

}
