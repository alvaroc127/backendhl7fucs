package com.fucs.APIBackHl7.jms.interfac;


import java.io.Serializable;

import com.fucs.commo.web.excepcion.BussinesExcepcion;


public interface PublisherJmsAlertasInterfac  extends Serializable
{
	
	public void publisMessage(String message)
	throws BussinesExcepcion;

}
