package com.fucs.APIBackHl7.jms.interfac;

import java.io.Serializable;


import com.fucs.commo.web.excepcion.BussinesExcepcion;

public interface ConsumerJmsAlertas extends Serializable
{

	public String consumeMessage() throws BussinesExcepcion;
	

}
