package com.fucs.APIBackHl7.jms;

import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.jboss.logging.Logger;

import com.fucs.commo.web.rosource.ResourceInternal;
import com.fucs.commo.web.util.Constans;
public class CofigurationQueue 
{
	
	private static Logger log=Logger.getLogger(CofigurationQueue.class);
	
	private boolean isConfig;
	
	private Connection con;
	
	private Queue quealertas;
	
	private Session sesionalertas;
	
	private static CofigurationQueue miconfig;
	
	private ResourceInternal resor;
	
	
	
	
	
	private CofigurationQueue() 
	{
		try 
		{
		this.resor=ResourceInternal.getInstance();
		init();
		}catch (Exception e) 
		{
			log.debug("no se pudo cargar las clases JMS");
			log.debug("error ",e);
		}
	}
	
	
	public static CofigurationQueue getConfig() 
	{
		if(null==miconfig) 
		{
			miconfig=new CofigurationQueue();
		}
		return miconfig;
	}


	
	private void init() 
	throws NamingException,JMSException
	{
		log.debug("inicio de la configuracion ");
		Properties pros=new Properties();
		pros.put(Context.SECURITY_PRINCIPAL,resor.getProperty(Constans.KEY_BEAN,Constans.USERJMS));
		pros.put(Context.SECURITY_CREDENTIALS,resor.getProperty(Constans.KEY_BEAN,Constans.USERJMSPASS));
		InitialContext ini=new InitialContext(pros);
		ConnectionFactory cf=(ConnectionFactory)ini.lookup(resor.getProperty(Constans.KEY_BEAN,Constans.JMSCONFACT));
		this.con=cf.createConnection(resor.getProperty(Constans.KEY_BEAN,Constans.USERJMS), resor.getProperty(Constans.KEY_BEAN,Constans.USERJMSPASS));
		this.quealertas=(Queue) ini.lookup(resor.getProperty(Constans.KEY_BEAN,Constans.JMSQUEUEALER));
		this.sesionalertas=this.con.createSession(false, Session.AUTO_ACKNOWLEDGE);
		log.debug(" SE instanciron las sesiones. ");
	}



	public boolean isConfig() 
	{
		boolean a,b,c=false;
		a= null !=  con;
		log.debug(" connection ess "+ a);
		b= null !=  quealertas;
		log.debug(" connection ess "+ b);
		c = null != sesionalertas;
		log.debug(" session alertas "+ c);
		isConfig= a & b & c;
		return isConfig;
	}





	public void setConfig(boolean isConfig) {
		this.isConfig = isConfig;
	}





	public Connection getCon() {
		return con;
	}





	public void setCon(Connection con) {
		this.con = con;
	}





	public Queue getQuealertas() {
		return quealertas;
	}





	public void setQuealertas(Queue quealertas) {
		this.quealertas = quealertas;
	}





	public Session getSesionalertas() {
		return sesionalertas;
	}





	public void setSesionalertas(Session sesionalertas) {
		this.sesionalertas = sesionalertas;
	}
	
	
	public void close () 
	{
		try 
		{
		if(null != this.con)this.con.close();
		if(null != this.sesionalertas)this.con.close();
		}catch (Exception e) 
		{
			log.debug("ocurrio un error en el cierre de la cola ");
		}
	}
	
	
	

}
