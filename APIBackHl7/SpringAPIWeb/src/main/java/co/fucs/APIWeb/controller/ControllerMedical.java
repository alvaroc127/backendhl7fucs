package co.fucs.APIWeb.controller;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.pojo.Alertas;
import com.fucs.commo.web.pojo.AuditoriaObject;
import com.fucs.commo.web.pojo.Paciente;
import com.fucs.commo.web.pojo.Signal;
import com.fucs.commo.web.pojo.Usuario;
import com.fucs.commo.web.util.Constans;

import co.fucs.APIWeb.dto.DTOActiveMontUser;
import co.fucs.APIWeb.dto.DTOActiveUser;
import co.fucs.APIWeb.dto.DTOAlertas;
import co.fucs.APIWeb.dto.DTOAlertas.DTOAlert;
import co.fucs.APIWeb.dto.DTOBase;
import co.fucs.APIWeb.dto.DTOConsultSignalDateTime;
import co.fucs.APIWeb.dto.DTOMonitores;
import co.fucs.APIWeb.dto.DTOPatientSignal;
import co.fucs.APIWeb.dto.DTOUsers;
import co.fucs.APIWeb.services.AuditoriaServices;
import co.fucs.APIWeb.services.ServicePersistences;
import co.fucs.APIWeb.services.ServicesClient;
import co.fucs.APIWeb.util.exception.Util;

@CrossOrigin
@Controller
@RequestMapping(value = "medical" ,method = {RequestMethod.POST,RequestMethod.OPTIONS})
public class ControllerMedical 
{
	private static final Logger logger = LoggerFactory.getLogger(ControllerMedical.class);
	
	private AuditoriaServices audiserv;
	
	private ServicePersistences servpersis;
	
	private ServicesClient serviclient;
	
	public ControllerMedical() {
		// TODO Auto-generated constructor stub
	}
	
	
	@Autowired
	public ControllerMedical(AuditoriaServices audiser,ServicePersistences servpersis,ServicesClient serviclie) 
	{
		this.audiserv=audiser;
		this.servpersis=servpersis;
		this.serviclient=serviclie;
	}
	
	
	@RequestMapping(value = "bindusermon", method =  RequestMethod.POST)
	public ResponseEntity<DTOBase> bindMonitorUser
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
     @RequestBody(required = true) DTOActiveMontUser usermont,
     HttpServletRequest req)
	throws BussinesExcepcion
	{
		logger.debug("inicio de activacion del usuario");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("bindMontUs");
    	aub.setEventUsu(usermont.getUsername());
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audiserv.saveAuditorioDetall(aub);
    	Util.validateSourceAndUUID(uuid,sources);
    	Util.validateVinculeUser(usermont);
		servpersis.bindMonitorUser(usermont);
		logger.debug(" nos fuimos de base de datos");
    	try 
    	{
    		ObjectMapper map=new ObjectMapper();
			logger.debug("se recibe el json"+map.writeValueAsString(usermont));
    	}catch (Exception e)
    	{
			logger.debug("error creando el string json",e);
			throw new BussinesExcepcion(Constans.ERROR.ERROR_BUSSINES.getMensaje(),
    				Constans.ERROR.ERROR_BUSSINES.getCodigo());
		}
		
		DTOBase resp=new DTOBase();
		resp.setReDate(Util.getCurrentDateString());
		resp.setStatus("00");
		resp.setURI(req.getRequestURI());
		return new ResponseEntity<DTOBase>(resp, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "unbindusermon", method =  RequestMethod.POST)
	public ResponseEntity<DTOBase> unbindMonitorUser
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
     @RequestBody(required = true) DTOActiveMontUser usermont,
     HttpServletRequest req)
	throws BussinesExcepcion
	{
		logger.debug("inicio de desactivacion  del monitor");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("unbindMontUs");
    	aub.setEventUsu(usermont.getUsername());
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audiserv.saveAuditorioDetall(aub);
    	Util.validateSourceAndUUID(uuid,sources);
    	Util.validateunBindMont(usermont);
		servpersis.unbindMont(usermont);
		DTOBase resp=new DTOBase();
		resp.setReDate(Util.getCurrentDateString());
		resp.setStatus("00");
		resp.setURI(req.getRequestURI());
		return new ResponseEntity<DTOBase>(resp, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "findmon", method =  RequestMethod.POST)
	public ResponseEntity<DTOMonitores> findMonitor
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
     HttpServletRequest req)
	throws BussinesExcepcion
	{
		logger.debug("inicio de desactivacion  del monitor");
    	AuditoriaObject aub=new AuditoriaObject();
    	java.util.Map<String,Integer>maps=new java.util.Hashtable<String,Integer>();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("bindMontUs");
    	aub.setEventUsu("findmon");
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audiserv.saveAuditorioDetall(aub);
    	Util.validateSourceAndUUID(uuid,sources);
    	maps=servpersis.findMonitorAvible();
		
		DTOMonitores resp=new DTOMonitores();
		resp.setReDate(Util.getCurrentDateString());
		resp.setStatus("00");
		resp.setURI(req.getRequestURI());
		resp.setMap(maps);
		return new ResponseEntity<DTOMonitores>(resp, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "activemovil", method =  RequestMethod.POST)
	public ResponseEntity<DTOBase> activemovil
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
     @RequestBody(required = true) DTOActiveUser actvmon,
     HttpServletRequest req)
	throws BussinesExcepcion
	{
		logger.debug("inicio de desactivacion  del monitor");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("activemovil");
    	aub.setEventUsu(actvmon.getUsername());
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audiserv.saveAuditorioDetall(aub);
    	Util.validateSourceAndUUID(uuid,sources);
    	Util.validateUsername(actvmon);
    	servpersis.activeMovil(actvmon);
    	
    	
		DTOBase resp=new DTOBase();
		resp.setReDate(Util.getCurrentDateString());
		resp.setStatus("00");
		resp.setURI(req.getRequestURI());
		return new ResponseEntity<DTOBase>(resp, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "unactivemovil", method =  RequestMethod.POST)
	public ResponseEntity<DTOBase> unactivemovil
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
     @RequestBody(required = true) DTOActiveUser actvmon,
     HttpServletRequest req)
	throws BussinesExcepcion
	{
		logger.debug("inicio de desactivacion  del monitor");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("unactivemovil");
    	aub.setEventUsu(actvmon.getUsername());
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audiserv.saveAuditorioDetall(aub);
    	Util.validateSourceAndUUID(uuid,sources);
    	Util.validateUsername(actvmon);
    	servpersis.unActiveMovil(actvmon);
    	
    	
		DTOBase resp=new DTOBase();
		resp.setReDate(Util.getCurrentDateString());
		resp.setStatus("00");
		resp.setURI(req.getRequestURI());
		return new ResponseEntity<DTOBase>(resp, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "activealert", method =  RequestMethod.POST)
	public ResponseEntity<DTOBase> activealert
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
     @RequestBody(required = true) DTOActiveUser actvmon,
     HttpServletRequest req)
	throws BussinesExcepcion
	{
		logger.debug("inicio de desactivacion  del monitor");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("activealert");
    	aub.setEventUsu(actvmon.getUsername());
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audiserv.saveAuditorioDetall(aub);
    	Util.validateSourceAndUUID(uuid,sources);
    	Util.validateUsername(actvmon);
    	servpersis.activeAlert(actvmon);
    	
    	
		DTOBase resp=new DTOBase();
		resp.setReDate(Util.getCurrentDateString());
		resp.setStatus("00");
		resp.setURI(req.getRequestURI());
		return new ResponseEntity<DTOBase>(resp, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "unactivealert", method =  RequestMethod.POST)
	public ResponseEntity<DTOBase> unactivealert
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
     @RequestBody(required = true) DTOActiveUser actvmon,
     HttpServletRequest req)
	throws BussinesExcepcion
	{
		logger.debug("inicio de desactivacion  del monitor");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("unactivealert");
    	aub.setEventUsu(actvmon.getUsername());
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audiserv.saveAuditorioDetall(aub);
    	Util.validateSourceAndUUID(uuid,sources);
    	Util.validateUsername(actvmon);
    	servpersis.unActiveAlert(actvmon);
    	
    	
		DTOBase resp=new DTOBase();
		resp.setReDate(Util.getCurrentDateString());
		resp.setStatus("00");
		resp.setURI(req.getRequestURI());
		return new ResponseEntity<DTOBase>(resp, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "validateactivealertmovil", method =  RequestMethod.POST)
	public ResponseEntity<DTOBase> validateactivealertmovil
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
     @RequestBody(required = true) DTOActiveUser actvmon,
     HttpServletRequest req)
	throws BussinesExcepcion
	{
		logger.debug("inicio de desactivacion  del monitor");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("validateactivealertmovil");
    	aub.setEventUsu(actvmon.getUsername());
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audiserv.saveAuditorioDetall(aub);
    	Util.validateSourceAndUUID(uuid,sources);
    	Util.validateUsername(actvmon);
    	servpersis.isConfigAlertMovil(actvmon);
    	
		DTOBase resp=new DTOBase();
		resp.setReDate(Util.getCurrentDateString());
		resp.setStatus("00");
		resp.setURI(req.getRequestURI());
		return new ResponseEntity<DTOBase>(resp, HttpStatus.OK);
	}
	
	
	
	@RequestMapping(value = "consultsignal", method =  RequestMethod.POST)
	public ResponseEntity<DTOBase> consultsignal
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
     @RequestBody(required = true) DTOConsultSignalDateTime consult,
     HttpServletRequest req)
	throws BussinesExcepcion
	{
		logger.debug("inicio de la consutla de seniales   del monitor");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("consult signal date");
    	aub.setEventUsu(consult.getUsername());
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audiserv.saveAuditorioDetall(aub);
    	Util.validateSourceAndUUID(uuid,sources);
    	Paciente pat=null;
    	if(Util.isNullOEmpty(consult)) 
    	{
    		logger.debug(" error en la infor enviada");
    		BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
    	}
    	if(Util.isNullOEmpty(consult.getUsername())) 
    	{
    		logger.debug(" no nombre de usuario");
    		BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
    	}
    	if(Util.isNullOEmpty(consult.getDateTime())) 
    	{
    		logger.debug(" no fecha ");
    		BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
    	}
    	Usuario us=servpersis.findPatient(consult.getUsername());
    	logger.debug("paso al consulta cleinte");
    	try {
    	pat=serviclient.consulSignalPatient(us, consult.getDateTime());
    	}catch (Exception e) 
    	{
    		logger.debug("  ocurrio un error en la consulta de signal ",e);
    		BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
    	if(null==pat) 
    	{
    		BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
    	}
    	DTOPatientSignal dtopatient=new DTOPatientSignal();
    	dtopatient.setUsername(pat.getName());
    	java.util.ArrayList<String> obser=new java.util.ArrayList<String>(); 
    	try 
    	{
    		for(Signal sig:pat.getSignals()) 
    		{
    		obser.add(Util.convertJsonStringFhirp(sig.getObservation()));
    		}
    	}catch (Exception e) 
    	{
    		logger.debug(" error paila pailas",e);
    		BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
    	dtopatient.setObservations(obser);    	
    	dtopatient.setReDate(Util.getCurrentDateString());
    	dtopatient.setStatus("00");
    	dtopatient.setURI(req.getRequestURI());
		return new ResponseEntity<DTOBase>(dtopatient, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "usernomon", method =  RequestMethod.POST)
	public ResponseEntity<DTOBase> usernomon
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
     HttpServletRequest req)
	throws BussinesExcepcion
	{
		logger.debug("inicio de la consutla de seniales   del monitor");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("consult usernomon");
    	aub.setEventUsu("no name");
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audiserv.saveAuditorioDetall(aub);
    	Util.validateSourceAndUUID(uuid,sources);
    	java.util.List<Usuario> uss=servpersis.findUsersNoMont();
    	DTOUsers dtto=new DTOUsers();
    	dtto.setUsuarios(uss);    	
    	dtto.setReDate(Util.getCurrentDateString());
    	dtto.setStatus("00");
    	dtto.setURI(req.getRequestURI());
		return new ResponseEntity<DTOBase>(dtto, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "usermon", method =  RequestMethod.POST)
	public ResponseEntity<DTOBase> usermon
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
     HttpServletRequest req)
	throws BussinesExcepcion
	{
		logger.debug("inicio de los usuario vinculados al monitor");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("consult usermon");
    	aub.setEventUsu("no name");
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audiserv.saveAuditorioDetall(aub);
    	Util.validateSourceAndUUID(uuid,sources);
    	java.util.List<Usuario> uss=servpersis.findUserMon();
    	DTOUsers dtto=new DTOUsers();
    	dtto.setUsuarios(uss);    	
    	dtto.setReDate(Util.getCurrentDateString());
    	dtto.setStatus("00");
    	dtto.setURI(req.getRequestURI());
		return new ResponseEntity<DTOBase>(dtto, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "consultalrms", method =  RequestMethod.POST)
	public ResponseEntity<DTOBase> consultAlrms
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
     @RequestBody(required = true) DTOConsultSignalDateTime consult,
     HttpServletRequest req)
	throws BussinesExcepcion
	{
		logger.debug("inicio de la consutla de alarmas del ");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("consult alertas");
    	aub.setEventUsu(consult.getUsername());
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audiserv.saveAuditorioDetall(aub);
    	Util.validateSourceAndUUID(uuid,sources);
    	try 
    	{
    		ObjectMapper map=new ObjectMapper();
			logger.debug("se recibe el json"+map.writeValueAsString(consult));
    	}catch (Exception e)
    	{
			logger.debug("error creando el string json",e);
			throw new BussinesExcepcion(Constans.ERROR.ERROR_BUSSINES.getMensaje(),
    				Constans.ERROR.ERROR_BUSSINES.getCodigo());
		}
    	
    	if(Util.isNullOEmpty(consult)) 
    	{
    		logger.debug(" error en la infor enviada");
    		BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
    	}
    	if(Util.isNullOEmpty(consult.getUsername())) 
    	{
    		logger.debug(" no nombre de usuario");
    		BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
    	}
    	if(Util.isNullOEmpty(consult.getDateTime())) 
    	{
    		logger.debug(" no fecha "+consult.getDateTime());
    		BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
    	}
    	
    	if(Util.isNullOEmpty(consult.getIp())) 
    	{
    		logger.debug(" no IP ");
    		BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
    	}
    	Usuario us=servpersis.findPatient(consult.getUsername());
    	java.util.Map<String,Integer> mon=servpersis.findUserMonbyIp(consult.getIp());
    	if(null == mon) 
    	{
    		logger.debug(" no Ise encontro el monitor ");
    		BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus; 		
    	}
    	if(mon.isEmpty()) 
    	{
    		logger.debug(" no Ise encontro el monitor ");
    		BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
    	}
     	 java.util.List<Alertas> als=serviclient.consultAl(us,consult.getDateTime());
     	 java.util.List<DTOAlert> alsdto=new java.util.ArrayList<DTOAlert>();
     	 DTOAlertas alertas=new DTOAlertas();
     	 for(Alertas alrs:als) 
     	 {
     		 DTOAlert alr=alertas.new DTOAlert();
     		 logger.debug(alrs.getDescripcion());
     		 alr.setDescripcion(alrs.getDescripcion());
     		 alr.setHoraAlerta(alrs.getHoraAlerta());
     		 alr.setSeveridad(alrs.getSeveridad());
     		 alsdto.add(alr);
     	 }
     	 alertas.setAlerts(alsdto);
     	 alertas.setStatus("00");
     	 alertas.setURI(req.getRequestURI());
     	 alertas.setReDate(Util.getCurrentDateString());
		return new ResponseEntity<DTOBase>(alertas, HttpStatus.OK);
	}
	
	

}
