package co.fucs.APIWeb.util.exception;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.bouncycastle.cert.ocsp.Req;
import org.hl7.fhir.r4.model.Observation;

import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.pojo.Usuario;
import com.fucs.commo.web.util.Constans;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import co.fucs.APIWeb.dao.DAORol;
import co.fucs.APIWeb.dao.DAOTipoDocumento;
import co.fucs.APIWeb.dao.DAOUsuario;
import co.fucs.APIWeb.dto.DTOActiveMontUser;
import co.fucs.APIWeb.dto.DTOActiveUser;
import co.fucs.APIWeb.dto.DTORegistroUser;

public class Util
{
	
	public static boolean isNullOEmpty(Object s) 
	{
		if(null == s)
		{
			return true;
		}
		if(s instanceof String)
		{
			return ((String) s).isEmpty();
		}
		return false;
	}
	
	public static String getCurrentDateString() 
	{
		SimpleDateFormat simpl=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		return simpl.format(new java.util.Date());
	}
	
	
	public static String getDateInString(Calendar date) 
	{
		java.util.Date dat=new java.util.Date(date.getTimeInMillis());
		SimpleDateFormat simpl=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		return simpl.format(dat);
	}
	
	
	public static String getDateInString(String date)
	throws BussinesExcepcion
	{
		SimpleDateFormat simpl=new SimpleDateFormat("yyyy-MM-dd");
		try
		{
			java.util.Date form=simpl.parse(date);
			return simpl.format(form);
		}catch (Exception e) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_BUSSINES.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_BUSSINES.getMensaje());
			throw bus;
		}
	}
	
	
	public static String getDateTimeInString(String date)
			throws BussinesExcepcion
			{
				SimpleDateFormat simpl=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				try
				{
					java.util.Date form=simpl.parse(date);
					return simpl.format(form);
				}catch (Exception e) 
				{
					BussinesExcepcion bus=new BussinesExcepcion();
					bus.setCodigo(Constans.ERROR.ERROR_BUSSINES.getCodigo());
					bus.setMensaje(Constans.ERROR.ERROR_BUSSINES.getMensaje());
					throw bus;
				}
			}
	
	
	public static java.util.Date getStringToDate(String date)
	throws BussinesExcepcion
	{
		SimpleDateFormat simpl=new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date dateob=null;
		try
		{
			dateob=simpl.parse(date);
		}catch (ParseException e) 
		{
			BussinesExcepcion bus=new BussinesExcepcion(e);
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		return dateob;
	}
	
	
	public static Calendar getDateStringToCalendar(String date)
	throws BussinesExcepcion
	{
		SimpleDateFormat simpl=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		java.util.Date dateob=null;
		try
		{
	 dateob=simpl.parse(date);
		}catch (ParseException e) 
		{
			BussinesExcepcion bus=new BussinesExcepcion(e);
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		Calendar cal=Calendar.getInstance();
		cal.setTimeInMillis(dateob.getTime());
		return cal;
	}
	
	public static Calendar getDateStringToCalendarspc(String date)
			throws BussinesExcepcion
			{
				SimpleDateFormat simpl=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				java.util.Date dateob=null;
				try
				{
			 dateob=simpl.parse(date);
				}catch (ParseException e) 
				{
					BussinesExcepcion bus=new BussinesExcepcion(e);
					bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
					bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
					throw bus;
				}
				Calendar cal=Calendar.getInstance();
				cal.setTimeInMillis(dateob.getTime());
				return cal;
			}
	
	
	public static  void validateUserRegister(DTORegistroUser reg)
	throws BussinesExcepcion
	{
		boolean a,b,c,d,e,f = false;
		a=isNullOEmpty(reg);
		if(a) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		b=isNullOEmpty(reg.getTipodocumento());
		c=isNullOEmpty(reg.getDireccion());
		d=isNullOEmpty(reg.getDocumento());
		e=isNullOEmpty(reg.getNumeroCelular());
		f=isNullOEmpty(reg.getPassword());
		if(b & c & d & e & f) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
	}
	
	
	
	public static void validateSourceAndUUID(String ...in)
	throws BussinesExcepcion 
	{
		if(isNullOEmpty(in)) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		if(isNullOEmpty(in[0]) ||  isNullOEmpty(in[1]) ) 
		{			
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		boolean out= in[0].equals(Constans.VALID_SOURCES1) || in[0].equals(Constans.VALID_SOURCES2); 
		boolean out1=in[1].equals(Constans.VALID_SOURCES1) || in[1].equals(Constans.VALID_SOURCES2);
		if(false == out && false == out1) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
	}
	
	
	public static Integer getKeyforDefatulRol() 
	{
		java.util.Set<Integer> setKeys=DAORol.roles.keySet();
		Integer val=null;
		for(Integer key:setKeys) 
		{
			if(DAORol.roles.get(key).getDescrip().equals(Constans.DEFAULTROL)) 
			{
				val=key;
				break;
			}
			
		}
		return val;
	}
	
	
	
	
	
	public static  void validateUsername(DTOActiveUser reg)
			throws BussinesExcepcion
			{
				boolean a,b = false;
				a=isNullOEmpty(reg);
				if(a) 
				{
					BussinesExcepcion bus=new BussinesExcepcion();
					bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
					bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
					throw bus;
				}
				b=isNullOEmpty(reg.getUsername());
				if(b) 
				{
					BussinesExcepcion bus=new BussinesExcepcion();
					bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
					bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
					throw bus;
				}
			}
	
	public static  void validateUserActive(DTOActiveUser reg)
			throws BussinesExcepcion
			{
				boolean a,b,c,d = false;
				a=isNullOEmpty(reg);
				if(a) 
				{
					BussinesExcepcion bus=new BussinesExcepcion();
					bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
					bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
					throw bus;
				}
				b=isNullOEmpty(reg.getRoles());
				c=isNullOEmpty(reg.getStatusUser());
				d=isNullOEmpty(reg.getUsername());
				if(b & c & d) 
				{
					BussinesExcepcion bus=new BussinesExcepcion();
					bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
					bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
					throw bus;
				}
			}
	
	
	public static void validateVinculeUser(DTOActiveMontUser dtomon)
	throws BussinesExcepcion
	{
		boolean a,b,c = false;
		a=isNullOEmpty(dtomon);
		if(a) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		b=isNullOEmpty(dtomon.getIpMonitor());
		c=isNullOEmpty(dtomon.getUsername());
		if( b & c ) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;	
		}
	}
	
	
	public static void validateunBindMont(DTOActiveMontUser dtomon)
	throws BussinesExcepcion
	{
		boolean a,b,c = false;
		a=isNullOEmpty(dtomon);
		if(a) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		b=isNullOEmpty(dtomon.getIpMonitor());
		if(b) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;	
		}
		c=isNullOEmpty(dtomon.getUsername());
		if(c) 
		{
					BussinesExcepcion bus=new BussinesExcepcion();
					bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
					bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
					throw bus;	
		}
	}
	
	
	
	public static void loadDocAndtTipobyUsername(Usuario us) 
	{
		String manip=us.getName();
		Integer tipoDoc=Character.getNumericValue(manip.charAt(0));
		String document=manip.substring(1);
		us.setTipoDoc(DAOTipoDocumento.tipoDoc.get(tipoDoc));
		us.setNumerodoc(document);
	}
	
	
	
	public static String  convertJsonStringFhirp(Observation ob)
	throws BussinesExcepcion
	{
		IParser par=FhirContext.forR4().newJsonParser();
		try 
		{
		return par.encodeResourceToString(ob);
		}catch (Exception e) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getMensaje());
			throw bus;
		}
		
	}

}
