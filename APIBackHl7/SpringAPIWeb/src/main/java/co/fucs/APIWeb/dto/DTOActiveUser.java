package co.fucs.APIWeb.dto;

import com.fucs.commo.web.pojo.Rol;


public class DTOActiveUser extends DTOBase 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7160303828901882566L;
	
	
	private java.util.List<Rol>roles;
	
	private Integer statusUser;
	
	private String username;

	public DTOActiveUser() 
	{
		// TODO Auto-generated constructor stub
	}

	public java.util.List<Rol> getRoles() {
		return roles;
	}

	public void setRoles(java.util.List<Rol> roles) {
		this.roles = roles;
	}

	

	public Integer getStatusUser() {
		return statusUser;
	}

	public void setStatusUser(Integer statusUser) {
		this.statusUser = statusUser;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
	

}
