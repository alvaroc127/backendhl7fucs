package co.fucs.APIWeb.dto;

import java.io.Serializable;

public class DTOBase implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6866640059150670759L;

	/**
	 * 
	 */
	

	private String URI;
	
	private String reDate;
	
	private String status;
	
	
	public DTOBase() {
		// TODO Auto-generated constructor stub
	}


	public String getURI() {
		return URI;
	}


	public void setURI(String uRI) {
		URI = uRI;
	}


	public String getReDate() {
		return reDate;
	}


	public void setReDate(String reDate) {
		this.reDate = reDate;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	

}
