package co.fucs.APIWeb;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.stereotype.Controller;

import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.pojo.AuditoriaObject;
import com.fucs.commo.web.util.Constans;

import co.fucs.APIWeb.dto.DTOGenericResponse;
import co.fucs.APIWeb.services.AuditoriaServices;
import co.fucs.APIWeb.util.exception.Util;

@ControllerAdvice
//@RequestMapping(value ="/",method= {RequestMethod.GET,RequestMethod.POST})
public class ExcepcionController
{
	private static final Logger logger = LoggerFactory.getLogger(ExcepcionController.class);

	private AuditoriaServices aud;
	
	public ExcepcionController() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public ExcepcionController( AuditoriaServices aud) {
		this.aud=aud;
		// TODO Auto-generated constructor stub
	}
	/*@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		request.getRequestURI();
		request.getContextPath();
		request.getProtocol();
		request.getRemoteAddr();
		request.getRemoteHost();
		// logica de validaciones generar un delegado de validacion y un enrutador
		return null;
	}*/	 
	
	
	@ExceptionHandler(NoHandlerFoundException.class)
	public @ResponseBody ResponseEntity<DTOGenericResponse> pagNotFount(HttpServletRequest  request) 
	{
		logger.debug("la pagina no se puedo encontrar ");
		AuditoriaObject obj=new AuditoriaObject();
		DTOGenericResponse genres=new DTOGenericResponse();
		obj.setIp(request.getRemoteAddr());
		obj.setUrl(request.getRequestURI());
		obj.setDate(Calendar.getInstance());
		logger.debug("se inicia el proceso de auditoria");
		try
		{
		aud.saveAuditorioa(obj);
		}catch (BussinesExcepcion e)
		{
			logger.debug("excepcion en el controlador");
			genres.setMessage(e.getMensaje());
			genres.setReDate(Util.getCurrentDateString());
			genres.setStatus(String.valueOf(e.getCodigo()));
			logger.debug("excepcion en el finalizador");
		}
		return new ResponseEntity<DTOGenericResponse>(genres, HttpStatus.NOT_FOUND);
				
	}
	
	
	@ExceptionHandler(BussinesExcepcion.class)
	public @ResponseBody ResponseEntity<DTOGenericResponse> errorInBussines(Exception exp)
	{
		BussinesExcepcion bss=(BussinesExcepcion)exp;
		DTOGenericResponse genres=new DTOGenericResponse();
		genres.setMessage(bss.getMensaje());
		genres.setStatus(String.valueOf(bss.getCodigo()));
		switch (bss.getCodigo())
		{
		case -501: case -150:
			return new ResponseEntity<DTOGenericResponse>(genres,HttpStatus.BAD_REQUEST);
		case -50: case -40: case -30: case -180:
			return new ResponseEntity<DTOGenericResponse>(genres,HttpStatus.PRECONDITION_FAILED);
		case -60: case -80:  case -90:
			return new ResponseEntity<DTOGenericResponse>(genres,HttpStatus.UNAUTHORIZED);			
		case -300:
			return new ResponseEntity<DTOGenericResponse>(genres,HttpStatus.INTERNAL_SERVER_ERROR);		
		default:
			return new ResponseEntity<DTOGenericResponse>(genres,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
	
	

}
