package co.fucs.APIWeb.clients;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fucs.commo.web.rosource.ResourceInternal;
import com.fucs.commo.web.util.Constans;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidConfig.Priority;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.ApnsConfig;
import com.google.firebase.messaging.Aps;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.google.protobuf.Duration;




public class ClientCFM 
{
	
	private static final Logger logger = LoggerFactory.getLogger(ClientCFM.class);
	
	static 
	{
		try
		{
			FileInputStream file=new FileInputStream
			(new File(ResourceInternal.getInstance().getProperty(Constans.KEY_WEB,Constans.FIREBASEKEY)));
			FirebaseOptions fireob=new FirebaseOptions.Builder().
			setCredentials(GoogleCredentials.fromStream(file)).build();
			FirebaseApp.initializeApp(fireob);
		}catch (Exception e) 
		{
			logger.debug("error en la excepcion ",e);
		}
		
	}
	
	public ClientCFM() 
	{
		
		// TODO Auto-generated constructor stub
	}
	
	
	
	public void sendMenssageToFCM(Map<String,String> mapdata)
	{
		try 
		{
			AndroidConfig androidconfig=
			AndroidConfig.builder().setTtl(20000).
			setCollapseKey(Constans.TOPICSFUCS)
			.setPriority(Priority.HIGH)
			.setNotification(AndroidNotification.builder().setTag(Constans.TOPICSFUCS).build()).build();
			
			ApnsConfig apnsConfig = ApnsConfig.builder()
			        .setAps(Aps.builder().
			setCategory(Constans.TOPICSFUCS).setThreadId(Constans.TOPICSFUCS).build()).build();
			
			Message message=Message.builder().putAllData(mapdata).setApnsConfig(apnsConfig)
					.setAndroidConfig(androidconfig).
					setNotification(Notification.builder().setTitle("ALERTA DESDE UCI")
					.setBody
					(" El paciente "+mapdata.get("patient_id")+"mon"+mapdata.get("montior_ip")).build()).
					setTopic("fucstopic").
					build();
			 FirebaseMessaging.getInstance().sendAsync(message).get();
		}catch (Exception e) 
		{
		
			logger.debug("no fue posible enviar el mensaje a firebase ",e);
		}
		
	}
	
	
	
	
	
	
	

}
