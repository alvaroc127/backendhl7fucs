package co.fucs.APIWeb.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.AuditoriaObject;
import com.fucs.commo.web.util.Constans;

import co.fucs.APIWeb.HomeController;
import co.fucs.APIWeb.dao.DAOAuditoria;
import co.fucs.APIWeb.util.exception.Util;

@Service
public class AuditoriaServices 
{
	
	private static final Logger logger = LoggerFactory.getLogger(AuditoriaServices.class);
	
	
	public void saveAuditorioa(AuditoriaObject auditar)
	throws BussinesExcepcion
	{
		logger.debug("inicio del grabado en auditoria ");
		if(!Util.isNullOEmpty(auditar.getIp()) && !Util.isNullOEmpty(auditar.getUrl()) && !Util.isNullOEmpty(auditar.getDate())) 
		{
			logger.debug("los objetos no son null ");
			DAOAuditoria daoAu=new DAOAuditoria();
			try
			{
				daoAu.saveAuditoria(auditar);
			}catch (PercistenExcepcion e)
			{
				BussinesExcepcion busi= new BussinesExcepcion(e);
				busi.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
				busi.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
				throw busi;
			}
		}
		
	}
	
	
	public void saveAuditorioDetall(AuditoriaObject auditar)
			throws BussinesExcepcion
			{
				logger.debug("inicio del grabado en auditoria ");
				if(!Util.isNullOEmpty(auditar.getIp()) && !Util.isNullOEmpty(auditar.getUrl()) && !Util.isNullOEmpty(auditar.getDate())) 
				{
					logger.debug("los objetos no son null ");
					DAOAuditoria daoAu=new DAOAuditoria();
					try
					{
						daoAu.saveAuditoriaAvanc(auditar);
					}catch (PercistenExcepcion e)
					{
						logger.debug("ocurrio un error en ",e);
						BussinesExcepcion busi= new BussinesExcepcion(e);
						busi.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
						busi.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
						throw busi;
					}
				}
				
			}
	
	
	

}
