package co.fucs.APIWeb.dto;

import java.io.Serializable;

public class DTOBaseUser extends DTOBase implements Serializable 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3778620664069285607L;

	private String username;
	
	private String password;
	
	
	public DTOBaseUser() {
		// TODO Auto-generated constructor stub
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	
}
