package co.fucs.APIWeb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.fucs.APIWeb.dao.BaseDAO;
import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.AuditoriaObject;
import com.fucs.commo.web.util.Constans;



public class DAOAuditoria extends BaseDAO
{
	
	private final static String INSERTAUDIT="INSERT INTO dbo.AUDITORIA(audisource,audidate,audipath) VALUES (?,?,?) ";
	
	
	private final static String INSERTAUDITAVAN="INSERT INTO dbo.AUDITORIA(audisource,audidate,audipath,event1,event2,eventus) VALUES (?,?,?,?,?,?)";
	
	private static final Logger logger = LoggerFactory.getLogger(DAOAuditoria.class);
	
	public DAOAuditoria() 
	{
	 super();
	}
	
	
	public void saveAuditoria(AuditoriaObject audit)
	throws PercistenExcepcion
	{
		logger.debug(" solicitando el JNDI ");
		Connection con=this.getConection(Constans.FUCSCONECT);
		PreparedStatement stat=null;
		logger.debug(" solicitando el JNDI ");
		try 
		{
		stat=con.prepareStatement(INSERTAUDIT);
		stat.setString(1, audit.getIp());
		stat.setTimestamp(2, new java.sql.Timestamp(audit.getDate().getTimeInMillis()));
		stat.setString(3, audit.getUrl());
		stat.executeUpdate();
		}catch (Exception e) 
		{
			PercistenExcepcion perx=new PercistenExcepcion(e);
			perx.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			perx.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw perx;
		}
		this.close(stat);
		this.close(con);
	}
	
	
	public void saveAuditoriaAvanc(AuditoriaObject audit)
			throws PercistenExcepcion
			{
				logger.debug(" solicitando el JNDI ");
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				logger.debug(" solicitando el JNDI ");
				try 
				{
				stat=con.prepareStatement(INSERTAUDITAVAN);
				stat.setString(1, audit.getIp());
				stat.setTimestamp(2, new java.sql.Timestamp(audit.getDate().getTimeInMillis()));
				stat.setString(3, audit.getUrl());
				stat.setString(4, null==audit.getEvento1()?"":audit.getEvento1());
				stat.setString(5,null==audit.getEvento2()?"":audit.getEvento2());
				stat.setString(6,null==audit.getEventUsu()?"":audit.getEventUsu());
				stat.executeUpdate();
				}catch (Exception e) 
				{
					logger.debug("error en auditar ",e);
					PercistenExcepcion perx=new PercistenExcepcion(e);
					perx.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
					perx.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
					throw perx;
				}
				this.close(stat);
				this.close(con);
			}
			
	
	
	

}
;