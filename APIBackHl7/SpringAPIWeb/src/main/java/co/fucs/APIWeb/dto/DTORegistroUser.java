package co.fucs.APIWeb.dto;

public class DTORegistroUser extends DTOBase 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8098181771297077464L;
	
	
	private String documento;
	
	private Integer tipodocumento;
	
	private String fechaNacimiento;
	
	private String direccion;
	
	private String numeroCelular;
	
	private String password;
	
	
	
	
	
	
	public DTORegistroUser() 
	{
		// TODO Auto-generated constructor stub
	}




	public String getDocumento() {
		return documento;
	}




	public void setDocumento(String documento) {
		this.documento = documento;
	}




	public Integer getTipodocumento() {
		return tipodocumento;
	}




	public void setTipodocumento(Integer tipodocumento) {
		this.tipodocumento = tipodocumento;
	}




	public String getFechaNacimiento() {
		return fechaNacimiento;
	}




	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}




	public String getDireccion() {
		return direccion;
	}




	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}




	public String getNumeroCelular() {
		return numeroCelular;
	}




	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}

	
	

}
