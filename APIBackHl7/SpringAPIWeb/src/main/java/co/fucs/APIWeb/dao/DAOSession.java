package co.fucs.APIWeb.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.fucs.APIWeb.dao.BaseDAO;
import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.Sesion;
import com.fucs.commo.web.util.Constans;



public class DAOSession extends BaseDAO 
{
	
	private static final Logger logger = LoggerFactory.getLogger(DAOSession.class);
	
	public static final String INSERTSESION=
			"INSERT INTO dbo.SESION_WEB(ID_USU,STATUS,ACCESTOKEN,EXPIRE ) VALUES"+
			"(?,1,?,?)";
	
	
	
	
	public static final  String VALIDATEEXISTSESION=
	" SELECT count(ID_USU) FROM dbo.SESION_WEB sw " +
	" WHERE ID_USU = ? " +
	" AND STATUS  = 1 " +
	" GROUP BY ID_USU ";
	
	
	public static final  String VALIDATEEXISTSESIONFIELDS=
			" SELECT sw.ID_SESION, sw.ID_USU , sw.DATE_CREATE, sw.STATUS, sw.ACCESTOKEN, sw.EXPIRE  FROM dbo.SESION_WEB sw " +
			" WHERE ID_SESION =  " +
			" (	SELECT ID_SESION  "+
			" FROM dbo.SESION_WEB sw  "+
			" WHERE ID_USU = ? "+
			" AND STATUS  = 1 " +
			" GROUP BY ID_SESION ) "+
			" AND STATUS  = 1 ";
	
	public static final String ENDSESION =
			" UPDATE dbo.SESION_WEB "+
			" SET STATUS = 2 " +
			" WHERE ID_USU = ? ";
	
	
	public DAOSession() 
	{
		super();
	}
	
	
	public int insertStartSession(Sesion se) 
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.FUCSCONECT);
		PreparedStatement stat=null;
		logger.debug("se inicia la insercion de sesion ");
		try 
		{
			stat=con.prepareStatement(INSERTSESION);			
			stat.setInt(1,se.getIdUsuario());
			stat.setString(2,se.getAccestoken());
			stat.setInt(3, se.getExpire());
			return stat.executeUpdate();
		}catch (Exception e) 
		{
			logger.debug("error en el update",e);
			PercistenExcepcion pers=new PercistenExcepcion(e);
			pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw pers;
		}finally 
		{
			this.close(stat);
			this.close(con);
		}
	}
	
	
	
	public void loadExisteSession(Sesion se) 
			throws PercistenExcepcion
			{
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				ResultSet rs=null;
				logger.debug("se valida existencia de sesion ");
				try 
				{
					stat=con.prepareStatement(VALIDATEEXISTSESIONFIELDS);
					stat.setInt(1, se.getIdUsuario());
					rs=stat.executeQuery();
					while(rs.next()) 
					{
						se.setIdsesion(rs.getInt("ID_SESION"));
						se.setIdUsuario(rs.getInt("ID_USU"));
						Calendar cal= Calendar.getInstance();
						cal.setTimeInMillis(rs.getTimestamp("DATE_CREATE").getTime());
						se.setDateCreated(cal);
						se.setStatus(rs.getInt("STATUS"));
						se.setAccestoken(rs.getString("ACCESTOKEN"));
						se.setExpire(rs.getInt("EXPIRE"));
					}
				}catch (Exception e) 
				{
					logger.debug("error en la validacion ",e);
				PercistenExcepcion pers=new PercistenExcepcion();
				pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
				pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
				throw pers;
				}finally 
				{
					this.close(rs);
					this.close(stat);
					this.close(con);
				}
			}

	
	public boolean validateExisteSession(Sesion se) 
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.FUCSCONECT);
		PreparedStatement stat=null;
		ResultSet rs=null;
		Integer count=-1;
		logger.debug("se valida existencia de sesion ");
		try 
		{
			stat=con.prepareStatement(VALIDATEEXISTSESION);
			stat.setInt(1, se.getIdUsuario());
			rs=stat.executeQuery();
			while(rs.next()) 
			{
				count=rs.getInt(1);
			}
			return count==1?true:false;
		}catch (Exception e) 
		{
			logger.debug("error en la validacion ",e);
		PercistenExcepcion pers=new PercistenExcepcion();
		pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
		pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
		throw pers;
		}finally 
		{
			this.close(rs);
			this.close(stat);
			this.close(con);
		}
	}
	
	public boolean updateEndSession(Sesion se) 
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.FUCSCONECT);
		PreparedStatement stat=null;
		logger.debug(" se realiza el update de sesion");
		try 
		{
		stat=con.prepareStatement(ENDSESION);
		stat.setInt(1, se.getIdUsuario());
		Integer salid=stat.executeUpdate();
		return salid>0?true:false;
		}catch (Exception e) 
		{
			logger.debug("error en el update end sesion ",e);
			PercistenExcepcion perx=new PercistenExcepcion();
			perx.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			perx.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw perx;
		}finally 
		{
			this.close(stat);
			this.close(con);
		}
	} 
	
	 
	
}
