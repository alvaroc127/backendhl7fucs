package co.fucs.APIWeb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.fucs.APIWeb.dao.BaseDAO;


import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.Permiso;
import com.fucs.commo.web.pojo.Rol;
import com.fucs.commo.web.pojo.Usuario;
import com.fucs.commo.web.util.Constans;
import co.fucs.APIWeb.dao.DAOTipoDocumento;
import co.fucs.APIWeb.dao.DAORol;



/*
 * se debe determinar sin dao de base de datos o llamadas a los cliententes
 */
public class DAOUsuario  extends BaseDAO
{
	
	private static final Logger logger = LoggerFactory.getLogger(DAOUsuario.class);
	
	public DAOUsuario() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	private  static final String SELECTBYDOTIP=
			" SELECT  us.ID_USU,us.NUMERO_DOC ,us.DATE_CREATE ,us.EMAIL ,us.EMAIL, " +
			" us.TIPO_DOC, us.password ,us.STATUS ,us.DATE_CREATE, " +
			" uxr.ID_ROL AS ROLCODE, rxp.ID_PER AS PERCODE, us.date_update " +
			" FROM  dbo.USUARIOXROL uxr " +
			" INNER JOIN dbo.USUARIO us ON (uxr.ID_USU = us.ID_USU ) " +
			" INNER JOIN dbo.ROLXPERMISOS rxp  ON (rxp.ID_ROL = uxr.ID_ROL ) " +
			" INNER JOIN dbo.ROL r ON (r.ID_ROL = rxp.ID_ROL ) " +
			" INNER JOIN dbo.PERMISOS p  ON (rxp.ID_PER =p.ID_PER ) " +
			" WHERE  us.STATUS = 1 AND r.STATUS  = 1 AND p.STATUS  = 1 " +
			 " AND us.TIPO_DOC = ?" +
			 " AND us.NUMERO_DOC = ?";
	
	
	private  static final String SELECTBYDOTIPNOSTATUS=
			" SELECT  us.ID_USU,us.NUMERO_DOC ,us.DATE_CREATE ,us.EMAIL ,us.EMAIL, " +
			" us.TIPO_DOC, us.password ,us.STATUS ,us.DATE_CREATE, " +
			" uxr.ID_ROL AS ROLCODE, rxp.ID_PER AS PERCODE, us.date_update " +
			" FROM  dbo.USUARIOXROL uxr " +
			" INNER JOIN dbo.USUARIO us ON (uxr.ID_USU = us.ID_USU ) " +
			" INNER JOIN dbo.ROLXPERMISOS rxp  ON (rxp.ID_ROL = uxr.ID_ROL ) " +
			" INNER JOIN dbo.ROL r ON (r.ID_ROL = rxp.ID_ROL ) " +
			" INNER JOIN dbo.PERMISOS p  ON (rxp.ID_PER =p.ID_PER ) " +
			" WHERE  r.STATUS  = 1 AND p.STATUS  = 1 " +
			 " AND us.TIPO_DOC = ?" +
			 " AND us.NUMERO_DOC = ?";
	
	private static final String INSERUSER=
			"INSERT INTO FUCS.dbo.USUARIO " +
			" (TIPO_DOC, NUMERO_DOC, FECHA_NACIMIENTO, DIRECCION, NUMEROCEL, password, STATUS) " +
			" VALUES(?,?,?,?,?,?,2) ";
	
	private static final String SELECTPREENRIL=
			" SELECT u.ID_USU FROM dbo.USUARIO u  "+
			" WHERE u.TIPO_DOC = ? "+
			" AND u.NUMERO_DOC  = ? ";
	
	
	private static final String INSERROLUSU=
			"INSERT INTO dbo.USUARIOXROL (ID_USU ,ID_ROL ) VALUES (?,?)";
	
	private static final String UPDATEUSSACT=
			" UPDATE dbo.USUARIO " +
			" SET STATUS =  1 " +
			" WHERE NUMERO_DOC = ?  "+
			" AND TIPO_DOC  = ? ";
	
	
	private static final String DELETEROLUSU=
			" DELETE FROM dbo.USUARIOXROL  " +
			" WHERE ID_ROL <> 4 AND ID_USU = ? ";
	
	private  static final String SELECTBYID=
			" SELECT  us.ID_USU,us.NUMERO_DOC ,us.DATE_CREATE ,us.EMAIL ,us.EMAIL, " +
			" us.TIPO_DOC, us.password ,us.STATUS ,us.DATE_CREATE, " +
			" uxr.ID_ROL AS ROLCODE, rxp.ID_PER AS PERCODE, us.date_update " +
			" FROM  dbo.USUARIOXROL uxr " +
			" INNER JOIN dbo.USUARIO us ON (uxr.ID_USU = us.ID_USU ) " +
			" INNER JOIN dbo.ROLXPERMISOS rxp  ON (rxp.ID_ROL = uxr.ID_ROL ) " +
			" INNER JOIN dbo.ROL r ON (r.ID_ROL = rxp.ID_ROL ) " +
			" INNER JOIN dbo.PERMISOS p  ON (rxp.ID_PER =p.ID_PER ) " +
			" WHERE  us.STATUS = 1 AND r.STATUS  = 1 AND p.STATUS  = 1 " +
			 " AND us.ID_USU = ?";
	
	
	private static final String VALIDATEALERT=
			" SELECT  us.ID_USU,us.NUMERO_DOC ,us.DATE_CREATE ,us.EMAIL ,us.EMAIL, " +
					" us.TIPO_DOC, us.password ,us.STATUS ,us.DATE_CREATE, " +
					" uxr.ID_ROL AS ROLCODE, rxp.ID_PER AS PERCODE, us.date_update " +
					" FROM  dbo.USUARIOXROL uxr " +
					" INNER JOIN dbo.USUARIO us ON (uxr.ID_USU = us.ID_USU ) " +
					" INNER JOIN dbo.ROLXPERMISOS rxp  ON (rxp.ID_ROL = uxr.ID_ROL ) " +
					" INNER JOIN dbo.ROL r ON (r.ID_ROL = rxp.ID_ROL ) " +
					" INNER JOIN dbo.PERMISOS p  ON (rxp.ID_PER =p.ID_PER ) " +
					" WHERE  us.STATUS = 1 AND r.STATUS  = 1 AND p.STATUS  = 1 " +
					 " AND us.TIPO_DOC = ?" +
					 " AND us.NUMERO_DOC = ?"+
					 " AND us.APPMOVIL = 'S' "+
					 " AND us.ALERTAS = 'S' ";
	
	private static final String ACTIVEMOVIL =
			" UPDATE dbo.USUARIO " +
			" SET APPMOVIL = 'S'  " +
			" WHERE NUMERO_DOC = ? " +
			" AND TIPO_DOC  = ? ";
	
	private static final String UNACTIVEMOVIL=
			" UPDATE dbo.USUARIO " +
			" SET APPMOVIL = 'N' " +
			" WHERE NUMERO_DOC = ? " +
			" AND TIPO_DOC  = ? ";
	
	private static final String ACTIVEALERT=
			" UPDATE dbo.USUARIO " +
			" SET ALERTAS  = 'S' " +
			" WHERE NUMERO_DOC = ? " +
			" AND TIPO_DOC  = ?  ";

	private static final String UNACTIVEALERT=
			" UPDATE dbo.USUARIO  " +
			" SET ALERTAS  = 'N' " +
			" WHERE NUMERO_DOC = ? " +
			" AND TIPO_DOC  = ?  ";
	
	
	private static final String CHANGEPASS=
			" UPDATE dbo.USUARIO  " +
			" SET password = ? " +
			" WHERE ID_USU  = ? ";
	
	
	private static final String SELECTUSERNOMON=
			" SELECT u.*,r.ID_ROL FROM dbo.USUARIO u  "+
			" INNER JOIN USUARIOXROL u2 ON u2.ID_USU  = u.ID_USU "+
			" INNER JOIN rol r ON r.ID_ROL = u2.ID_ROL  "+
			" WHERE r.NOMBRE ='DEFAULT' "+
			" AND u.ID_USU NOT IN (SELECT u2.ID_USU FROM dbo.USUARIO u2  "+
			" INNER JOIN USUARIOXMONITOR uxm ON uxm.ID_USU  = u2.ID_USU)";
	
	
	
	private static final String SELECTUSERMON=
			" SELECT u.*,r.ID_ROL,m2.IP FROM dbo.USUARIO u   "+
			" INNER JOIN USUARIOXMONITOR uxm ON uxm.ID_USU  = u.ID_USU "+
			" INNER JOIN USUARIOXROL uxr ON uxr.ID_USU  = u.ID_USU  "+
			" INNER JOIN rol r ON r.ID_ROL = uxr.ID_ROL   "+
			" INNER JOIN Monitor m2  ON uxm.ID = m2.ID ";
			//" WHERE r.NOMBRE ='DEFAULT' ";
			//" AND u.STATUS = 1  ";
	
	
	public boolean activeMovil(int tipodoc,String numerodoc) 
			throws PercistenExcepcion
			{
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				logger.debug("inicio de la activacion del movil");
				try 
				{
					stat=con.prepareStatement(ACTIVEMOVIL);
					stat.setString(1, numerodoc);
					stat.setInt(2, tipodoc);
					return stat.executeUpdate()>0?true:false;
				}catch (Exception e) 
				{
					logger.debug("error en la activacion ",e);
					logger.debug("finalizacion de la activacion del movil ");
					PercistenExcepcion pers=new PercistenExcepcion(e);
					pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
					pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
					throw  pers;
				}	
			}
	
	
	public boolean unactiveMovil(int tipodoc,String numerodoc) 
			throws PercistenExcepcion
			{
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				logger.debug("inicio de la desactivacion del movil del movil");
				try 
				{
					stat=con.prepareStatement(UNACTIVEMOVIL);
					stat.setString(1, numerodoc);
					stat.setInt(2, tipodoc);
					return stat.executeUpdate()>0?true:false;
				}catch (Exception e) 
				{
					logger.debug("error en la desacactivacion ",e);
					logger.debug("finalizacion de la desacactivacion del movil ");
					PercistenExcepcion pers=new PercistenExcepcion(e);
					pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
					pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
					throw  pers;
				}	
			}
	
	public boolean activeAlert(int tipodoc,String numerodoc) 
			throws PercistenExcepcion
			{
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				logger.debug("inicio de la activacion de alertas");
				try 
				{
					stat=con.prepareStatement(ACTIVEALERT);
					stat.setString(1, numerodoc);
					stat.setInt(2, tipodoc);
					return stat.executeUpdate()>0?true:false;
				}catch (Exception e) 
				{
					logger.debug("activacion del alertas ",e);
					logger.debug("finalizacion de la alertas  ");
					PercistenExcepcion pers=new PercistenExcepcion(e);
					pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
					pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
					throw  pers;
				}	
			}

	
	
	public boolean unactiveAlert(int tipodoc,String numerodoc) 
			throws PercistenExcepcion
			{
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				logger.debug("inicio de la  desacactivacion de alertas");
				try 
				{
					stat=con.prepareStatement(UNACTIVEALERT);
					stat.setString(1, numerodoc);
					stat.setInt(2, tipodoc);
					return stat.executeUpdate()>0?true:false;
				}catch (Exception e) 
				{
					logger.debug("desacactivacion del alertas ",e);
					logger.debug("finalizacion de la desacactivacion de aleta  ");
					PercistenExcepcion pers=new PercistenExcepcion(e);
					pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
					pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
					throw  pers;
				}	
			}
	
	public Usuario findbyID(Integer idUsu)
			throws PercistenExcepcion
			{
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				ResultSet rs=null;
				Usuario us=null;
				java.util.Hashtable<String, Usuario> usuarios=new java.util.Hashtable<String, Usuario>();
				try
				{
					 stat=con.prepareStatement(SELECTBYID);
					 stat.setInt(1,idUsu);
					 rs=stat.executeQuery();
					while(rs.next()) 
				 	{
						if (!usuarios.isEmpty()) us=usuarios.get(rs.getInt("TIPO_DOC")+rs.getString("NUMERO_DOC"));
				 		if(null==us) 
				 		{
				 		us=new Usuario();
				 		us.setIdusu(rs.getLong("ID_USU"));
				 		
				 		us.setEmail(rs.getString("EMAIL"));
				 		us.setNumerodoc(rs.getString("NUMERO_DOC"));
				 		//us.setGenero(rs.getInt("GENERO"));
				 		//rs.getTimestamp("FECHA_NACIMIENTO");
				 		//us.setDireccion(rs.getString("DIRECCION"));
				 		//us.setNumeroCel(rs.getString("NUMEROCEL"));
				 		us.setPassword(rs.getString("password"));
				 		us.setStatus(rs.getInt("STATUS"));
				 		Calendar cal=Calendar.getInstance();
				 		cal.setTimeInMillis(rs.
				 				getTimestamp("DATE_CREATE").getTime());
				 		us.setDateCreated(cal);
				 		us.setName(rs.getInt("TIPO_DOC")+us.getNumerodoc());
				 		Rol rol=DAORol.roles.get(rs.getInt("ROLCODE"));
				 		Permiso perm=DAOPermiso.permisos.get(rs.getInt("PERCODE"));
				 		rol.addPermis(perm.getId(),perm);
				 		us.usuarioaddRol(rol.getId(), rol);
				 		usuarios.put(us.getName(),us);
				 		}
				 		else
				 		{
				 			Rol rol=DAORol.roles.get(rs.getInt("ROLCODE"));
					 		Permiso perm=DAOPermiso.permisos.get(rs.getInt("PERCODE"));
					 		rol.addPermis(perm.getId(),perm);
					 		us.usuarioaddRol(rol.getId(), rol);
					 		usuarios.put(us.getName(),us);
				 		}
				 	}	
				}catch (Exception e) 
				{
					PercistenExcepcion perx=new PercistenExcepcion(e);
					logger.debug("error del dao USUARIO",e);
					perx.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
					perx.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
					throw perx;
				}
				Conexion.close(rs);
			 	Conexion.close(stat);
			 	Conexion.close(con);
				return usuarios.get(us.getName());
			}
	
	
	public boolean deleteRolUser(int idUsu) 
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.FUCSCONECT);
		PreparedStatement stat=null;
		logger.debug("inicio de la  eliminacion del rol");
		try 
		{
			stat=con.prepareStatement(DELETEROLUSU);
			stat.setInt(1,idUsu);
			return stat.executeUpdate()>0?true:false;
		}catch (Exception e) 
		{
			logger.debug("error en la eliminacion",e);
			logger.debug("finalizacion de la eliminacion del rol");
			PercistenExcepcion pers=new PercistenExcepcion(e);
			pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw  pers;
		}	
	}
	
	
	public boolean createRolUs(Integer usu,Integer rol)
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.DEFAULTCONECT);
		PreparedStatement stat=null;
		try 
		{
		stat=con.prepareStatement(INSERROLUSU);	
		stat.setInt(1, usu);
		stat.setInt(2, rol);
		return stat.executeUpdate() > 0?true:false;
		}catch (Exception e) 
		{
			logger.debug("error del dao USUARIO",e);
			PercistenExcepcion pers=new PercistenExcepcion(e);
			pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw pers;
		}finally 
		{
			this.close(stat);
			this.close(con);
		}
		
	}
	
	
	public boolean updateStateActive(Usuario us)
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.DEFAULTCONECT);
		PreparedStatement stat=null;
		try 
		{
			stat=con.prepareStatement(UPDATEUSSACT);
			stat.setString(1, us.getNumerodoc());
			stat.setInt(2, us.getTipoDoc().getIdtipdoc());
			return stat.executeUpdate()>0?true:false;
		}catch (Exception e) 
		{
			logger.debug("error del dao USUARIO",e);
			PercistenExcepcion pers=new PercistenExcepcion(e);
			pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw pers;
		}finally 
		{
			this.close(stat);
			this.close(con);
		}
		
	}
	
	public boolean createUser(Usuario us)
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.DEFAULTCONECT);
		PreparedStatement stat=null;
		try 
		{
			stat=con.prepareStatement(INSERUSER);
			stat.setInt(1, us.getTipoDoc().getIdtipdoc());
			stat.setString(2, us.getNumerodoc());
			stat.setTimestamp(3, null==us.getFechaNacimiento()?null:new Timestamp(us.getFechaNacimiento().getTime()));
			stat.setString(4, us.getDireccion());
			stat.setString(5, us.getNumeroCel());
			stat.setString(6,us.getPassword());
			return  stat.executeUpdate()>0?true:false;
		}catch (Exception e) 
		{
			logger.debug("error del dao USUARIO",e);
			PercistenExcepcion pers=new PercistenExcepcion(e);
			pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw pers;
		}finally 
		{
			this.close(stat);
			this.close(con);
		}
	}
	
	
	public Integer findbyDocPreEnroll(Integer tipodoc,String numeroDoc)
			throws PercistenExcepcion
			{
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				ResultSet rs=null;
				logger.debug(" se busca el usuario " +tipodoc);
				logger.debug(" se busca el usuario " +numeroDoc);
				try
				{
					stat=con.prepareStatement(SELECTPREENRIL);
					stat.setInt(1, tipodoc);
					stat.setString(2, numeroDoc);
					rs=stat.executeQuery();
					Integer out=null;
					while(rs.next()) 
					{
						out=rs.getInt("ID_USU");
					}
					return out;
				}catch (Exception e) 
				{
					logger.debug("error del dao USUARIO",e);
					PercistenExcepcion pers=new PercistenExcepcion(e);
					pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
					pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
					throw pers;
				}finally 
				{
				 this.close(rs);
				 this.close(stat);
				 this.close(con);
				}
			}
	
	
	
	public Usuario findbyDocNumero(Integer tipodoc,String numeroDoc)
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.FUCSCONECT);
		PreparedStatement stat=null;
		ResultSet rs=null;
		Usuario us=null;
		java.util.Hashtable<String, Usuario> usuarios=new java.util.Hashtable<String, Usuario>();
		logger.debug("el numer tipo doc "+tipodoc+"el numero documento "+numeroDoc);
		try
		{
			 stat=con.prepareStatement(SELECTBYDOTIP);
			 stat.setInt(1,tipodoc);
			 stat.setString(2,numeroDoc);
			 rs=stat.executeQuery();
			while(rs.next()) 
		 	{
				if (!usuarios.isEmpty()) us=usuarios.get(rs.getInt("TIPO_DOC")+rs.getString("NUMERO_DOC"));
		 		if(null==us) 
		 		{
		 		us=new Usuario();
		 		us.setIdusu(rs.getLong("ID_USU"));
		 		
		 		us.setEmail(rs.getString("EMAIL"));
		 		us.setNumerodoc(rs.getString("NUMERO_DOC"));
		 		//us.setGenero(rs.getInt("GENERO"));
		 		//rs.getTimestamp("FECHA_NACIMIENTO");
		 		//us.setDireccion(rs.getString("DIRECCION"));
		 		//us.setNumeroCel(rs.getString("NUMEROCEL"));
		 		us.setPassword(rs.getString("password"));
		 		us.setStatus(rs.getInt("STATUS"));
		 		Calendar cal=Calendar.getInstance();
		 		cal.setTimeInMillis(rs.
		 				getTimestamp("DATE_CREATE").getTime());
		 		us.setDateCreated(cal);
		 		us.setName(rs.getInt("TIPO_DOC")+us.getNumerodoc());
		 		Rol rol=DAORol.roles.get(rs.getInt("ROLCODE"));
		 		Permiso perm=DAOPermiso.permisos.get(rs.getInt("PERCODE"));
		 		rol.addPermis(perm.getId(),perm);
		 		us.usuarioaddRol(rol.getId(), rol);
		 		us.setTipoDoc(DAOTipoDocumento.tipoDoc.get(rs.getInt("TIPO_DOC")));
		 		usuarios.put(us.getName(),us);
		 		}
		 		else
		 		{
		 			Rol rol=DAORol.roles.get(rs.getInt("ROLCODE"));
			 		Permiso perm=DAOPermiso.permisos.get(rs.getInt("PERCODE"));
			 		rol.addPermis(perm.getId(),perm);
			 		us.usuarioaddRol(rol.getId(), rol);
			 		usuarios.put(us.getName(),us);
			 		logger.debug("consult oel usuario" +us.getName()+"jsjsj"+us.getNumerodoc());
		 		}
		 	}	
		}catch (Exception e) 
		{
			PercistenExcepcion perx=new PercistenExcepcion(e);
			logger.debug("error del dao USUARIO",e);
			perx.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			perx.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw perx;
		}
		Conexion.close(rs);
	 	Conexion.close(stat);
	 	Conexion.close(con);
		return usuarios.get(us.getName());
	}
	
	
	public Usuario findbyDocNumeroUnBind(Integer tipodoc,String numeroDoc)
			throws PercistenExcepcion
			{
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				ResultSet rs=null;
				Usuario us=null;
				java.util.Hashtable<String, Usuario> usuarios=new java.util.Hashtable<String, Usuario>();
				logger.debug("el numer tipo doc "+tipodoc+"el numero documento "+numeroDoc);
				try
				{
					 stat=con.prepareStatement(SELECTBYDOTIPNOSTATUS);
					 stat.setInt(1,tipodoc);
					 stat.setString(2,numeroDoc);
					 rs=stat.executeQuery();
					while(rs.next()) 
				 	{
						if (!usuarios.isEmpty()) us=usuarios.get(rs.getInt("TIPO_DOC")+rs.getString("NUMERO_DOC"));
				 		if(null==us) 
				 		{
				 		us=new Usuario();
				 		us.setIdusu(rs.getLong("ID_USU"));
				 		
				 		us.setEmail(rs.getString("EMAIL"));
				 		us.setNumerodoc(rs.getString("NUMERO_DOC"));
				 		//us.setGenero(rs.getInt("GENERO"));
				 		//rs.getTimestamp("FECHA_NACIMIENTO");
				 		//us.setDireccion(rs.getString("DIRECCION"));
				 		//us.setNumeroCel(rs.getString("NUMEROCEL"));
				 		us.setPassword(rs.getString("password"));
				 		us.setStatus(rs.getInt("STATUS"));
				 		Calendar cal=Calendar.getInstance();
				 		cal.setTimeInMillis(rs.
				 				getTimestamp("DATE_CREATE").getTime());
				 		us.setDateCreated(cal);
				 		us.setName(rs.getInt("TIPO_DOC")+us.getNumerodoc());
				 		Rol rol=DAORol.roles.get(rs.getInt("ROLCODE"));
				 		Permiso perm=DAOPermiso.permisos.get(rs.getInt("PERCODE"));
				 		rol.addPermis(perm.getId(),perm);
				 		us.usuarioaddRol(rol.getId(), rol);
				 		us.setTipoDoc(DAOTipoDocumento.tipoDoc.get(rs.getInt("TIPO_DOC")));
				 		usuarios.put(us.getName(),us);
				 		}
				 		else
				 		{
				 			Rol rol=DAORol.roles.get(rs.getInt("ROLCODE"));
					 		Permiso perm=DAOPermiso.permisos.get(rs.getInt("PERCODE"));
					 		rol.addPermis(perm.getId(),perm);
					 		us.usuarioaddRol(rol.getId(), rol);
					 		usuarios.put(us.getName(),us);
					 		logger.debug("consult oel usuario" +us.getName()+"jsjsj"+us.getNumerodoc());
				 		}
				 	}	
				}catch (Exception e) 
				{
					PercistenExcepcion perx=new PercistenExcepcion(e);
					logger.debug("error del dao USUARIO",e);
					perx.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
					perx.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
					throw perx;
				}
				Conexion.close(rs);
			 	Conexion.close(stat);
			 	Conexion.close(con);
				return usuarios.get(us.getName());
			}
	
	public Usuario validateAlert(Integer tipodoc,String numeroDoc)
			throws PercistenExcepcion
			{
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				ResultSet rs=null;
				Usuario us=null;
				java.util.Hashtable<String, Usuario> usuarios=new java.util.Hashtable<String, Usuario>();
				logger.debug("tipo de documento"+tipodoc+" numero doc"+numeroDoc);
				try
				{
					 stat=con.prepareStatement(VALIDATEALERT);
					 stat.setInt(1,tipodoc);
					 stat.setString(2,numeroDoc);
					 rs=stat.executeQuery();
					while(rs.next()) 
				 	{
						if (!usuarios.isEmpty()) us=usuarios.get(rs.getInt("TIPO_DOC")+rs.getString("NUMERO_DOC"));
				 		if(null==us) 
				 		{
				 		us=new Usuario();
				 		us.setIdusu(rs.getLong("ID_USU"));
				 		
				 		us.setEmail(rs.getString("EMAIL"));
				 		us.setNumerodoc(rs.getString("NUMERO_DOC"));
				 		//us.setGenero(rs.getInt("GENERO"));
				 		//rs.getTimestamp("FECHA_NACIMIENTO");
				 		//us.setDireccion(rs.getString("DIRECCION"));
				 		//us.setNumeroCel(rs.getString("NUMEROCEL"));
				 		us.setPassword(rs.getString("password"));
				 		us.setStatus(rs.getInt("STATUS"));
				 		Calendar cal=Calendar.getInstance();
				 		cal.setTimeInMillis(rs.
				 				getTimestamp("DATE_CREATE").getTime());
				 		us.setDateCreated(cal);
				 		us.setName(rs.getInt("TIPO_DOC")+us.getNumerodoc());
				 		Rol rol=DAORol.roles.get(rs.getInt("ROLCODE"));
				 		Permiso perm=DAOPermiso.permisos.get(rs.getInt("PERCODE"));
				 		rol.addPermis(perm.getId(),perm);
				 		us.usuarioaddRol(rol.getId(), rol);
				 		usuarios.put(us.getName(),us);
				 		}
				 		else
				 		{
				 			Rol rol=DAORol.roles.get(rs.getInt("ROLCODE"));
					 		Permiso perm=DAOPermiso.permisos.get(rs.getInt("PERCODE"));
					 		rol.addPermis(perm.getId(),perm);
					 		us.usuarioaddRol(rol.getId(), rol);
					 		usuarios.put(us.getName(),us);
					 		logger.debug("consultando");
				 		}
				 	}	
				}catch (Exception e) 
				{
					PercistenExcepcion perx=new PercistenExcepcion(e);
					logger.debug("error del dao USUARIO",e);
					perx.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
					perx.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
					throw perx;
				}
				Conexion.close(rs);
			 	Conexion.close(stat);
			 	Conexion.close(con);
				return usuarios.get(us.getName());
			}
	
	
	
	public boolean resetPass(Integer idUsu,String newpass)
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.DEFAULTCONECT);
		PreparedStatement stat=null;
		try 
		{
			stat=con.prepareStatement(CHANGEPASS);
			stat.setString(1,newpass);
			stat.setInt(2, idUsu);
			return  stat.executeUpdate()>0?true:false;
		}catch (Exception e) 
		{
			logger.debug("error del dao USUARIO",e);
			PercistenExcepcion pers=new PercistenExcepcion(e);
			pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw pers;
		}finally 
		{
			this.close(stat);
			this.close(con);
		}
	}
	
	public java.util.List<Usuario> findUserNoMonitor()
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.DEFAULTCONECT);
		PreparedStatement stat=null;
		ResultSet rs=null;
		java.util.List<Usuario> usuarios=new java.util.ArrayList<Usuario>();
		try 
		{
			stat=con.prepareStatement(SELECTUSERNOMON);
			rs=stat.executeQuery();
			while(rs.next()) 
			{
				Usuario us=new Usuario();
				us.setIdusu(rs.getLong("ID_USU"));
				us.setTipoDoc(DAOTipoDocumento.tipoDoc.get(rs.getInt("TIPO_DOC")));
				us.setNumerodoc(rs.getString("NUMERO_DOC"));
				Rol rol=DAORol.roles.get(rs.getInt("ID_ROL"));
				java.util.Map<Integer,Rol>roles=new java.util.HashMap<Integer, Rol>();
				roles.put(rol.getId(),rol);
				us.setRoles(roles);
				usuarios.add(us);
			}
			return usuarios;
		}catch (Exception e) 
		{
			logger.debug("error del dao USUARIO",e);
			PercistenExcepcion pers=new PercistenExcepcion(e);
			pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw pers;
		}finally 
		{
			this.close(con);
			this.close(stat);
			this.close(rs);
		}
	}
	
	
	public java.util.List<Usuario> findUserMonitor()
	throws PercistenExcepcion
			{
				Connection con=this.getConection(Constans.DEFAULTCONECT);
				PreparedStatement stat=null;
				ResultSet rs=null;
				java.util.List<Usuario> usuarios=new java.util.ArrayList<Usuario>();
				try 
				{
					stat=con.prepareStatement(SELECTUSERMON);
					rs=stat.executeQuery();
					while(rs.next()) 
					{
						Usuario us=new Usuario();
						us.setIdusu(rs.getLong("ID_USU"));
						us.setTipoDoc(DAOTipoDocumento.tipoDoc.get(rs.getInt("TIPO_DOC")));
						us.setNumerodoc(rs.getString("NUMERO_DOC"));
						us.setEmail(rs.getString("IP"));
						Rol rol=DAORol.roles.get(rs.getInt("ID_ROL"));
						java.util.Map<Integer,Rol>roles=new java.util.HashMap<Integer, Rol>();
						roles.put(rol.getId(),rol);
						us.setRoles(roles);
						usuarios.add(us);
					}
					return usuarios;
				}catch (Exception e) 
				{
					logger.debug("error del dao USUARIO",e);
					PercistenExcepcion pers=new PercistenExcepcion(e);
					pers.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
					pers.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
					throw pers;
				}finally 
				{
					this.close(con);
					this.close(stat);
					this.close(rs);
				}
			}
	

}
