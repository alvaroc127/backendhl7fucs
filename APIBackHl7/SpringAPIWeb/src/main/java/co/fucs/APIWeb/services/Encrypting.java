package co.fucs.APIWeb.services;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.rosource.ResourceInternal;
import com.fucs.commo.web.util.Constans;

public class Encrypting 
{
	
	private static final Logger logger = LoggerFactory.getLogger(Encrypting.class);
	
	private String varcripanddecrip;
	
	private String secretKey;
	
	private ResourceInternal reso;
	
	
	public Encrypting() 
	{
		reso=ResourceInternal.getInstance();
		this.secretKey=reso.getProperty(Constans.KEY_WEB, Constans.KEY_SECRETCIFRED);
	}
	
	public String encrypt()
	throws BussinesExcepcion
	{
		logger.debug("inicio del cifrado ");
		StringBuilder str=new StringBuilder();
		byte[] varencrip=varcripanddecrip.getBytes();
		byte [] secretkey=secretKey.getBytes();
		byte [] keyrand=new byte[16];
		SecureRandom secu=new SecureRandom();
		secu.nextBytes(keyrand);
		SecretKeySpec secre=new SecretKeySpec(secretkey,"AES");
		IvParameterSpec randovec=new IvParameterSpec(keyrand);
		try 
		{
			Cipher crip=Cipher.getInstance("AES/CBC/PKCS5Padding");
			crip.init(Cipher.DECRYPT_MODE,secre,randovec);
			byte []cifredWord=new byte[crip.getOutputSize(varencrip.length)];
			int outcifred=crip.update(secretkey, 0, secretkey.length, cifredWord, 0);
			outcifred+=crip.doFinal(cifredWord, outcifred);
			str.append(Base64.getEncoder().encodeToString(cifredWord))
			.append("FFF_FFF_FFF")
			.append(Base64.getEncoder().encodeToString(keyrand));
			logger.debug(" fin del cifrado ");
			return str.toString();
		}catch (Exception e) 
		{
			logger.debug(" error en el cifrado del cifrado ",e);
			BussinesExcepcion bues=new BussinesExcepcion(e);
			bues.setCodigo(Constans.ERROR.ERROR_BUSSINES.getCodigo());
			bues.setMensaje(Constans.ERROR.ERROR_BUSSINES.getMensaje());
			throw bues;
		}
	}
	
	
	
	private String decrypt()
	throws BussinesExcepcion
	{
		String varVect;
		int i=0;
		String[] cifredvec=new String[2];
		Scanner scan=new Scanner(this.varcripanddecrip);
		scan.useDelimiter("F{3}_F{3}_F{3}");
		try 
		{
			while(scan.hasNext() && i< 2) 
			{
				cifredvec[i]=scan.next();
				i++;
			}
			scan.close();
			
		}catch (Exception e) 
		{
			BussinesExcepcion buss=new BussinesExcepcion(e);
			buss.setCodigo(Constans.ERROR.ERROR_BUSSINES.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_BUSSINES.getMensaje());
			throw buss;
		}
		this.varcripanddecrip=cifredvec[0];
		varVect=cifredvec[1];
		
		byte[]keyrand=Base64.getDecoder().decode(varVect);
		byte[]crypvar=Base64.getDecoder().decode(varcripanddecrip);
		byte[] secretk=this.secretKey.getBytes();
		SecretKeySpec secrepKey=new SecretKeySpec(secretk,"AES");
		IvParameterSpec randVec=new IvParameterSpec(keyrand);
		try 
		{
			Cipher cip=Cipher.getInstance("AES/CBC/PKCS5Padding");
			cip.init(Cipher.DECRYPT_MODE,secrepKey,randVec);
			byte[] decrip=new byte[cip.getOutputSize(crypvar.length)];
			int descrypsize=cip.update(crypvar, 0, crypvar.length, decrip, 0);
			descrypsize+=cip.doFinal(decrip, descrypsize);
			return new String(decrip);
		}catch (Exception e) 
		{
			BussinesExcepcion buss=new BussinesExcepcion(e);
			buss.setCodigo(Constans.ERROR.ERROR_BUSSINES.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_BUSSINES.getMensaje());
			throw buss;
		}
	}


	public String getVarcripanddecrip() {
		return varcripanddecrip;
	}



	public void setVarcripanddecrip(String varcripanddecrip) {
		this.varcripanddecrip = varcripanddecrip;
	}


	public String getSecretKey() {
		return secretKey;
	}


	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}



	
	

}
