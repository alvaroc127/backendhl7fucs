package co.fucs.APIWeb.security;

import java.util.Collection;
import java.util.Set;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fucs.commo.web.pojo.Rol;
import com.fucs.commo.web.pojo.Sesion;
import com.fucs.commo.web.pojo.Usuario;

/*
 * de este genero una nueva instancia para cada autenticacion
 */
public class UserAuthentic extends UsernamePasswordAuthenticationToken implements UserDetails
{

	private  Sesion se;
	
	public UserAuthentic(Object principal, Object credentials) {
		super(principal, credentials);
		// TODO Auto-generated constructor stub
	}
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -7280918875634014744L;

	
	//aqui retorna los permisos del usuario, estos se comparan contra los del xml
	@Override
	public Collection<GrantedAuthority> getAuthorities() 
	{
		java.util.List<GrantedAuthority> roles=new java.util.ArrayList<>();
		java.util.Map<Integer,Rol> map=this.getSe().getUs().getRoles();
		Set<Integer> set=this.getSe().getUs().getRoles().keySet();
		SimpleGrantedAuthority ro;
		for(Integer key:set) 
		{
			Rol rol=map.get(key);
			ro=new SimpleGrantedAuthority("ROLE_"+rol.getDescrip());
			roles.add(ro);
		}
		 return roles;
	}

	@Override
	public boolean isAuthenticated() 
	{
		return se.getStatus()==1;
	}
	
	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException 
	{
	 if(isAuthenticated ==true) 
	 {
		 se.setStatus(1);
	 }
	}

	public Sesion getSe() {
		return se;
	}


	public void setSe(Sesion se) {
		this.se = se;
	}
	@Override
	public String getPassword() 
	{
	return "{noop}"+se.getIdsesion();
	}

	@Override
	public String getUsername() 
	{
		return se.getUs().getName();
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired()
	{
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() 
	{
		return se.getUs().getStatus()==1;
	}
	

}
