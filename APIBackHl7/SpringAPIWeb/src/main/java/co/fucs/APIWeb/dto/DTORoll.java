package co.fucs.APIWeb.dto;

import com.fucs.commo.web.pojo.Rol;

public class DTORoll extends DTOBase 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8290283817511205627L;
	
	
	private java.util.Map<Integer,Rol>map;
	
	public DTORoll() {
		// TODO Auto-generated constructor stub
	}

	public java.util.Map<Integer, Rol> getMap() {
		return map;
	}

	public void setMap(java.util.Map<Integer, Rol> map) {
		this.map = map;
	}

	

}
