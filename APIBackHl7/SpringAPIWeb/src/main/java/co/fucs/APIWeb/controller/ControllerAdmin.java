package co.fucs.APIWeb.controller;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.pojo.AuditoriaObject;
import com.fucs.commo.web.util.Constans;

import co.fucs.APIWeb.dto.DTOActiveUser;
import co.fucs.APIWeb.dto.DTOBase;
import co.fucs.APIWeb.dto.DTORegistroUser;
import co.fucs.APIWeb.services.AuditoriaServices;
import co.fucs.APIWeb.services.ServicePersistences;
import co.fucs.APIWeb.util.exception.Util;

@Controller
@RequestMapping(value = "admin",method = RequestMethod.POST)
public class ControllerAdmin 
{
	
	private AuditoriaServices audserv;
	
	private ServicePersistences service;
	
	private static final Logger logger = LoggerFactory.getLogger(ControllerAdmin.class);
	
	
	public ControllerAdmin() 
	{
		// TODO Auto-generated constructor stub
	}



    @Autowired
	public ControllerAdmin(AuditoriaServices audserv,ServicePersistences persis) 
    {
		super();
		this.audserv = audserv;
		this.service=persis;
	}
    
    
    @CrossOrigin("*")
    @RequestMapping(value ="activate",  method = RequestMethod.POST)
    public ResponseEntity<DTOBase>  activarUsuario
    (@RequestHeader(value ="Content-Type", required = true)String contenttype,
     @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
    @RequestBody(required =true)DTOActiveUser body,
    HttpServletRequest req
    		)
    throws BussinesExcepcion
    {
    	logger.debug("inicio de activacion del usuario");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("Activar Usuario");
    	aub.setEventUsu(body.getUsername());
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audserv.saveAuditorioDetall(aub);
    	Util.validateUserActive(body);
    	Util.validateSourceAndUUID(uuid,sources);
    	logger.debug("se todo okey");
    	logger.debug(" nos fuimos de base de datos");
    	try 
    	{
    		ObjectMapper map=new ObjectMapper();
			logger.debug("se recibe el json"+map.writeValueAsString(body));
    	}catch (Exception e)
    	{
			logger.debug("error creando el string json",e);
			throw new BussinesExcepcion(Constans.ERROR.ERROR_BUSSINES.getMensaje(),
    				Constans.ERROR.ERROR_BUSSINES.getCodigo());
		}
    	service.activarUsuuario(body);
    	DTOBase dtobase=new DTOBase();
    	dtobase.setReDate(Util.getCurrentDateString());
    	dtobase.setStatus("00");
    	dtobase.setURI(req.getRequestURI());
    	return new ResponseEntity<DTOBase>(dtobase, HttpStatus.ACCEPTED);
    }
    
    
    @CrossOrigin("*")
    @RequestMapping(value ="unactive",  method = RequestMethod.POST)
    public ResponseEntity<DTOBase>  desactivaUsuario
    (@RequestHeader(value ="Content-Type", required = true)String contenttype,
     @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
    @RequestBody(required =true)DTOActiveUser body,
    HttpServletRequest req
    		)
    throws BussinesExcepcion
    {
    	logger.debug("inicio de unactive del usuario");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("Activar unactive");
    	aub.setEventUsu(body.getUsername());
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audserv.saveAuditorioDetall(aub);
    	Util.validateUserActive(body);
    	Util.validateSourceAndUUID(uuid,sources);
    	logger.debug("se todo okey");
    	logger.debug(" nos fuimos de base de datos");
    	service.unactiveUser(body);
    	DTOBase dtobase=new DTOBase();
    	dtobase.setReDate(Util.getCurrentDateString());
    	dtobase.setStatus("00");
    	dtobase.setURI(req.getRequestURI());
    	return new ResponseEntity<DTOBase>(dtobase, HttpStatus.ACCEPTED);
    }
    
    
    @CrossOrigin("*")
    @RequestMapping(value ="changpass",  method = RequestMethod.POST)
    public ResponseEntity<DTOBase>  changpass
    (@RequestHeader(value ="Content-Type", required = true)String contenttype,
     @RequestHeader(value ="UUID", required = true)String uuid,
     @RequestHeader(value ="source",required =true)String sources,
    @RequestBody(required =true)DTORegistroUser body,
    HttpServletRequest req
    		)
    throws BussinesExcepcion
    {
    	logger.debug("inicio de change passs del usuario");
    	AuditoriaObject aub=new AuditoriaObject();
    	aub.setDate(Calendar.getInstance());
    	aub.setEvento1("changpass");
    	aub.setEventUsu(String.valueOf(body.getTipodocumento())+body.getDocumento());
    	aub.setIp(req.getRemoteAddr());
    	aub.setUrl(req.getRequestURI());
    	audserv.saveAuditorioDetall(aub);
    	if(Util.isNullOEmpty(String.valueOf(body.getTipodocumento())+body.getDocumento())) 
    	{
    		throw new BussinesExcepcion(Constans.ERROR.ERROR_BUSSINES.getMensaje(),
    				Constans.ERROR.ERROR_BUSSINES.getCodigo());
    		
    	}
    	Util.validateSourceAndUUID(uuid,sources);
    	logger.debug("se todo okey");
    	logger.debug(" nos fuimos de base de datos");
    	service.changePassword(String.valueOf(body.getTipodocumento())+body.getDocumento(), body.getPassword());
    	DTOBase dtobase=new DTOBase();
    	dtobase.setReDate(Util.getCurrentDateString());
    	dtobase.setStatus("00");
    	dtobase.setURI(req.getRequestURI());
    	return new ResponseEntity<DTOBase>(dtobase, HttpStatus.ACCEPTED);
    }
	
	
	

}
