package co.fucs.APIWeb.dto;

import com.fucs.commo.web.pojo.TipoDocumento;

public class DTOTipoDoc extends DTOBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = -243072105244386621L;
	
	
	public DTOTipoDoc() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	private java.util.Map<Integer,TipoDocumento> tipodocs;


	public java.util.Map<Integer, TipoDocumento> getTipodocs() {
		return tipodocs;
	}



	public void setTipodocs(java.util.Map<Integer, TipoDocumento> tipodocs) {
		this.tipodocs = tipodocs;
	}
	
	
	

}
