package co.fucs.APIWeb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.jboss.logging.Logger;

import co.fucs.APIWeb.dao.BaseDAO;

import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.TipoDocumento;
import com.fucs.commo.web.util.Constans;



public class DAOTipoDocumento extends BaseDAO
{
	
	private final static String SELECTALLDOC ="SELECT ID_DOC,STATUS,DESCRIP FROM dbo.TIPO_DOC";
	
	private static final Logger logger =
			Logger.getLogger(DAOTipoDocumento.class);
	
	public static java.util.Map<Integer,TipoDocumento>tipoDoc=new java.util.Hashtable<>();
	
	private static DAOTipoDocumento DAOTipo=null;
	
	private DAOTipoDocumento() {
		super();
	// TODO Auto-generated constructor stub
	}
	
	
	public static DAOTipoDocumento getInstance()
	throws PercistenExcepcion
	{
		if(null == DAOTipo) 
		{
			DAOTipo=new DAOTipoDocumento();
			DAOTipo.findDocumentType();
		}
		return DAOTipo;
	}
	
	
	
	private  void findDocumentType()
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.FUCSCONECT);
		PreparedStatement stat=null;
		ResultSet rs=null;

		try 
		{
		 stat=con.prepareStatement(SELECTALLDOC);
		 rs=stat.executeQuery();
		 	while(rs.next()) 
		 	{
		 		TipoDocumento doc=new TipoDocumento();
		 		doc.setIdtipdoc(rs.getInt("ID_DOC"));
		 		doc.setDescrip(rs.getString("DESCRIP"));
		 		doc.setStatus(rs.getInt("STATUS"));
		 		tipoDoc.put(doc.getIdtipdoc(),doc);
		 	}	
		}catch (Exception e) 
		{
		logger.debug("error en la consulta ",e);
		e.printStackTrace();
		}
		Conexion.close(con);
	 	Conexion.close(stat);
	 	Conexion.close(rs);

	}
	
	
	

}
