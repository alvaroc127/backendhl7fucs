package co.fucs.APIWeb.dto;

public class DTOActiveMontUser extends DTOBase 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -828409082423208023L;

	private String ipMonitor;
	
	private String username;
	
	public DTOActiveMontUser() {
		// TODO Auto-generated constructor stub
	}

	public String getIpMonitor() {
		return ipMonitor;
	}

	public void setIpMonitor(String ipMonitor) {
		this.ipMonitor = ipMonitor;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	

}
