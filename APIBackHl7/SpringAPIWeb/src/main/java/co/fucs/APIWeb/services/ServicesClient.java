package co.fucs.APIWeb.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.pojo.Alertas;
import com.fucs.commo.web.pojo.Paciente;
import com.fucs.commo.web.pojo.Signal;
import com.fucs.commo.web.pojo.Usuario;
import com.fucs.commo.web.util.Constans;

import co.fucs.APIWeb.clients.ClientFhirp;
import co.fucs.APIWeb.util.exception.Util;

@Service
public class ServicesClient 
{
	
	private static final Logger logger = LoggerFactory.getLogger(ServicesClient.class);
	
	
	public ServicesClient() 
	{
		// TODO Auto-generated constructor stub
	}
	
	
	
	public Paciente consulSignalPatient(Usuario us,String date)
	throws BussinesExcepcion
	{
		ClientFhirp clint=new ClientFhirp();
		java.util.Calendar datecon=null;
		try {
		datecon=Util.getDateStringToCalendar(date);
		}catch (Exception e) 
		{
			logger.debug("converti la fecha no se pudo convertir");
			datecon=Util.getDateStringToCalendarspc(date);
		}
		logger.debug("converti la fecha");
		java.util.List<Signal> list=null;
		try 
		{
		list=clint.consultarSignals(String.valueOf(us.getIdusu()),datecon);
		}catch (Exception e) 
		{
			logger.debug("error",e);
			BussinesExcepcion bus=new BussinesExcepcion(e);
			bus.setCodigo(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getMensaje());
			throw bus;
		}
		Paciente pac=new Paciente();
		pac.setName(us.getName());
		pac.setIdusu(us.getIdusu());
		pac.setNumerodoc(us.getNumerodoc());
		pac.setTipoDoc(us.getTipoDoc());
		pac.setSignals(list);
		return pac;
	}
	
	
	public java.util.List<Alertas> consultAl(Usuario us,String date)
			throws BussinesExcepcion
			{
				ClientFhirp clint=new ClientFhirp();
				java.util.Calendar datecon=Util.getDateStringToCalendar(date);
				java.util.List<Alertas> list=null;
				try 
				{
				list=clint.consultarAlarms(String.valueOf(us.getIdusu()), datecon);
				return list;
				}catch (Exception e)
				{
					BussinesExcepcion bus=new BussinesExcepcion(e);
					bus.setCodigo(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getCodigo());
					bus.setMensaje(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getMensaje());
					throw bus;
				} 
			}
	

}
