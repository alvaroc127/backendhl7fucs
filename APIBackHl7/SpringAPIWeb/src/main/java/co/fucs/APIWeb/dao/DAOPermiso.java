package co.fucs.APIWeb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.fucs.APIWeb.dao.BaseDAO;
import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.Permiso;
import com.fucs.commo.web.util.Constans;



public class DAOPermiso extends BaseDAO
{
	
	private  static final  String SELECTALLPERMI ="SELECT ID_PER,STATUS,NOMBRE FROM dbo.PERMISOS";
	
	private static final Logger logger = LoggerFactory.getLogger(DAOPermiso.class);

	
	public static Map<Integer,Permiso> permisos; 
	
	private static DAOPermiso permiso;
	 
	 
	
	private DAOPermiso() {
		super();
		
	// TODO Auto-generated constructor stub
	}
	
	
	
	public static DAOPermiso getDAOPermiso() 
	throws PercistenExcepcion
	{
		if(null==permiso) 
		{
			permiso=new DAOPermiso();
			permisos=new Hashtable<Integer,Permiso>();
			permiso.findPermis();
		}
		return permiso;
	}
	
	public void findPermis()
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.FUCSCONECT);
		PreparedStatement stat=null;
		ResultSet rs=null;
		try 
		{
		 stat=con.prepareStatement(SELECTALLPERMI);
		 rs=stat.executeQuery();
		 	while(rs.next()) 
		 	{
		 		Permiso per=new Permiso();
		 		per.setId(rs.getInt("ID_PER"));
		 		per.setEstado(rs.getInt("STATUS"));
		 		per.setDescrip(rs.getString("NOMBRE"));
		 		permisos.put(per.getId(),per);
		 	}
		}catch (Exception e) 
		{
		logger.debug("error en la conexion",e);
		PercistenExcepcion perex=new PercistenExcepcion(e);
		perex.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
		perex.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
		throw perex;
		}
		Conexion.close(rs);
	 	Conexion.close(stat);
	 	Conexion.close(con);
	}
	
	
	

}
