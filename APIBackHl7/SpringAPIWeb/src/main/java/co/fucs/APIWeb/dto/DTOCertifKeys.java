package co.fucs.APIWeb.dto;

import java.util.Map;
import java.util.List;

public class DTOCertifKeys extends DTOBase 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2257247793130259844L;

	public DTOCertifKeys() {
		// TODO Auto-generated constructor stub
	}
	
	private  List<DTOKey> keys;

	public List<DTOKey> getKeys() {
		return keys;
	}

	public void setKeys(List<DTOKey> keys) {
		this.keys = keys;
	}
	

}
