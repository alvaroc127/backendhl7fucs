package co.fucs.APIWeb.security;




import java.io.IOException;
import java.security.interfaces.RSAKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.filter.GenericFilterBean;

import com.auth0.jwk.Jwk;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fucs.commo.web.util.Constans;

import co.fucs.APIWeb.dto.DTOCertifKeys;
import co.fucs.APIWeb.dto.DTOKey;
import co.fucs.APIWeb.security.client.ClientSecurity;
import co.fucs.APIWeb.security.client.HttpMiniGrawp;
import co.fucs.APIWeb.security.interfac.BeanFilterInterf;

public class BeanFilter extends GenericFilterBean  implements BeanFilterInterf{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2786248382191468723L;
	
	private static final Logger logger = LoggerFactory.getLogger(BeanFilter.class);
	
	private DTOCertifKeys certikey;
	
	private BeanValidateTokenSession tokensesion;
	
	public BeanFilter() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	public BeanFilter(BeanValidateTokenSession tokensesion) {
		super();
		this.tokensesion = tokensesion;
	}




	//solo saco el toke Barear y coloco el usuario y password nuevamente en el Autorization para 
	 // que el lo sigua trabajando normal hacia adentro. 
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException 
	{
		logger.debug("inicio del filter ");
		HttpMiniGrawp req=new HttpMiniGrawp((HttpServletRequest)request);
	
		
		String token=req.getHeader("Authorization");
		if(null == token) 
		{
		 chain.doFilter(request, response);
		 return ;
		}
		
		logger.debug("inicio del filter "+req.getRequestURI());
		logger.debug("inicio del filter " +req.getContextPath());
		if(req.getRequestURI().equals(req.getContextPath()+"/login/user") ||  req.getRequestURI().equals("403") || req.getRequestURI().equals("404"))
		{
			logger.debug(" me voy chaaou ");
			chain.doFilter(request, response);
			return;
		}
		StringBuilder credentialsbuil=new StringBuilder();
		 try
		 {
			 DecodedJWT decode =JWT.decode(token.replace("Bearer","").trim());
			 logger.debug(" inicio el proceso de decode");
			 DTOKey key= this.certikey.getKeys().get(0);
			 java.util.Map<String,Object> has=new Hashtable<String, Object>();
			 has.put("n", key.getN());
			 has.put("e",key.getE());
			 Jwk jwk=new Jwk(key.getKid(), key.getKty(), key.getAlg(), key.getUse(),
					 new java.util.ArrayList<String>(),null, key.getX5c(),key.getX5t(),has);	 		 
		 RSAPublicKey rsa= (RSAPublicKey) jwk.getPublicKey();
		 Algorithm algo=Algorithm.RSA256(rsa, null);
		 algo.verify(decode);
		 logger.debug(decode.getHeader());
		 logger.debug(decode.getPayload());
		 String user=decode.getClaim("preferred_username").asString();
		 String pass=Constans.INTERKEY;
		 logger.debug("el usuario es "+user);
		 logger.debug("el usuario es "+pass);// llmar el al bean vlaidate token sesion
		 UserDetails auth=tokensesion.loadUserByUsername(user);
		 pass=auth.getPassword();
		 logger.debug("el usuario es "+pass);// llmar el al bean vlaidate token sesion
		 SecurityContextHolder.getContext().setAuthentication((Authentication)auth);
		 credentialsbuil.append(user)
		 .append(":").append(pass);
		 String credentials=Base64.getEncoder().encodeToString(credentialsbuil.toString().getBytes());
		 req.addParameter("Authorization","Basic "+credentials);
		 logger.debug(credentials);
		 chain.doFilter(req, response);
		 }catch (Exception e) 
		 {
			 logger.debug("ocurrio un error en la validacion",e);
			 logger.debug("se da inicio al public key");
			 logger.debug(" error redireccionado a  403 ");
			 if(e instanceof UsernameNotFoundException)
			 {
				request.getRequestDispatcher("/403").forward(request, response);
				chain.doFilter(request, response);
				return;
			 }
			 logger.debug(" error redireccionado a  403 ");
			 //request.getRequestDispatcher("403");
			 chain.doFilter(request, response);
			 return;
		 }
	}

	@Override
	public void init() 
	{
		ClientSecurity client=new ClientSecurity();
		if(certikey ==null) 
		{
			certikey=new DTOCertifKeys();
			if(client.getKeyKeyCloack() instanceof DTOCertifKeys)
			{
				certikey.setKeys(((DTOCertifKeys)client.getKeyKeyCloack()).getKeys());
				certikey.setReDate(((DTOCertifKeys)client.getKeyKeyCloack()).getReDate());
				certikey.setStatus(((DTOCertifKeys)client.getKeyKeyCloack()).getStatus());
			} 
		}
	}

	public DTOCertifKeys getCertikey() {
		return certikey;
	}

	public void setCertikey(DTOCertifKeys certikey) {
		this.certikey = certikey;
	}
		

}
