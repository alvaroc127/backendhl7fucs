package co.fucs.APIWeb.security.client;

import java.util.Calendar;

import org.apache.taglibs.standard.lang.jstl.Constants;
import org.keycloak.authorization.client.AuthorizationDeniedException;
import org.keycloak.authorization.client.AuthzClient;
import org.keycloak.authorization.client.util.HttpResponseException;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.authorization.AuthorizationRequest;
import org.keycloak.representations.idm.authorization.AuthorizationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import com.fucs.commo.web.excepcion.ExceptionIntegration;
import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.AuditoriaObject;
import com.fucs.commo.web.rosource.ResourceInternal;
import com.fucs.commo.web.util.Constans;

import co.fucs.APIWeb.dao.DAOAuditoria;
import co.fucs.APIWeb.dto.DTOBase;
import co.fucs.APIWeb.dto.DTOCertifKeys;
import co.fucs.APIWeb.dto.DTOLoginUser;
import co.fucs.APIWeb.dto.DTOSession;
import co.fucs.APIWeb.services.AuditoriaServices;
import co.fucs.APIWeb.util.exception.Util;

public class ClientSecurity 
{
	
	private static final Logger logger = LoggerFactory.getLogger(ClientSecurity.class);
	
	private ResourceInternal resour;
	
	
	public ClientSecurity() 
	{
		this.resour=ResourceInternal.getInstance();
	}


	public ResourceInternal getResour() {
		return resour;
	}


	public void setResour(ResourceInternal resour) {
		this.resour = resour;
	}
	
	
	public DTOBase generateVASSesionLogin(DTOLoginUser login) 
	throws ExceptionIntegration
	{
		logger.debug("iniciando el consume del login ");
		AuthzClient client=AuthzClient.create();
		logger.debug(" el cliente fue creado ");
		AccessTokenResponse resp=null;
	//AuthorizationRequest req=new AuthorizationRequest();
	
		try {
		 resp=
		client.obtainAccessToken(login.getUsername(), login.getPassword());
		}catch (Exception e) 
		{
			logger.debug("ocurrio este error",e);
			if(e instanceof HttpResponseException) 
			{
				HttpResponseException responex=(HttpResponseException)e;
				logger.debug("obtengo un error por clave o usuario ");
				AuditoriaObject audi=new AuditoriaObject
				(login.getURI(),Calendar.getInstance(),"localhost",String.valueOf(responex.getStatusCode()),responex.getMessage(),login.getUsername());
						DAOAuditoria dao=new DAOAuditoria();
						try 
						{
						dao.saveAuditoria(audi);
						}catch (PercistenExcepcion peer)
						{
							logger.debug("la auditoria no se pudo almacenar ");
							logger.debug("error",peer);
						}
			}
			ExceptionIntegration exp=new ExceptionIntegration();
			exp.setCodigo(Constans.ERROR.ERROR_AUTENTICACION.getCodigo());
			exp.setMensaje(Constans.ERROR.ERROR_AUTENTICACION.getMensaje());
			throw  exp;
		}
		logger.debug("obtengo respuesta correcta");
		
		if(null!=resp.getError()&& !resp.getError().isEmpty()) 
		{
			AuditoriaObject audi=new AuditoriaObject
			(login.getURI(),Calendar.getInstance(),"localhost",resp.getError() ,resp.getErrorDescription(),login.getUsername());
			DAOAuditoria dao=new DAOAuditoria();
			try 
			{
			dao.saveAuditoria(audi);
			}catch (PercistenExcepcion peer)
			{
				logger.debug("la auditoria no se pudo almacenar ");
				logger.debug("error",peer);
			}
			logger.debug("se genero un error en el login producido por el ");
			ExceptionIntegration exp=new ExceptionIntegration();
			exp.setCodigo(Constans.ERROR.ERROR_AUTENTICACION.getCodigo());
			exp.setMensaje(Constans.ERROR.ERROR_AUTENTICACION.getMensaje());
			throw  exp;
		}
		AuditoriaObject audi=new AuditoriaObject
		(login.getURI(),Calendar.getInstance(),"localhost",resp.getScope() ,resp.getTokenType(),login.getUsername());
		DAOAuditoria dao=new DAOAuditoria();
				try 
				{
				dao.saveAuditoria(audi);
				}catch (PercistenExcepcion peer)
				{
					logger.debug("la auditoria no se pudo almacenar ");
					logger.debug("error",peer);
				}
				DTOSession see=new 
				DTOSession(resp.getToken(), resp.getExpiresIn(), resp.getRefreshToken(), resp.getRefreshExpiresIn(),resp.getExpiresIn()/60);
				see.setReDate(Util.getCurrentDateString());
				see.setStatus("00");
				see.setExpire((int)resp.getExpiresIn()/60);
	return see;
	}
	
	
	public DTOBase getKeyKeyCloack() 
	{
		logger.debug("iniciando el consume del login ");
		resour=ResourceInternal.getInstance();
		String httpbase=resour.getProperty(Constans.KEY_WEB, Constans.URLBASEKEYCLOACK);
		logger.debug(" se han cargado los certificados "+httpbase);
		String uri=resour.getProperty(Constans.KEY_WEB, Constans.URLCERTIFKEYS);
		logger.debug(" se han cargado los certificados "+uri);
		logger.debug(" el cliente fue creado ");
		RestTemplate client=new RestTemplate();
		client.setUriTemplateHandler(new DefaultUriBuilderFactory(httpbase));
		DTOCertifKeys certKeys=client.getForObject(uri, DTOCertifKeys.class);
		certKeys.setStatus("00");
		certKeys.setReDate(Util.getCurrentDateString());
		return certKeys;
	}
	
	
	


}
