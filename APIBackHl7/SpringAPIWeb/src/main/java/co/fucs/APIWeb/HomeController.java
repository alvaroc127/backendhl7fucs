package co.fucs.APIWeb;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.pojo.AuditoriaObject;
import com.fucs.commo.web.util.Constans;

import co.fucs.APIWeb.dto.DTOGenericResponse;
import co.fucs.APIWeb.services.AuditoriaServices;
import co.fucs.APIWeb.util.exception.Util;


/**
 * Handles requests for the application home page.
 */
@Controller()
@RequestMapping(value ="/", consumes= {MediaType.ALL_VALUE},
produces = {MediaType.APPLICATION_XHTML_XML_VALUE,MediaType.TEXT_HTML_VALUE,MediaType.APPLICATION_JSON_VALUE})
public class HomeController 
{
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	private AuditoriaServices auser;
	
	public HomeController() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public HomeController(AuditoriaServices auser) 
	{
		this.auser=auser;
	}
	
	
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String requestToGetFrontController(Locale locale, Model model)
	{
		logger.info("Welcome home! The client locale is {}.", locale);	
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		String formattedDate = dateFormat.format(date);
		model.addAttribute("serverTime", formattedDate );
		return "home";
	}
	
	
	@RequestMapping(value ="/",method = RequestMethod.POST)
	public String requestToPostFrontController() 
	{
		logger.debug(" se tiene el post se  validan lo necesario y se procede a redireccionarl ");
		
		return null;
	}
	
	@RequestMapping(value ="404",method = {RequestMethod.GET,RequestMethod.POST},
			consumes = {MediaType.ALL_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE} )
	public @ResponseBody ResponseEntity<DTOGenericResponse> pagNotFountreal(HttpServletRequest  request)
	{
		logger.debug(" Inicio mapgina not fount");
		logger.debug("la pagina no se puedo encontrar ");
		AuditoriaObject obj=new AuditoriaObject();
		DTOGenericResponse genres=new DTOGenericResponse();
		obj.setIp(request.getRemoteAddr());
		logger.debug(" IP DE LA FUENTE " +obj.getIp());
		obj.setUrl(request.getRequestURI());
		logger.debug(" URL DE LA FUENTE " +obj.getUrl());
		obj.setDate(Calendar.getInstance());
		logger.debug(" URL DE LA FUENTE " +obj.getDate());
		logger.debug("se inicia el proceso de auditoria");
		try
		{
		 auser.saveAuditorioa(obj);
		genres.setStatus(String.valueOf(Constans.ERROR.ERROR_PAG_NO_FOUND.getCodigo()));
		genres.setMessage(Constans.ERROR.ERROR_PAG_NO_FOUND.getMensaje());
		}catch (BussinesExcepcion e)
		{
			logger.debug("excepcion en el controlador");
			genres.setMessage(e.getMensaje());
			genres.setReDate(Util.getCurrentDateString());
			genres.setStatus(String.valueOf(e.getCodigo()));
			logger.debug("este es el mensaje ",e);
			logger.debug("excepcion en el finalizador");
		}
		return new ResponseEntity<DTOGenericResponse>(genres, HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(value ="403",method = {RequestMethod.GET,RequestMethod.POST},
			consumes = {MediaType.ALL_VALUE},
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody ResponseEntity<DTOGenericResponse> pagNotAutho(HttpServletRequest  request)
	{
		logger.debug(" Inicio mapgina not fount");
		logger.debug("la pagina no se puedo encontrar ");
		AuditoriaObject obj=new AuditoriaObject();
		DTOGenericResponse genres=new DTOGenericResponse();
		obj.setIp(request.getRemoteAddr());
		logger.debug(" IP DE LA FUENTE " +obj.getIp());
		obj.setUrl(request.getRequestURI());
		logger.debug(" URL DE LA FUENTE " +obj.getUrl());
		obj.setDate(Calendar.getInstance());
		logger.debug(" URL DE LA FUENTE " +obj.getDate());
		logger.debug("se inicia el proceso de auditoria");
		try
		{
		 auser.saveAuditorioa(obj);
		genres.setStatus(String.valueOf(Constans.ERROR.ERROR_PAG_NO_AUT.getCodigo()));
		genres.setMessage(Constans.ERROR.ERROR_PAG_NO_AUT.getMensaje());
		}catch (BussinesExcepcion e)
		{
			logger.debug("excepcion en el controlador");
			genres.setMessage(e.getMensaje());
			genres.setReDate(Util.getCurrentDateString());
			genres.setStatus(String.valueOf(e.getCodigo()));
			logger.debug("este es el mensaje ",e);
			logger.debug("excepcion en el finalizador");
		}
		return new ResponseEntity<DTOGenericResponse>(genres, HttpStatus.FORBIDDEN);
	}
	
	
	
}
