package co.fucs.APIWeb.security.interfac;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.pojo.Sesion;

public interface BeanValidateTokenSessionInt extends UserDetailsService 
{
	public void init();
	
	
	public void validateSesion(Sesion se)
	throws BussinesExcepcion ;


}
