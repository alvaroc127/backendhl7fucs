package co.fucs.APIWeb.controller;

import java.io.Serializable;
import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.pojo.AuditoriaObject;
import com.fucs.commo.web.util.Constans;

import co.fucs.APIWeb.dto.DTOBase;
import co.fucs.APIWeb.dto.DTOLoginUser;
import co.fucs.APIWeb.dto.DTOSession;
import co.fucs.APIWeb.security.UserAuthentic;
import co.fucs.APIWeb.services.AuditoriaServices;
import co.fucs.APIWeb.services.SecurityValidation;
import co.fucs.APIWeb.util.exception.Util;


@Controller
@RequestMapping(value = "login")
public class ControllerLogin implements Serializable
{
	private static final Logger logger = LoggerFactory.getLogger(ControllerLogin.class);
	
	private AuditoriaServices auditoria;
	
	private SecurityValidation security;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2970791009789772114L;

	public ControllerLogin() 
	{
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public ControllerLogin(AuditoriaServices auds,SecurityValidation secu) 
	{
		auditoria=auds;
		this.security=secu;
		// TODO Auto-generated constructor stub
	}
	
	
	
	//@CrossOrigin(origins=Constans.CONST_CORS_QA)
	@CrossOrigin(origins="*")
	@PostMapping(value ="/user")
	public @ResponseBody ResponseEntity<Object> loginUser(/*@RequestHeader(value ="User-Agent",required =true)String User,*/
			@RequestHeader(value ="Content-Type", required = true)String contenttype,
			@RequestHeader(value ="UUID", required = true)String uuid,
			@RequestHeader(value ="source",required =true)String sources,
			@RequestBody DTOLoginUser loginuser,
			HttpServletRequest httpreq)
	throws BussinesExcepcion
	{
		logger.debug("inicio de administracion ");
		logger.debug("opcion ip " + httpreq.getLocalAddr());
		logger.debug(" ell UUID " +uuid);
		logger.debug(" el sources "+ sources);
		try {
		ObjectMapper obj=new ObjectMapper();
		logger.debug("se recibe el request "+ obj.writeValueAsString(loginuser));
		}catch (Exception e) {
		logger.debug("error ",e
				);
		}
		AuditoriaObject aud =new AuditoriaObject(httpreq.getRequestURI(),Calendar.getInstance(),httpreq.getLocalAddr());
		aud.setEventUsu(loginuser.getUsername());
		this.auditoria.saveAuditorioDetall(aud);
		boolean sou2=sources.equals(Constans.VALID_SOURCES2);
		boolean sou1=sources.equals(Constans.VALID_SOURCES1);
		boolean noconti=false;
		if(sou2) {noconti=true;}
		if(sou1) {noconti=true;}
		if(!noconti) 
		{	
			logger.debug(" la fuente de inicio no es valida");
			BussinesExcepcion bussi=new BussinesExcepcion(Constans.ERROR.ERROR_FUENTE_INVALIDA.getMensaje(),
					Constans.ERROR.ERROR_FUENTE_INVALIDA.getCodigo());
			throw bussi;
		}
		DTOSession sesion=this.security.validateAndStartSesionClient(loginuser);
		return new ResponseEntity<Object>(sesion,HttpStatus.OK);
	}
	
	
	@CrossOrigin(origins="*")
	@PostMapping(value ="/logout")
	public @ResponseBody ResponseEntity<Object> logout(/*@RequestHeader(value ="User-Agent",required =true)String User,*/
			@RequestHeader(value ="Content-Type", required = true)String contenttype,
			@RequestHeader(value ="UUID", required = true)String uuid,
			@RequestHeader(value ="source",required =true)String sources,
			@RequestBody DTOLoginUser loginuser,
			HttpServletRequest httpreq)
	throws BussinesExcepcion
	{
		logger.debug("inicio de administracion ");
		logger.debug("opcion ip " + httpreq.getLocalAddr());
		logger.debug(" ell UUID " +uuid);
		logger.debug(" el sources "+ sources);
		try {
		ObjectMapper obj=new ObjectMapper();
		logger.debug("se recibe el request "+ obj.writeValueAsString(loginuser));
		}catch (Exception e) {
		logger.debug("error ",e
				);
		}
		AuditoriaObject aud =new AuditoriaObject(httpreq.getRequestURI(),Calendar.getInstance(),httpreq.getLocalAddr());
		aud.setEventUsu(loginuser.getUsername());
		aud.setEvento2("logout");
		this.auditoria.saveAuditorioDetall(aud);
		boolean sou2=sources.equals(Constans.VALID_SOURCES2);
		boolean sou1=sources.equals(Constans.VALID_SOURCES1);
		boolean noconti=false;
		if(sou2) {noconti=true;}
		if(sou1) {noconti=true;}
		if(!noconti) 
		{	
			logger.debug(" la fuente de inicio no es valida");
			BussinesExcepcion bussi=new BussinesExcepcion(Constans.ERROR.ERROR_FUENTE_INVALIDA.getMensaje(),
					Constans.ERROR.ERROR_FUENTE_INVALIDA.getCodigo());
			throw bussi;
		}
			this.security.logout(loginuser);
			DTOBase dtobase=new DTOBase();
			dtobase.setStatus("00");
			dtobase.setReDate(Util.getCurrentDateString());
			dtobase.setURI(httpreq.getRequestURI());
		return new ResponseEntity<Object>(dtobase,HttpStatus.OK);
	}
	
	
}
