package co.fucs.APIWeb.dto;

import java.io.Serializable;

import com.fucs.commo.web.pojo.Usuario;

public class DTOUsers extends DTOBase implements Serializable 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1247135092641995105L;
	
	
	java.util.List<Usuario> usuarios;


	public java.util.List<Usuario> getUsuarios() {
		return usuarios;
	}


	public void setUsuarios(java.util.List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	
	


}
