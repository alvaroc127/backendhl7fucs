package co.fucs.APIWeb.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.accept.ContentNegotiationManagerFactoryBean;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

//@EnableWebSecurity
@Configuration
@ComponentScan(basePackages = {"co.fucs.APIWeb"})
@Import(co.fucs.APIWeb.config.ConfigSeg.class)
public class SpringConfig  implements WebMvcConfigurer
{
	@Bean
    public ViewResolver viewResolver() 
	{
		ContentNegotiationManagerFactoryBean contentNegotiationManager = new ContentNegotiationManagerFactoryBean();
	    contentNegotiationManager.addMediaType("json", MediaType.APPLICATION_JSON);

	    InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
	    viewResolver.setPrefix("/WEB-INF/jsp/");
	    viewResolver.setSuffix(".jsp");

	    MappingJackson2JsonView defaultView = new MappingJackson2JsonView();
	    defaultView.setExtractValueFromSingleKeyModel(true);

	    ContentNegotiatingViewResolver contentViewResolver = new ContentNegotiatingViewResolver();
	    contentViewResolver.setContentNegotiationManager(contentNegotiationManager.getObject());
	    contentViewResolver.setViewResolvers(Arrays.<ViewResolver> asList(viewResolver));
	    contentViewResolver.setDefaultViews(Arrays.<View> asList(defaultView));
	    return contentViewResolver;
    }
	

	
	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) 
	{
		// TODO Auto-generated method stub
		configurer.favorPathExtension(false).
        favorParameter(true).
        parameterName("mediaType").
        ignoreAcceptHeader(true).
        defaultContentType(MediaType.APPLICATION_JSON).
        mediaType("xml", MediaType.APPLICATION_XML).
        mediaType("json", MediaType.APPLICATION_JSON);
	}
	
	

}
