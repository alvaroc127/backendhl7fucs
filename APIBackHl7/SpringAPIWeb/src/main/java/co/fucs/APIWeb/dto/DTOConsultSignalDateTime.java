package co.fucs.APIWeb.dto;

public class DTOConsultSignalDateTime  extends DTOBase
{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8931750558339928056L;


	String username;
	private  String ip;
	
	
	
	
	String dateTime;
	
	public DTOConsultSignalDateTime() {
		// TODO Auto-generated constructor stub
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getDateTime() {
		return dateTime;
	}


	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}


	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}
	
	

}
