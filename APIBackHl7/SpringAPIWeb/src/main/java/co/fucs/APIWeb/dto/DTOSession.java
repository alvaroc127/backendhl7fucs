package co.fucs.APIWeb.dto;

import java.util.List;

import com.fucs.commo.web.pojo.Rol;



public class DTOSession extends DTOBase 
{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5274998645560910735L;

	private String accesToken;
	
	private long timexpireSecond;
	
	private String refreshToken;
	
	private long refreshexp;
		
	private long timexpirMin;
	
	private List<Rol> roles;
	
	private Integer expire;
	
	
	public DTOSession() {
		// TODO Auto-generated constructor stub
	}

	
	

	public DTOSession(String accesToken, long timexpireSecond, String refreshToken, long refreshexp, long timexpirMin) {
		super();
		this.accesToken = accesToken;
		this.timexpireSecond = timexpireSecond;
		this.refreshToken = refreshToken;
		this.refreshexp = refreshexp;
		this.timexpirMin = timexpirMin;
	}




	public String getAccesToken() {
		return accesToken;
	}


	public void setAccesToken(String accesToken) {
		this.accesToken = accesToken;
	}


	public long getTimexpireSecond() {
		return timexpireSecond;
	}


	public void setTimexpireSecond(long timexpireSecond) {
		this.timexpireSecond = timexpireSecond;
	}


	public String getRefreshToken() {
		return refreshToken;
	}


	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}


	public long getRefreshexp() {
		return refreshexp;
	}


	public void setRefreshexp(long refreshexp) {
		this.refreshexp = refreshexp;
	}


	public long getTimexpirMin() {
		return timexpirMin;
	}


	public void setTimexpirMin(long timexpirMin) {
		this.timexpirMin = timexpirMin;
	}




	public List<Rol> getRoles() {
		return roles;
	}




	public void setRoles(List<Rol> roles) {
		this.roles = roles;
	}




	public Integer getExpire() {
		return expire;
	}




	public void setExpire(Integer expire) {
		this.expire = expire;
	}
	
}
