package co.fucs.APIWeb.util.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value =HttpStatus.NOT_FOUND, reason="No such Order")
public class ExceptionResponseNotFound extends RuntimeException 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8260536667187902529L;
	
	private Integer status=404;
	
	private String message;

	public ExceptionResponseNotFound() 
	{
		this.message="Pagina no encontrada";
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	


}
