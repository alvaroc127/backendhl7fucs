package co.fucs.APIWeb.dto;

public class DTOLoginUser  extends DTOBaseUser
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8905112051371606986L;

	private String registerDay;
	
	private Integer counttryLogin;
	
	public DTOLoginUser() {
		// TODO Auto-generated constructor stub
	}

	public String getRegisterDay() {
		return registerDay;
	}

	public void setRegisterDay(String registerDay) {
		this.registerDay = registerDay;
	}

	public Integer getCounttryLogin() {
		return counttryLogin;
	}

	public void setCounttryLogin(Integer counttryLogin) {
		this.counttryLogin = counttryLogin;
	}


	
	@Override
	public String toString() 
	{
		StringBuilder str=new StringBuilder();
		str.append("\"registryDay\":")
		.append(this.registerDay)
		.append(",")
		.append("\"counttryLogin\":")
		.append(this.counttryLogin);
		return str.toString();
	}
}
