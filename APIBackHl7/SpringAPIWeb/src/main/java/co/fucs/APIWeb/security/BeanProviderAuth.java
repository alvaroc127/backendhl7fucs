package co.fucs.APIWeb.security;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class BeanProviderAuth implements AuthenticationProvider{

	private BeanValidateTokenSession validate;
	
	private static final Logger logger = LoggerFactory.getLogger(BeanProviderAuth.class);
	
	public BeanProviderAuth() {
		// TODO Auto-generated constructor stub
	}
	
	
	public BeanProviderAuth(BeanValidateTokenSession validate )
	{
		this.validate=validate;
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException 
	{
		try 
		{
			logger.debug("estoy entrando para authenticar algo "+authentication.getName());
			logger.debug("estoy entrando para authenticar algo "+authentication.getCredentials());
			Authentication ath=(Authentication)validate.loadUserByUsername(authentication.getName());
			SecurityContextHolder.getContext().setAuthentication(ath);
			return ath;
		}catch (Exception e) 
		{
			UsernameNotFoundException exp=(UsernameNotFoundException)e;
			throw new AuthenticationCredentialsNotFoundException(exp.getMessage(), e);
		}
		
	}

	@Override
	public boolean supports(Class<?> authentication) 
	{
		return true; 
	}

}
