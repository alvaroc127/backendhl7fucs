package co.fucs.APIWeb.dto;

import java.io.Serializable;

public class DTOAlertas extends DTOBase implements Serializable 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6540463047849748642L;
	
	java.util.List<DTOAlert> alerts;
	
	
	
	
	
	
	public java.util.List<DTOAlert> getAlerts() {
		return alerts;
	}






	public void setAlerts(java.util.List<DTOAlert> alerts) {
		this.alerts = alerts;
	}






	public class DTOAlert 
	{
		
		private String horaAlerta;
		
		private Integer severidad;
		
		private String descripcion;
		
		private Integer idalert;

		public String getHoraAlerta() {
			return horaAlerta;
		}

		public void setHoraAlerta(String horaAlerta) {
			this.horaAlerta = horaAlerta;
		}

		public Integer getSeveridad() {
			return severidad;
		}

		public void setSeveridad(Integer severidad) {
			this.severidad = severidad;
		}

		public String getDescripcion() {
			return descripcion;
		}

		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}

		public Integer getIdalert() {
			return idalert;
		}

		public void setIdalert(Integer idalert) {
			this.idalert = idalert;
		}
		
		public DTOAlert() {
			// TODO Auto-generated constructor stub
		}
		
	}
	
	

}
