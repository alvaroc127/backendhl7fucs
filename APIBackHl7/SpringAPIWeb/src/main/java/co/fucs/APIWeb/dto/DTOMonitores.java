package co.fucs.APIWeb.dto;

public class DTOMonitores extends DTOBase {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4357487415336066822L;
	
	private java.util.Map<String,Integer> map;
	
	public DTOMonitores() {
		// TODO Auto-generated constructor stub
	}

	public java.util.Map<String, Integer> getMap() {
		return map;
	}

	public void setMap(java.util.Map<String, Integer> map) {
		this.map = map;
	}
	
}
