package co.fucs.APIWeb.services;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import co.fucs.APIWeb.dao.DAOMonitor;
import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.Rol;
import com.fucs.commo.web.pojo.TipoDocumento;
import com.fucs.commo.web.pojo.Usuario;
import com.fucs.commo.web.util.Constans;

import co.fucs.APIWeb.dao.DAORol;
import co.fucs.APIWeb.dao.DAOTipoDocumento;
import co.fucs.APIWeb.dao.DAOUsuario;
import co.fucs.APIWeb.dto.DTOActiveMontUser;
import co.fucs.APIWeb.dto.DTOActiveUser;
import co.fucs.APIWeb.dto.DTOBase;
import co.fucs.APIWeb.dto.DTORegistroUser;
import co.fucs.APIWeb.util.exception.Util;


@Service
public class ServicePersistences 
{
	
	
	private static final Logger logger = LoggerFactory.getLogger(ServicePersistences.class);
	
	
	
	
	
	public ServicePersistences() 
	{
		try 
		{
		logger.debug("se cargan los tipos de documentos.");
		DAOTipoDocumento.getInstance();
		}catch (PercistenExcepcion e) 
		{
			logger.debug("error en la persistencia ",e);
		}
	}
	
	
	
	
	public void registrarUsuario(DTORegistroUser us)
	throws BussinesExcepcion
	{
		Usuario user=createUsuario(us);
		DAOUsuario daous=new DAOUsuario();
		try 
		{
		logger.debug("  vamos a crear el usuario ");
		daous.createUser(user);
		logger.debug(" inser ejecutradop ");
		Integer idusu=daous.findbyDocPreEnroll(user.getTipoDoc().getIdtipdoc(), user.getNumerodoc());
		user.setIdusu((long)idusu);
		daous.createRolUs(idusu, Util.getKeyforDefatulRol());
		}catch (PercistenExcepcion e) 
		{
			logger.debug("  se encontro el siguiente error",e);
			BussinesExcepcion buss=new BussinesExcepcion(e);
			buss.setCodigo(e.getCodigo());
			buss.setMensaje(e.getMensaje());
			throw buss;
		}	
	}
	
	
	private Usuario createUsuario(DTORegistroUser us) 
	throws BussinesExcepcion
	{
		Usuario user=new Usuario();
		
		user.setNumerodoc(us.getDocumento());
		user.setDireccion(us.getDireccion());
		user.setNumeroCel(us.getNumeroCelular());
		TipoDocumento tipodoc=DAOTipoDocumento.tipoDoc.get(us.getTipodocumento());
		user.setTipoDoc(tipodoc);
		user.setFechaNacimiento(Util.getStringToDate(us.getFechaNacimiento()));
		user.setPassword(us.getPassword());
		return user;
	}
	
	
	private Usuario createUsuario(String username) 
	{
		logger.debug("se procede a la instancia del dto al pojo model");
		Usuario user=new Usuario();
		user.setName(username);
		Integer out= Character.getNumericValue(username.charAt(0));
		TipoDocumento doc=DAOTipoDocumento.tipoDoc.get(out);
		user.setTipoDoc(doc);
		user.setNumerodoc(username.substring(1));
		return user;
	}
	
	
	private Usuario createUsuario(DTOBase us) 
	{
		logger.debug("se procede a la instancia del dto al pojo model");
		Usuario user=new Usuario();
		if(us instanceof DTOActiveUser)
		{
			DTOActiveUser actu=(DTOActiveUser) us;
			user.setName(actu.getUsername());
			Integer out= Character.getNumericValue(actu.getUsername().charAt(0));
			TipoDocumento doc=DAOTipoDocumento.tipoDoc.get(out);
			user.setTipoDoc(doc);
			user.setNumerodoc(actu.getUsername().substring(1));
			if(null != actu.getRoles() && !actu.getRoles().isEmpty())
			{
			 for(Rol rol: actu.getRoles()) 
			 {
				user.getRoles().put(rol.getId(),rol); 
			 }
			}
			return user;
		}
		if(us instanceof DTOActiveMontUser) 
		{
			DTOActiveMontUser actu1=(DTOActiveMontUser) us;
			user.setName(actu1.getUsername());
			Integer out= Character.getNumericValue(actu1.getUsername().charAt(0));
			TipoDocumento doc=DAOTipoDocumento.tipoDoc.get(out);
			user.setTipoDoc(doc);
			user.setNumerodoc(actu1.getUsername().substring(1));
			return user;
		}
		return null;
	}
	
	
	public void activarUsuuario(DTOActiveUser actvu) 
	throws BussinesExcepcion
	{
		logger.debug("inicio de activacion del usuario");
		Usuario us=createUsuario(actvu);
		Integer usid=null;
		if(null==us) 
		{
		BussinesExcepcion bus=new BussinesExcepcion();
		bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
		bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
		throw bus;
		}
		DAOUsuario dau=new DAOUsuario();
		try 
		{
		usid=dau.findbyDocPreEnroll(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (PercistenExcepcion pers) 
		{
			logger.debug("ocurrio un error activando el usuario",pers);
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(pers.getCodigo());
			bus.setMensaje(pers.getMensaje());
			throw bus;
		}
		if(null ==usid)
		{
			logger.debug("  no se pudo encontrar el usuario");
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		us.setIdusu((long)usid);
		java.util.Set<Integer>keyset=us.getRoles().keySet();
		try 
		{
			for(Integer key: keyset) 
			{
			dau.createRolUs(new BigDecimal(us.getIdusu()).intValue(),key);
			}
			if(!dau.updateStateActive(us)) 
			{
				logger.debug("no se pudo realizar ");
				throw new PercistenExcepcion("error no update",-500);
			}
		}catch (PercistenExcepcion persp) 
		{
			logger.debug("ocurrio un erro en el update ",persp);
			BussinesExcepcion bus=new BussinesExcepcion(persp);
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
	}
	
	public void unactiveUser(DTOActiveUser activUser)
	throws BussinesExcepcion
	{
		logger.debug("inicio de desactivacion del usuario");
		Usuario us=createUsuario(activUser);
		boolean out=false;
		if(null==us) 
		{
		BussinesExcepcion bus=new BussinesExcepcion();
		bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
		bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
		throw bus;
		}
		DAOUsuario dau=new DAOUsuario();
		try 
		{
		us=dau.findbyDocNumero(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (PercistenExcepcion pers) 
		{
			logger.debug("ocurrio un error activando el usuario",pers);
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(pers.getCodigo());
			bus.setMensaje(pers.getMensaje());
			throw bus;
		}
		if(null ==us)
		{
			logger.debug("  no se pudo encontrar el usuario");
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		try 
		{
			out=dau.deleteRolUser(new BigDecimal(us.getIdusu()).intValue());
		}catch (PercistenExcepcion e)
		{
			logger.debug("  no se pudo encontrar el usuario",e);
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		if(false==out) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
	}
	
	public void bindMonitorUser(DTOActiveMontUser monuser)
	throws BussinesExcepcion
	{
		logger.debug("inicio de bind  del usuario usuario");
		Usuario us=createUsuario(monuser);
		java.util.Map<String,Integer>mon=null;
		Integer idUsu=null;
		Integer bindMon=null;
		boolean result=false;
		if(null==us) 
		{
		BussinesExcepcion bus=new BussinesExcepcion();
		bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
		bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
		throw bus;
		}
		DAOMonitor monda=new DAOMonitor();
		try 
		{
			mon= monda.findMonitor(monuser.getIpMonitor());
		}catch (Exception e) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		if(null == mon) 
			{
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;
			}
		DAOUsuario dau=new DAOUsuario();
		try 
		{
			idUsu=dau.findbyDocPreEnroll(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (Exception e) 
		{
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;
		}
		if(null == idUsu) 
		{
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;	
		}
		us.setIdusu(new BigDecimal(idUsu).longValue());
		try 
		{
		bindMon=monda.findUserBind(idUsu);
		}catch (Exception e) 
		{
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;
		}
		if(null != bindMon) 
		{
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;
		}
		try 
		{
		result=monda.createBindMontUser(mon.get(monuser.getIpMonitor()), idUsu);
		}
		catch (Exception e) 
		{
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;
		}
		if(!result) 
		{
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;
		}
		
	}
	
	
	public void unbindMont(DTOActiveMontUser actmon) 
			throws BussinesExcepcion
			{
				logger.debug(" inicio desvicular un dispostivo ");
				Usuario us=createUsuario(actmon);
				DAOMonitor daomon=new DAOMonitor();
				DAOUsuario daousu=new DAOUsuario();
				java.util.Map<String,Integer> mon=null;
				boolean isUnbind=false;
				try 
				{
				mon=daomon.findMonitor(actmon.getIpMonitor());
				}catch (Exception e) 
				{
					BussinesExcepcion buss= new BussinesExcepcion();
					buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
					buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
					throw buss;
				}	
				if(null == mon) 
				{
					BussinesExcepcion buss= new BussinesExcepcion();
					buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
					buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
					throw buss;
				}
				try 
				{
					us=daousu.findbyDocNumeroUnBind(us.getTipoDoc().getIdtipdoc() ,us.getNumerodoc());
				}catch (PercistenExcepcion e) 
				{
					BussinesExcepcion buss= new BussinesExcepcion();
					buss.setCodigo(Constans.ERROR.ERROR_BUSSINES.getCodigo());
					buss.setMensaje(Constans.ERROR.ERROR_BUSSINES.getMensaje());
					throw buss;
				}
				if(null==us) 
				{
					BussinesExcepcion buss= new BussinesExcepcion();
					buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
					buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
					throw buss;
				}
				try
				{
				isUnbind=daomon.unBindMontUser(mon.get(actmon.getIpMonitor()),new BigDecimal(us.getIdusu()).intValue());
				}catch (Exception e) 
				{
					BussinesExcepcion buss= new BussinesExcepcion();
					buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
					buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
					throw buss;
				}
				if(!isUnbind) 
				{
					BussinesExcepcion buss= new BussinesExcepcion();
					buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
					buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
					throw buss;
				}
			}
	
	public java.util.Map<String,Integer> findMonitorAvible() 
	throws BussinesExcepcion
	{
		logger.debug(" inicio de la consulta de monitor ");
		DAOMonitor daomon=new DAOMonitor();
		java.util.Map<String, Integer> monitores=null;
		try 
		{
		monitores=daomon.findMonitorall();
		}catch (Exception e) 
		{
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;
		}
		if(null == monitores) 
		{
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;
			
		}
		return monitores;
	}
	
	
	public void activeMovil(DTOActiveUser actu)
	throws BussinesExcepcion
	{
		logger.debug(" inicio de activar movil");
		Usuario us= createUsuario(actu);
		boolean out=false;
		if(null==us) 
		{
		BussinesExcepcion bus=new BussinesExcepcion();
		bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
		bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
		throw bus;
		}
		DAOUsuario dau=new DAOUsuario();
		try 
		{
		us=dau.findbyDocNumero(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (PercistenExcepcion pers) 
		{
			logger.debug("ocurrio un error activando  el movil",pers);
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(pers.getCodigo());
			bus.setMensaje(pers.getMensaje());
			throw bus;
		}
		if(null ==us)
		{
			logger.debug("  no se pudo encontrar el usuario");
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		try
		{
		out=dau.activeMovil(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (Exception e) 
		{
			logger.debug("  no se pudo activar el movil");
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		if(false==out) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;	
		}
	}
	
	
	public void unActiveMovil(DTOActiveUser actu)
	throws BussinesExcepcion
	{
		logger.debug(" inicio de unactive movil");
		Usuario us= createUsuario(actu);
		boolean out=false;
		if(null==us) 
		{
		BussinesExcepcion bus=new BussinesExcepcion();
		bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
		bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
		throw bus;
		}
		DAOUsuario dau=new DAOUsuario();
		try 
		{
		us=dau.findbyDocNumero(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (PercistenExcepcion pers) 
		{
			logger.debug("ocurrio un error desactivar   el movil",pers);
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(pers.getCodigo());
			bus.setMensaje(pers.getMensaje());
			throw bus;
		}
		if(null ==us)
		{
			logger.debug("  no se pudo encontrar el usuario");
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		try
		{
		out=dau.unactiveMovil(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (Exception e) 
		{
			logger.debug("  no se pudo activar el movil");
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		if(false==out) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;	
		}
		
	}
	
	
	public void activeAlert(DTOActiveUser actvaler)
	throws BussinesExcepcion
	{
		logger.debug(" inicio de activar alerta ");
		Usuario us= createUsuario(actvaler);
		boolean out=false;
		if(null==us) 
		{
		BussinesExcepcion bus=new BussinesExcepcion();
		bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
		bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
		throw bus;
		}
		DAOUsuario dau=new DAOUsuario();
		try 
		{
		us=dau.findbyDocNumero(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (PercistenExcepcion pers) 
		{
			logger.debug("ocurrio un error activar la alerta",pers);
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(pers.getCodigo());
			bus.setMensaje(pers.getMensaje());
			throw bus;
		}
		if(null ==us)
		{
			logger.debug("  no se pudo encontrar el usuario");
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		try
		{
		out=dau.activeAlert(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (Exception e) 
		{
			logger.debug("  no se pudo activar la alerta ",e);
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		if(false==out) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;	
		}
		
	}
	
	public void unActiveAlert(DTOActiveUser actvaler)
	throws BussinesExcepcion
	{
		logger.debug(" inicio de unactive alert");
		Usuario us= createUsuario(actvaler);
		boolean out=false;
		if(null==us) 
		{
		BussinesExcepcion bus=new BussinesExcepcion();
		bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
		bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
		throw bus;
		}
		DAOUsuario dau=new DAOUsuario();
		try 
		{
		us=dau.findbyDocNumero(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (PercistenExcepcion pers) 
		{
			logger.debug("ocurrio un error unactive la alerta",pers);
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(pers.getCodigo());
			bus.setMensaje(pers.getMensaje());
			throw bus;
		}
		if(null ==us)
		{
			logger.debug("  no se pudo encontrar el usuario");
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		try
		{
		out=dau.unactiveAlert(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (Exception e) 
		{
			logger.debug(" ocurrio un error unactiver la alerta ");
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		if(false==out) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;	
		}
	}
	
	
	public java.util.Map<Integer,TipoDocumento> getAllTypeDoc()
	throws BussinesExcepcion
	{
		logger.debug(" inicio de la consulta de tipos de documento ");
		 if(null==DAOTipoDocumento.tipoDoc) 
		 {
			 BussinesExcepcion buss= new BussinesExcepcion();
				buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
				buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
				throw buss;
		 }
		return DAOTipoDocumento.tipoDoc;
	}
	
	
	
	public  java.util.Map<Integer,Rol> getAllRoll()
			throws BussinesExcepcion
			{
				logger.debug(" inicio de la consulta de tipos de documento ");
				 if(null==DAORol.roles) 
				 {
					 BussinesExcepcion buss= new BussinesExcepcion();
						buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
						buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
						throw buss;
				 }
				return DAORol.roles;
			}
	
	
	public String isConfigAlertMovil(DTOActiveUser actuser)
	throws BussinesExcepcion
	{
		logger.debug(" validar si esta configurado ");		
		Usuario us= createUsuario(actuser);
		boolean out=false;
		if(null==us) 
		{
		BussinesExcepcion bus=new BussinesExcepcion();
		bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
		bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
		throw bus;
		}
		DAOUsuario dau=new DAOUsuario();
		try 
		{
		us=dau.findbyDocNumero(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (PercistenExcepcion pers) 
		{
			logger.debug("ocurrio un error al validar  la alerta movil",pers);
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(pers.getCodigo());
			bus.setMensaje(pers.getMensaje());
			throw bus;
		}
		if(null ==us)
		{
			logger.debug("  no se pudo encontrar el usuario");
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		try 
		{
		us=dau.validateAlert(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (Exception e) 
		{
			logger.debug("  no se pudo encontrar el validar usuario",e);
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		if(null ==us)
		{
			logger.debug("  no se pudo validar el usuario");
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;
		}
		return "true";
	}
	
	
	public Usuario findPatient(String username)
	throws BussinesExcepcion
	{
		logger.debug(" inicio de findPatient movil");
		Usuario us= createUsuario(username);
		Integer idUsu=null;
		if(null==us) 
		{
		BussinesExcepcion bus=new BussinesExcepcion();
		bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
		bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
		throw bus;
		}
		DAOUsuario dau=new DAOUsuario();
		try 
		{
			idUsu=dau.findbyDocPreEnroll(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (Exception e) 
		{
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;
		}
		if(null == idUsu) 
		{
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;	
		}
		us.setIdusu(new BigDecimal(idUsu).longValue());
		logger.debug("se encontro el paciente ");
		return us;
	}
	
	
	
	public void changePassword(String username,String newpass)
	throws BussinesExcepcion
	{
		
		Usuario us= createUsuario(username);
		boolean resul=false;
		if(null==us) 
		{
		BussinesExcepcion bus=new BussinesExcepcion();
		bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
		bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
		throw bus;
		}
		DAOUsuario dau=new DAOUsuario();
		try 
		{
			us=dau.findbyDocNumero(us.getTipoDoc().getIdtipdoc(), us.getNumerodoc());
		}catch (Exception e) 
		{
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;
		}
		if(null == us) 
		{
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;	
		}
		try 
		{
			resul=dau.resetPass(new BigDecimal(us.getIdusu()).intValue(),newpass);
		}
		catch (Exception e) 
		{
			logger.debug("se genero un erro en reset",e);
			BussinesExcepcion buss= new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw buss;	
		}
		if(false==resul) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			throw bus;	
		}
	}
	
	
	
	public java.util.List<Usuario> findUsersNoMont()
	throws BussinesExcepcion
	{
		DAOUsuario dao=new DAOUsuario();
		java.util.List<Usuario> uss=null;
		try 
		{
		uss=dao.findUserNoMonitor();
		}catch(PercistenExcepcion pee) 
		{
			logger.debug("ocurrio un error ",pee);
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(pee.getCodigo());
			bus.setMensaje(pee.getMensaje());
			throw bus;
		}
		if(null==uss || uss.isEmpty()) 
		{
			logger.debug("no se encontraron usuario para asignar monitores");
			BussinesExcepcion buss=new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_BUSSINES.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_BUSSINES.getMensaje());
			throw buss;
		}
		return uss;
	}
	
	
	public java.util.List<Usuario> findUserMon()
	throws BussinesExcepcion
	{
		DAOUsuario dao=new DAOUsuario();
		java.util.List<Usuario> uss=null;
		try 
		{
		uss=dao.findUserMonitor();
		}catch(PercistenExcepcion pee) 
		{
			logger.debug("ocurrio un error ",pee);
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(pee.getCodigo());
			bus.setMensaje(pee.getMensaje());
			throw bus;
		}
		if(null==uss || uss.isEmpty()) 
		{
			logger.debug("no se encontraron usuario para asignar monitores");
			BussinesExcepcion buss=new BussinesExcepcion();
			buss.setCodigo(Constans.ERROR.ERROR_BUSSINES.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_BUSSINES.getMensaje());
			throw buss;
		}
		return uss;
	}
	
	
	public java.util.Map<String,Integer> findUserMonbyIp(String ip)
			throws BussinesExcepcion
			{
				DAOMonitor dao=new DAOMonitor();
				java.util.Map<String,Integer> uss=null;
				try 
				{
				uss=dao.findMonitor(ip);
				}catch(PercistenExcepcion pee) 
				{
					logger.debug("ocurrio un error ",pee);
					BussinesExcepcion bus=new BussinesExcepcion();
					bus.setCodigo(pee.getCodigo());
					bus.setMensaje(pee.getMensaje());
					throw bus;
				}
				if(null==uss || uss.isEmpty()) 
				{
					logger.debug("no se encontraron usuario para asignar monitores");
					BussinesExcepcion buss=new BussinesExcepcion();
					buss.setCodigo(Constans.ERROR.ERROR_BUSSINES.getCodigo());
					buss.setMensaje(Constans.ERROR.ERROR_BUSSINES.getMensaje());
					throw buss;
				}
				return uss;
			}
	
	

}
