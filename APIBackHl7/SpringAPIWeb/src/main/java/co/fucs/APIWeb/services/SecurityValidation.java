package co.fucs.APIWeb.services;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.stereotype.Service;

import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.excepcion.ExceptionIntegration;
import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.Rol;
import com.fucs.commo.web.pojo.Sesion;
import com.fucs.commo.web.pojo.Usuario;
import com.fucs.commo.web.util.Constans;

import co.fucs.APIWeb.controller.ControllerLogin;
import co.fucs.APIWeb.dao.DAOPermiso;
import co.fucs.APIWeb.dao.DAORol;
import co.fucs.APIWeb.dao.DAOSession;
import co.fucs.APIWeb.dao.DAOUsuario;
import co.fucs.APIWeb.dto.DTOLoginUser;
import co.fucs.APIWeb.dto.DTOSession;
import co.fucs.APIWeb.security.UserAuthentic;
import co.fucs.APIWeb.security.client.ClientSecurity;
import co.fucs.APIWeb.util.exception.Util;

@Service
public class SecurityValidation 
{
	
	
	private AuditoriaServices audiserv;
	
	private NoOpPasswordEncoder nopass;
	
	private static final Logger logger = LoggerFactory.getLogger(SecurityValidation.class);

	public SecurityValidation() 
	{
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	SecurityValidation(AuditoriaServices audiserv,NoOpPasswordEncoder nopass)
	{
		this.audiserv=audiserv;
		this.nopass=nopass;
	}
	
	
	
	public DTOSession validateAndStartSesionClient(DTOLoginUser dtologin) 
	throws BussinesExcepcion
	{
		boolean isv=false;
		DTOSession sesion=new DTOSession();
		Usuario us=null;
		logger.debug("validate sesion inicio");
		if(Util.isNullOEmpty(dtologin))
		{
			logger.debug(" el objeto es null");
			BussinesExcepcion busex= 
			new BussinesExcepcion(Constans.ERROR.ERROR_AUTENTICACION.getMensaje(), 
					Constans.ERROR.ERROR_AUTENTICACION.getCodigo());
			throw busex;
		}
		
		if(Util.isNullOEmpty(dtologin.getPassword()) || Util.isNullOEmpty(dtologin.getUsername()))
		{
			logger.debug(" pass o usuario es null o vacio");
			logger.debug(" pass o usuario es null o vacio" + dtologin.getPassword());
			logger.debug(" pass o usuario es null o vacio" + dtologin.getUsername());
			BussinesExcepcion busex= 
			new BussinesExcepcion(Constans.ERROR.ERROR_AUTENTICACION.getMensaje(), 
					Constans.ERROR.ERROR_AUTENTICACION.getCodigo());
			throw busex;
		}
		logger.debug("validate client ");
		DAOSession daose=new DAOSession();
		DAOUsuario daousu=new DAOUsuario();
		Sesion se=new Sesion();
		String salid=dtologin.getUsername();
		Integer tipo=Character.getNumericValue(salid.charAt(0));
		String document=salid.substring(1);
		logger.debug("inicio de consulta ");
		try
		{
		logger.debug(" dao usuario ");
		us=daousu.findbyDocNumero(tipo, document);
		se.setUs(us);
		se.setIdUsuario(new BigDecimal(us.getIdusu()).intValue());
		isv=daose.validateExisteSession(se);
		logger.debug(" se valida la sesion  " +isv);
		}catch (PercistenExcepcion pers) 
		{
			logger.debug("erro en el inicio",pers);
			BussinesExcepcion bus=new BussinesExcepcion(pers);
			bus.setMensaje(Constans.ERROR.ERROR_AUTENTICACION.getMensaje());
			bus.setCodigo(Constans.ERROR.ERROR_AUTENTICACION.getCodigo());
			throw bus;
		}
		 if(isv)
			{
			BussinesExcepcion busex=
					new BussinesExcepcion(Constans.ERROR.ERROR_SESSION_ACTIVA.getMensaje(),
							Constans.ERROR.ERROR_SESSION_ACTIVA.getCodigo());			
				/*try 
				{
					daose.updateEndSession(se);
				}catch (PercistenExcepcion e) 
				{
				logger.debug("erro en el update de sesion",e);
				}*/
			throw busex; 	
			}
		try 
		{
			ClientSecurity client=new ClientSecurity();
			sesion=(DTOSession)client.generateVASSesionLogin(dtologin);
			se.setExpire(sesion.getExpire());
			se.setAccestoken(sesion.getAccesToken());
		} catch (ExceptionIntegration eextin) 
		{
			BussinesExcepcion buss=new BussinesExcepcion(eextin);
			buss.setCodigo(eextin.getCodigo());
			buss.setMensaje(eextin.getMensaje());
			throw buss;
		}
		try 
		{
			daose.insertStartSession(se);
			daose.loadExisteSession(se);
		}catch (PercistenExcepcion e) 
		{
			BussinesExcepcion buss=new BussinesExcepcion(e);
			buss.setCodigo(e.getCodigo());
			buss.setMensaje(e.getMensaje());
			throw buss;
		}
		java.util.Map<Integer,Rol>map=us.getRoles();
		java.util.List<Rol> roles=new java.util.ArrayList<>(); 
		for(Integer key:us.getRoles().keySet()) 
		{
			Rol rol=map.get(key);
			roles.add(rol);
		}
		sesion.setRoles(roles);
		UserAuthentic user=new UserAuthentic(se.getUs().getName(),nopass.encode(String.valueOf(se.getIdsesion())));
		user.setSe(se);
		SecurityContextHolder.getContext().setAuthentication(user);
		return sesion;
	}
	
	
	public void logout(DTOLoginUser dtologin)
	throws BussinesExcepcion
	{
		boolean outlog=false;
		if(Util.isNullOEmpty(dtologin))
		{
			logger.debug(" el objeto es null");
			BussinesExcepcion busex= 
			new BussinesExcepcion(Constans.ERROR.ERROR_AUTENTICACION.getMensaje(), 
					Constans.ERROR.ERROR_AUTENTICACION.getCodigo());
			throw busex;
		}
		
		if(Util.isNullOEmpty(dtologin.getUsername()))
		{
			logger.debug(" pass o usuario es null o vacio");
			logger.debug(" pass o usuario es null o vacio" + dtologin.getUsername());
			BussinesExcepcion busex= 
			new BussinesExcepcion(Constans.ERROR.ERROR_AUTENTICACION.getMensaje(), 
					Constans.ERROR.ERROR_AUTENTICACION.getCodigo());
			throw busex;
		}
		
		DAOSession daose=new DAOSession();
		DAOUsuario daous=new DAOUsuario();
		Integer tipdoc=Character.getNumericValue(dtologin.getUsername().charAt(0));
		Usuario us=null;
		try 
		{
		us=daous.findbyDocNumero(tipdoc,dtologin.getUsername().substring(1));
		}catch (PercistenExcepcion e) 
		{
			logger.debug("error en ",e);
			BussinesExcepcion busex= 
		     new BussinesExcepcion(Constans.ERROR.ERROR_AUTENTICACION.getMensaje(), 
			    Constans.ERROR.ERROR_AUTENTICACION.getCodigo());
			throw busex;
		}
		if(us==null) 
		{
			BussinesExcepcion busex= 
			new BussinesExcepcion(Constans.ERROR.ERROR_AUTENTICACION.getMensaje(), 
			 Constans.ERROR.ERROR_AUTENTICACION.getCodigo());
			throw busex;
		}
		Sesion se=new Sesion();
		se.setIdUsuario(new BigDecimal(us.getIdusu()).intValue());
		try {
			outlog=daose.updateEndSession(se);
		}catch (PercistenExcepcion e) 
		{
			BussinesExcepcion busex= 
					new BussinesExcepcion(Constans.ERROR.ERROR_AUTENTICACION.getMensaje(), 
					 Constans.ERROR.ERROR_AUTENTICACION.getCodigo());
					throw busex;
		}
		if(!outlog) 
		{
			BussinesExcepcion busex= 
					new BussinesExcepcion(Constans.ERROR.ERROR_AUTENTICACION.getMensaje(), 
					 Constans.ERROR.ERROR_AUTENTICACION.getCodigo());
					throw busex;
			
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
