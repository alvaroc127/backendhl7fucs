package co.fucs.APIWeb.controller;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.pojo.AuditoriaObject;
import com.fucs.commo.web.pojo.Rol;
import com.fucs.commo.web.pojo.TipoDocumento;
import com.fucs.commo.web.util.Constans;

import co.fucs.APIWeb.dto.DTOBase;
import co.fucs.APIWeb.dto.DTORegistroUser;
import co.fucs.APIWeb.dto.DTORoll;
import co.fucs.APIWeb.dto.DTOTipoDoc;
import co.fucs.APIWeb.services.AuditoriaServices;
import co.fucs.APIWeb.services.ServicePersistences;
import co.fucs.APIWeb.util.exception.Util;


@CrossOrigin
@Controller
@RequestMapping(value="usuario", method = {RequestMethod.POST,RequestMethod.OPTIONS})
public class ControllerRegistroUsuario 
{
	
	private static final Logger logger = LoggerFactory.getLogger(ControllerRegistroUsuario.class);
	
	private AuditoriaServices serv;
	
	private ServicePersistences persis;
	
	
	public ControllerRegistroUsuario() 
	{
		
	}
	
	@Autowired
	public ControllerRegistroUsuario(AuditoriaServices aud,ServicePersistences serpersis) 
	{
		this.serv=aud;
		this.persis=serpersis;
	}
	
	
	
	
	
	// solo se registra por usuario administrador suerte 
	// en security le digo que solo rol admin tiene acceso a esta ruta.
	//@CrossOrigin(allowedHeaders = "*",origins = "*",allowCredentials = "false",methods= {RequestMethod.POST,RequestMethod.OPTIONS})//@CrossOrigin("*")
	
	@RequestMapping(value="registro", method= {RequestMethod.POST,RequestMethod.OPTIONS})
	public ResponseEntity<DTOBase> registroUsuario 
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
	 @RequestHeader(value ="source",required =true)String sources,
	 @RequestBody(required = true) DTORegistroUser userregis,
	 HttpServletRequest req
	 /*@RequestHeader(value ="User-Agent",required =true)String User,*/)
	throws BussinesExcepcion
	{
		logger.debug("  Inicio de registro de usuario  ");
		AuditoriaObject ob=new AuditoriaObject();
		ob.setDate(Calendar.getInstance());
		ob.setIp(req.getRemoteAddr());
		ob.setUrl(req.getRequestURI());
		ob.setIp(userregis.getDocumento());
		ob.setEventUsu("registroUsuario");
		logger.debug("  Inicio de registro de usuario  ");
		serv.saveAuditorioDetall(ob);
		logger.debug("  Inicio de registro de usuario  ");
		try 
		{
			ObjectMapper map=new ObjectMapper();
			logger.debug("se recibe el json"+map.writeValueAsString(userregis));
		}catch (Exception e) 
		{
			BussinesExcepcion bus=new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_DATOS_INCORRECTOS.getMensaje());
			 throw bus;
		}
		Util.validateUserRegister(userregis);
		Util.validateSourceAndUUID(sources,uuid);
		logger.debug("  pasamos al registro  ");
		persis.registrarUsuario(userregis);
		DTOBase dtorespon=new DTOBase();
		dtorespon.setReDate(Util.getCurrentDateString());
		dtorespon.setStatus("00");
		dtorespon.setURI(req.getRequestURI());
		return new ResponseEntity<DTOBase>(dtorespon,HttpStatus.CREATED);
	}
	
	
	
	@CrossOrigin("*")//@CrossOrigin("*")
	@RequestMapping(value="tipodocs", method= RequestMethod.POST)
	public ResponseEntity<DTOBase> tipdocs 
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
	 @RequestHeader(value ="source",required =true)String sources,
	 HttpServletRequest req
	 /*@RequestHeader(value ="User-Agent",required =true)String User,*/)
	throws BussinesExcepcion
	{
		logger.debug("  Inicio consulta de  tipos documentos  ");
		AuditoriaObject ob=new AuditoriaObject();
		ob.setDate(Calendar.getInstance());
		ob.setIp(req.getRemoteAddr());
		ob.setUrl(req.getRequestURI());
		ob.setEventUsu("tipodoc");
		logger.debug("  Inicio consulta de  tipos documentos  ");
		serv.saveAuditorioDetall(ob);
		logger.debug("  Inicio consulta de  tipos documentos   ");
		
		java.util.Map<Integer,TipoDocumento>map=persis.getAllTypeDoc();
		
		DTOTipoDoc dtorespon=new DTOTipoDoc();
		dtorespon.setReDate(Util.getCurrentDateString());
		dtorespon.setStatus("00");
		dtorespon.setTipodocs(map);
		dtorespon.setURI(req.getRequestURI());
		return new ResponseEntity<DTOBase>(dtorespon,HttpStatus.OK);

	}
	
	
	
	@CrossOrigin("*")//@CrossOrigin("*")
	@RequestMapping(value="roles", method= RequestMethod.POST)
	public ResponseEntity<DTOBase> roles 
	(@RequestHeader(value ="Content-Type", required = true)String contenttype,
	 @RequestHeader(value ="UUID", required = true)String uuid,
	 @RequestHeader(value ="source",required =true)String sources,
	 HttpServletRequest req
	 /*@RequestHeader(value ="User-Agent",required =true)String User,*/)
	throws BussinesExcepcion
	{
	
		logger.debug("  Inicio consulta de  tipos documentos  ");
		AuditoriaObject ob=new AuditoriaObject();
		ob.setDate(Calendar.getInstance());
		ob.setIp(req.getRemoteAddr());
		ob.setUrl(req.getRequestURI());
		ob.setEventUsu("tipodoc");
		logger.debug("  Inicio consulta de  tipos documentos  ");
		serv.saveAuditorioDetall(ob);
		logger.debug("  Inicio consulta de  tipos documentos   ");
		java.util.Map<Integer,Rol>map=persis.getAllRoll();
		
		
		DTORoll dtoroll=new DTORoll();
		dtoroll.setReDate(Util.getCurrentDateString());
		dtoroll.setMap(map);
		dtoroll.setStatus("00");
		dtoroll.setURI(req.getRequestURI());
		return new ResponseEntity<DTOBase>(dtoroll,HttpStatus.OK);
	}

}
