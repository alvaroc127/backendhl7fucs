package co.fucs.APIWeb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import co.fucs.APIWeb.dao.BaseDAO;
import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.Rol;
import com.fucs.commo.web.util.Constans;



public class DAORol extends BaseDAO
{
	
	private final static String SELECTALLROLL ="SELECT ID_ROL,STATUS,NOMBRE FROM dbo.ROL";
	
	private static final Logger logger = LoggerFactory.getLogger(DAORol.class);
	
	public static java.util.Map<Integer,Rol> roles;
	
	private static DAORol daorol;
	
	private DAORol() {
		super();
	// TODO Auto-generated constructor stub
	}
	
	public static DAORol getInstance()
	throws PercistenExcepcion
	{
		if(null==daorol)
		{
			daorol=new DAORol();
			roles=new java.util.Hashtable<Integer,Rol>();
			daorol.findallRoll();
		}
		return daorol;
	}
	
	
	
	
	public void findallRoll()
	throws PercistenExcepcion
	{
		Connection con=this.getConection(Constans.FUCSCONECT);
		PreparedStatement stat=null;
		ResultSet rs=null;
		try 
		{
		 stat=con.prepareStatement(SELECTALLROLL);
		 rs=stat.executeQuery();
		 	while(rs.next()) 
		 	{
		 		Rol rol=new Rol();
		 		rol.setId(rs.getInt("ID_ROL"));
		 		rol.setEstado(rs.getInt("STATUS"));
		 		rol.setDescrip(rs.getString("NOMBRE"));
		 		roles.put(rol.getId(),rol);
		 	}	
		}catch (Exception e) 
		{
			PercistenExcepcion perex=new PercistenExcepcion(e);
			logger.debug("error en la BD",e);
			perex.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			perex.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw perex;
			
		}
		Conexion.close(rs);
	 	Conexion.close(stat);
	 	Conexion.close(con);
	}
	
	
	

}
