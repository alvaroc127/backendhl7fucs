package co.fucs.APIWeb.clients.consumer;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.ejb.ActivationConfigProperty;

import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Patient;
import org.jboss.ejb3.annotation.ResourceAdapter;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fucs.commo.web.pojo.Usuario;
import com.fucs.commo.web.rosource.ResourceInternal;
import com.fucs.commo.web.util.Constans;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.model.api.IResource;
import ca.uhn.fhir.parser.IParser;
import co.fucs.APIWeb.clients.ClientCFM;
import co.fucs.APIWeb.dao.DAOUsuario;






@ResourceAdapter(value = "activemq-remot")
@MessageDriven(name = "ConsumerAlertasMDB",activationConfig = {
		@ActivationConfigProperty(propertyName = "useJNDI",propertyValue = "False"),
		@ActivationConfigProperty(propertyName = "destinationType",propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination",propertyValue = "alertasQueue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode",propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "user",propertyValue = "jmsuser"),
		@ActivationConfigProperty(propertyName = "password",propertyValue = "admin123")
		/*@ActivationConfigProperty(propertyName = "useJNDI",propertyValue = "True")
		@ActivationConfigProperty(propertyName = "jndiParams",
		propertyValue = 
	 "java.naming.factory.initial=org.wildfly.naming.client.WildFlyInitialContextFactory;java.naming.provider.url=http-remoting://127.0.0.1:9090;java.naming.security.principal=jmsuser;java.naming.security.credentials=admin123"
		)*/	
		})
public class ConsumerAlertasMDB implements MessageListener
{

	
	private static final Logger logger = LoggerFactory.getLogger(ConsumerAlertasMDB.class);
	
	private ClientCFM client;
	
	
	public ConsumerAlertasMDB() 
	{
		client=new ClientCFM();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	/**
	 * se recibe el mensaje para ser enviado al front
	 * todo desde esta classe, si se requiere se puede crear un delegado
	 */
	@Override
	public void onMessage(Message message) 
	{
		System.out.println(" se pudo llego un messaje");
		logger.debug("se recibe el mensaje omg");
		try 
		{
			TextMessage mess=(TextMessage)message;
			String salida=mess.getText();
			IParser parse=FhirContext.forR4().newJsonParser();
			Bundle bun= (Bundle) parse.parseResource(salida);
			java.util.Map<String,String> map=new Hashtable<String, String>();
			int i =0;
			for(BundleEntryComponent com:bun.getEntry()) 
			{
				if(com.getResource() instanceof Observation) 
				{
					 Observation ober=(Observation)com.getResource();
					 String id=ober.getId();
					 if(map.containsKey(id)) 
					 {
						 map.put("obser_iden"+id+i++,ober.getValueSampledData().getData().replaceAll("Varies", ""));
					 }else 
					 {
						 map.put(id,ober.getValueSampledData().getData().replaceAll("Varies", "")); 
					 }
				}
				if(com.getResource() instanceof Patient)
				{
					Patient pat=(Patient)com.getResource();
					String id=pat.getId();
					map.put("patient_id", id);
					map.put("montior_ip",pat.getName().get(1).getFamily());
				}
				client.sendMenssageToFCM(map);
				//momento para enviar la cola del mensaje
			} 
		logger.debug("nuevo mensaje recibido");
		}catch(Exception ex) 
		{			
			ex.printStackTrace();
			System.out.println(" ocurrio un error terrible ");
			logger.debug("hay noooo",ex);
		}
	}
	
	

}
 