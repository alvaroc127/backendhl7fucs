package co.fucs.APIWeb.dto;

public class DTOPatientSignal extends DTOBase
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7620286609400354619L;

	
	public DTOPatientSignal() {
		// TODO Auto-generated constructor stub
	}

	private String username;

	
	private java.util.List<String> observations;


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public java.util.List<String> getObservations() {
		return observations;
	}


	public void setObservations(java.util.List<String> observations) {
		this.observations = observations;
	}
	
	
	
}
