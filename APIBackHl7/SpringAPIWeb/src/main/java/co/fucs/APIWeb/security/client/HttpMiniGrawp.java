package co.fucs.APIWeb.security.client;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class HttpMiniGrawp extends HttpServletRequestWrapper 
{

	private Hashtable<String,String> headParam=new Hashtable<>();
	
	public HttpMiniGrawp(HttpServletRequest request) 
	{
		super(request);	
	}
	
	
	
	public void addParameter(String key,String value) 
	{
		this.headParam.put(key, value);
	}
	
	
	
	@Override
	public String getHeader(String name) 
	{
		if(null!=this.headParam.get(name) && null!=super.getHeader(name)) 
		{
			return this.headParam.get(name);
		}
		return super.getHeader(name);
	}
	
	@Override
	public Enumeration<String> getHeaderNames() 
	{
		if(null==super.getHeaderNames()) 
		{
			return null;
		}
		List<String> names= Collections.list(super.getHeaderNames());
		for(String name:this.headParam.keySet()) 
		{
			names.add(name);	
		}
		return Collections.enumeration(names);
	}
	
	@Override
	public Enumeration<String> getHeaders(String name) 
	{
		if (null== super.getHeaders(name))
		{	
			return null;
		}
			List<String>names=Collections.list(super.getHeaders(name));
			if(this.headParam.containsKey(name))
			{
			names.remove(name);
			names.add(this.headParam.get(name));
			}
			
			return Collections.enumeration(names);
	}

}
