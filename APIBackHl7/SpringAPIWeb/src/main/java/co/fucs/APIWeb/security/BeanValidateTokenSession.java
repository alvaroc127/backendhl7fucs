package co.fucs.APIWeb.security;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.pojo.Sesion;
import com.fucs.commo.web.pojo.Usuario;
import com.fucs.commo.web.util.Constans;

import co.fucs.APIWeb.dao.DAOPermiso;
import co.fucs.APIWeb.dao.DAORol;
import co.fucs.APIWeb.dao.DAOSession;
import co.fucs.APIWeb.dao.DAOUsuario;
import co.fucs.APIWeb.security.interfac.BeanValidateTokenSessionInt;


// aqui se debe llamar al metodo para validar sesion y al metodo 
// para sacar los datos de la sesion o validar la session 
// este senioor genera una instacion de  UserAuthentic, lo unico que falta es obtener los parametros del req aqui
public class BeanValidateTokenSession implements BeanValidateTokenSessionInt
{
	private static final Logger logger = LoggerFactory.getLogger(BeanValidateTokenSession.class);
	
	private NoOpPasswordEncoder nopass;
	
	public BeanValidateTokenSession() 
	{
		
	}
	
	public BeanValidateTokenSession(NoOpPasswordEncoder noPass) 
	{
		nopass=noPass;
	}
	

	@Override
	public void init() 
	{
		try 
		{
			
			DAOPermiso.getDAOPermiso();
			DAORol.getInstance();
		}catch (Exception e) 
		{
		logger.debug("no se puedo cargar los roles y permisos ",e);
		}
	}







	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException 
	{
		logger.debug(" se  procede agenerar el usuario ");
		Integer tipodoc=Character.getNumericValue(username.charAt(0));
		String document=username.substring(1);
		logger.debug(" se  consultarra  "+document+" $$ "+tipodoc);
		try {
		DAOUsuario us=new DAOUsuario();
		DAOSession das=new DAOSession();
		Usuario uss=us.findbyDocNumero(tipodoc, document);
		Sesion se=new Sesion();
		se.setUs(uss);
		se.setIdUsuario(new BigDecimal(uss.getIdusu()).intValue());
		boolean isv=das.validateExisteSession(se);
		if(!isv) 
		{
			logger.debug(" se  consultarra  "+isv);
			throw new UsernameNotFoundException(Constans.ERROR.ERROR_SESSION_INVALIDA.getMensaje());
		}
		das.loadExisteSession(se);
		try {
		validateSesion(se);
		}catch (BussinesExcepcion e) 
		{
			logger.debug("error en la validacio nde sesion");
			throw new UsernameNotFoundException(e.getMensaje());
		}
		logger.debug("se enviaraaaa "+ "{noop}"+se.getIdsesion());
		UserAuthentic user=new UserAuthentic(se.getUs().getName(),"{noop}"+se.getIdsesion());
		user.setSe(se);
		
		logger.debug(" retorno user   "+user.getPassword()+"  "+user.getUsername());
		return user;
		}catch (PercistenExcepcion pee) 
		{
			logger.debug("error en bd ",pee);
			throw new UsernameNotFoundException(pee.getMensaje());
		} 
	}



	@Override
	public void validateSesion(Sesion se)
		throws BussinesExcepcion 
	{
		Calendar now=Calendar.getInstance();
		Long diff=now.getTimeInMillis()-se.getDateCreated().getTimeInMillis() ;
		logger.debug(" se actualiza por que la diferencias es  "+ now.getTimeInMillis() + "--" +se.getDateCreated().getTimeInMillis());
		logger.debug(" se actualiza por que la diferencias es  "+ diff);
		int diff2=(int) (diff/1000); //en segundos
		diff2=diff2/60; //valor en minutos
		logger.debug(" se actualiza por que la diferencias es  "+ diff2);
		if(diff2>se.getExpire()) //se.getExpire()esta en minutos
		{
			DAOSession daose=new DAOSession();
			try 
			{
			daose.updateEndSession(se);
			}catch (PercistenExcepcion e)
			{
				BussinesExcepcion bus= new BussinesExcepcion(e);
				bus.setCodigo(Constans.ERROR.ERROR_SESSION_TERMINADA.getCodigo());
				bus.setMensaje(Constans.ERROR.ERROR_SESSION_TERMINADA.getMensaje());
			}
			
			BussinesExcepcion bus= new BussinesExcepcion();
			bus.setCodigo(Constans.ERROR.ERROR_SESSION_TERMINADA.getCodigo());
			bus.setMensaje(Constans.ERROR.ERROR_SESSION_TERMINADA.getMensaje());
			throw bus;
		}
		
		
	
		
	}



	


	
	
	
	
	
	
	

}
