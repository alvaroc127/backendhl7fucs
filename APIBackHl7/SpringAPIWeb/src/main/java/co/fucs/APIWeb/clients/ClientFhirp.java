package co.fucs.APIWeb.clients;




import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.Condition;
import org.hl7.fhir.r4.model.Condition.ConditionEvidenceComponent;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.SampledData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.pojo.Signal;
import com.fucs.commo.web.pojo.Alertas;
import com.fucs.commo.web.rosource.ResourceInternal;
import com.fucs.commo.web.util.Constans;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.TokenClientParam;
import co.fucs.APIWeb.util.exception.Util;


public class ClientFhirp 
{
	
	
	
	private static final Logger logger = LoggerFactory.getLogger(ClientFhirp.class);
	
	public ClientFhirp() {
		// TODO Auto-generated constructor stub
	}
	
	
	public java.util.List<Signal> consultarSignals(String id,java.util.Calendar date)
	throws BussinesExcepcion,Exception
	{
		logger.debug("se inicia la consulta de signal");
		TokenClientParam identif=new TokenClientParam(Observation.SP_IDENTIFIER);
		TokenClientParam datetoken=new TokenClientParam(Observation.SP_DATE);
		TokenClientParam datetokenend=new TokenClientParam(Observation.SP_DATE);
		java.util.List<Signal> signals=new java.util.ArrayList<>();
		
		logger.debug("se trae la base del url ");
		String out=ResourceInternal.
	    getInstance().getProperty(Constans.KEY_BEAN,Constans.LOCALBASEURL);
		IGenericClient clin=FhirContext.forR4().newRestfulGenericClient(out);
		logger.debug("se trae la base del url "+out);
		 Bundle respon=clin.search().forResource(Observation.class)
		.where(identif.exactly().code(id))
		.and(datetoken.exactly().code("gt"+Util.getDateInString(date)))
		.and(datetokenend.exactly().code("lt"+Util.getDateInString(date)))
		.returnBundle(Bundle.class)
		.execute();
		/* try
		 {
		 IParser par=FhirContext.forR4().newJsonParser();
		 String ou1t=par.setPrettyPrint(true).encodeResourceToString(respon);
		 logger.debug("la salida es la siguiente "+ou1t);
		 }catch (Exception e) 
		 {
			 logger.debug("error en la excepcion ",e);
		 }*/
		 if(null == respon || null == respon.getEntry())
		 {
			 
			 BussinesExcepcion bus=new BussinesExcepcion();
			 bus.setCodigo(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getCodigo());
			 bus.setMensaje(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getMensaje());
			 throw bus;
		 }
		 if(respon.getEntry().isEmpty())
		 {
			 
			 BussinesExcepcion bus=new BussinesExcepcion();
			 bus.setCodigo(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getCodigo());
			 bus.setMensaje(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getMensaje());
			 throw bus;
		 }
		 for(BundleEntryComponent com:respon.getEntry()) 
		 {
			 if(com.getResource() instanceof Observation) 
			 {
				 Observation obser=(Observation) com.getResource();
				 Signal sig=new Signal();
				 sig.setObservation(obser);
				 signals.add(sig);
			 }
			 
		 }
		 return signals;
	}
	
	
	
	public java.util.List<Alertas> consultarAlarms(String id,java.util.Calendar date)
			throws BussinesExcepcion,Exception
			{
				logger.debug("se inicia la consulta de Alertas");
				TokenClientParam identif=new TokenClientParam(Observation.SP_IDENTIFIER);
				TokenClientParam datetoken=new TokenClientParam(Observation.SP_DATE);
				TokenClientParam datetokenend=new TokenClientParam(Observation.SP_DATE);
				java.util.List<Alertas> signals=new java.util.ArrayList<>();
				Integer cont=1;
				
				logger.debug("se trae la base del url ");
				String out=ResourceInternal.
			    getInstance().getProperty(Constans.KEY_BEAN,Constans.LOCALBASEURL);
				IGenericClient clin=FhirContext.forR4().newRestfulGenericClient(out);
				logger.debug("se trae la base del url "+out);
				 Bundle respon=clin.search().forResource(Condition.class)
				.where(identif.exactly().code(id))
				.and(datetoken.exactly().code("gt"+Util.getDateInString(date)))
				.and(datetokenend.exactly().code("lt"+Util.getDateInString(date)))
				.returnBundle(Bundle.class)
				.execute();
				/* try
				 {
				 IParser par=FhirContext.forR4().newJsonParser();
				 String ou1t=par.setPrettyPrint(true).encodeResourceToString(respon);
				 logger.debug("la salida es la siguiente "+ou1t);
				 }catch (Exception e) 
				 {
					 logger.debug("error en la excepcion ",e);
				 }*/
				 if(null == respon || null == respon.getEntry())
				 {
					 
					 BussinesExcepcion bus=new BussinesExcepcion();
					 bus.setCodigo(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getCodigo());
					 bus.setMensaje(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getMensaje());
					 throw bus;
				 }
				 if(respon.getEntry().isEmpty())
				 {
					 
					 BussinesExcepcion bus=new BussinesExcepcion();
					 bus.setCodigo(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getCodigo());
					 bus.setMensaje(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getMensaje());
					 throw bus;
				 }
				 for(BundleEntryComponent com:respon.getEntry()) 
				 {
					 if(com.getResource() instanceof Condition) 
					 {
						 Condition obser=(Condition) com.getResource();
						 Alertas alrt=new Alertas();
						 java.util.List<ConditionEvidenceComponent> listevi=obser.getEvidence();
						 cont=1;
						 for(ConditionEvidenceComponent condien:listevi) 
						 {
							java.util.List<Extension> extend=condien.getExtension();
							 for(Extension ex:extend) 
							 {
								 SampledData sam=(SampledData)ex.getValue();
								 logger.debug("valor cont"+cont);
								 logger.debug("valor cont"+sam.getData());
								 switch (cont) 
								 {
								 	case 1: 
								 	{ 
								 	 alrt.setDescripcion(sam.getData());
								 	 cont++;
								 	}
								 	break;
								 	case 2: 
								 	{ 
								 		alrt.setHoraAlerta(sam.getData());
									   cont++;
								 	}
								 	break;
								 	case 3:
								 	{ 
									 alrt.setSeveridad(Integer.parseInt(sam.getData()));
									 cont++;
								 	}
								 	break;
								} 
							 }
						 }
						 signals.add(alrt);
					 }
					 
				 }
				 return signals;
			}

}
