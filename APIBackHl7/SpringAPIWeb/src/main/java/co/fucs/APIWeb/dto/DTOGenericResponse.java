package co.fucs.APIWeb.dto;

public class DTOGenericResponse extends DTOBase
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6042131597871996813L;

	private String  resum;
	
	private String message;
	
	
	public DTOGenericResponse() 
	{
		// TODO Auto-generated constructor stub
	}


	public String getResum() {
		return resum;
	}


	public void setResum(String resum) {
		this.resum = resum;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}
	


}
