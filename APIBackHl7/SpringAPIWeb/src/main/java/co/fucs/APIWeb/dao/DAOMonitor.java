package co.fucs.APIWeb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fucs.commo.web.excepcion.PercistenExcepcion;
import com.fucs.commo.web.util.Constans;

public class DAOMonitor extends BaseDAO 
{

	private static final Logger logger = LoggerFactory.getLogger(DAOMonitor.class);
	
	private static String FINMONBYIP=
			"SELECT m.ID ,m.IP FROM dbo.Monitor m " + 
			"WHERE m.IP  = ? ";
	
	
	private static String FINMONBYIPALL=
			" SELECT m.ID ,m.IP FROM dbo.Monitor m  " +
			" WHERE m.ID  NOT IN  " +
			" (SELECT DISTINCT  u.ID  FROM dbo.USUARIOXMONITOR u  " +
			" INNER JOIN USUARIO u2  ON (u2.ID_USU  = u.ID_USU ) " +
			" INNER JOIN Monitor m2  ON (m2.ID  = u.ID ) " +
			" ) ";
			
	
	private static String BINDMONITORUSER=
			"INSERT INTO dbo.USUARIOXMONITOR (ID ,ID_USU ) VALUES (?,?) ";
	
	
	private static String FINDBINDUSERMON=
			" SELECT u.ID , u.ID_USU  FROM dbo.USUARIOXMONITOR u "+
			" INNER JOIN USUARIO u2  ON (u2.ID_USU  = u.ID_USU ) " +
			" INNER JOIN Monitor m2  ON (m2.ID  = u.ID ) " +
			" WHERE  U.ID_USU = ? ";
	
	private static String deletebindmonuser=
			" DELETE FROM dbo.USUARIOXMONITOR  "+
			" WHERE ID = ? and ID_USU = ?  ";
	
	
	
	public DAOMonitor() {
		// TODO Auto-generated constructor stub
	}
	
	
	

	public  Integer  findUserBind(Integer idUsua)
	throws PercistenExcepcion
	{
		logger.debug("se inicia la consulta del monitor");
		Connection con=this.getConection(Constans.FUCSCONECT);
		PreparedStatement stat=null;
		ResultSet resul=null;
		Integer idmon=null;
		try 
		{
			stat=con.prepareStatement(FINDBINDUSERMON);
			stat.setInt(1, idUsua);
			resul=stat.executeQuery();
			while(resul.next()) 
			{
				idmon=resul.getInt("ID");
			}
			return idmon;
		}catch (Exception e) 
		{
			logger.debug(" algo salio mal ", e);
			PercistenExcepcion perx=new PercistenExcepcion(e);
			perx.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			perx.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw perx;
		}finally 
		{
			this.close(con);
			this.close(stat);
			this.close(resul);			
		}
	}
	
	
	
	
	public boolean unBindMontUser(Integer idmon,Integer idusuario) 
			throws PercistenExcepcion
			{
				logger.debug("se inicia la desvinculacion del monitor");
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				try 
				{
					stat=con.prepareStatement(deletebindmonuser);
					stat.setInt(1,idmon);
					stat.setInt(2,idusuario);
					return stat.executeUpdate()>0?true:false;
				}catch (Exception e) 
				{
					logger.debug(" algo salio mal ", e);
					PercistenExcepcion perx=new PercistenExcepcion(e);
					perx.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
					perx.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
					throw perx;
				
				}finally 
				{
					this.close(con);
					this.close(stat);
				}
				
			}
	
	
	
	
	
	public boolean createBindMontUser(Integer idmon,Integer idUsu) 
	throws PercistenExcepcion
	{
		logger.debug("se inicia la vincula cion del monitor");
		Connection con=this.getConection(Constans.FUCSCONECT);
		PreparedStatement stat=null;
		try 
		{
			stat=con.prepareStatement(BINDMONITORUSER);
			stat.setInt(1,idmon);
			stat.setInt(2,idUsu);
			return stat.executeUpdate()>0?true:false;
		}catch (Exception e) 
		{
			logger.debug(" algo salio mal ", e);
			PercistenExcepcion perx=new PercistenExcepcion(e);
			perx.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			perx.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw perx;
		
		}finally 
		{
			this.close(con);
			this.close(stat);
		}
		
	}
	
	
	public  java.util.Map<String,Integer> findMonitorall()
			throws PercistenExcepcion
			{
				logger.debug("se inicia la consulta del monitor");
				Connection con=this.getConection(Constans.FUCSCONECT);
				PreparedStatement stat=null;
				ResultSet resul=null;
				java.util.Map<String,Integer> monitores=new java.util.Hashtable<String, Integer>();
				try 
				{
					stat=con.prepareStatement(FINMONBYIPALL);
					resul=stat.executeQuery();
					while(resul.next()) 
					{
						monitores.put(resul.getString("IP"), resul.getInt("ID"));
					}
					return monitores;
				}catch (Exception e) 
				{
					logger.debug(" algo salio mal ", e);
					PercistenExcepcion perx=new PercistenExcepcion(e);
					perx.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
					perx.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
					throw perx;
				}finally 
				{
					this.close(con);
					this.close(stat);
					this.close(resul);			
				}
			}
	
	
	
	public  java.util.Map<String,Integer> findMonitor(String ip)
	throws PercistenExcepcion
	{
		logger.debug("se inicia la consulta del monitor");
		Connection con=this.getConection(Constans.FUCSCONECT);
		PreparedStatement stat=null;
		ResultSet resul=null;
		java.util.Map<String,Integer> monitores=new java.util.Hashtable<String, Integer>();
		try 
		{
			stat=con.prepareStatement(FINMONBYIP);
			stat.setString(1, ip);
			resul=stat.executeQuery();
			while(resul.next()) 
			{
				monitores.put(resul.getString("IP"), resul.getInt("ID"));
			}
			return monitores;
		}catch (Exception e) 
		{
			logger.debug(" algo salio mal ", e);
			PercistenExcepcion perx=new PercistenExcepcion(e);
			perx.setCodigo(Constans.ERROR.ERROR_BD.getCodigo());
			perx.setMensaje(Constans.ERROR.ERROR_BD.getMensaje());
			throw perx;
		}finally 
		{
			this.close(con);
			this.close(stat);
			this.close(resul);			
		}
	}

}
