package co.fucs.APIWeb.security.client;

import static org.junit.Assert.fail;

import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.junit.Test;

import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.pojo.Signal;
import com.fucs.commo.web.rosource.ResourceInternal;
import com.fucs.commo.web.util.Constans;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.TokenClientParam;
import co.fucs.APIWeb.clients.ClientFhirp;
import co.fucs.APIWeb.util.exception.Util;

public class ClienteTestFhrip 
{
	
	
	
	
	@Test
	public void testClientFhrip() 
	{

		String date="2021-04-01T15:36:14";
		
		
		
		System.out.println("se inicia la consulta de signal");
		TokenClientParam identif=new TokenClientParam(Observation.SP_IDENTIFIER);
		TokenClientParam datetoken=new TokenClientParam(Observation.SP_DATE);
		TokenClientParam datetokenend=new TokenClientParam(Observation.SP_DATE);
		java.util.List<Signal> signals=new java.util.ArrayList<>();
		Bundle respon=null;
		
		System.out.println("se trae la base del url ");
		//String out=ResourceInternal.
	    //getInstance().getProperty(Constans.KEY_BEAN,Constans.LOCALBASEURL);
		IGenericClient clin=FhirContext.forR4().newRestfulGenericClient("http://localhost:9090/APIBackHl7-web");
		System.out.println("se trae la base del url "+"http://localhost:9090/APIBackHl7-web");
		
		try 
		{
			System.out.println("se enviara la fecha "+  Util.getDateTimeInString(date));
		 respon=clin.search().forResource(Observation.class)
		.where(identif.exactly().code("4"))
		.and(datetoken.exactly().code("gt"+Util.getDateTimeInString(date)))
		.and(datetokenend.exactly().code("lt"+Util.getDateTimeInString(date)))
		.returnBundle(Bundle.class)
		.execute();
		}catch (Exception e) 
		{
			e.printStackTrace();
			// TODO: handle exception
		}
		 
		 try
		 {
		 IParser par=FhirContext.forR4().newJsonParser();
		 String ou1t=par.setPrettyPrint(true).encodeResourceToString(respon);
		 System.out.println("la salida es la siguiente "+ou1t);
		 }catch (Exception e) 
		 {
			 e.printStackTrace();
			 System.out.println("error en la excepcion ");
		 }
		 if(null == respon || null == respon.getEntry())
		 {
			 
			 BussinesExcepcion bus=new BussinesExcepcion();
			 bus.setCodigo(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getCodigo());
			 bus.setMensaje(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getMensaje());
			 System.out.println("error en la excepcion ");
			 fail();
		 }
		 if(respon.getEntry().isEmpty())
		 {
			 
			 BussinesExcepcion bus=new BussinesExcepcion();
			 bus.setCodigo(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getCodigo());
			 bus.setMensaje(Constans.ERROR.ERROR_CLIENTE_INTEGRA.getMensaje());
			 System.out.println("error en la excepcion ");
			 fail();
		 }
		 for(BundleEntryComponent com:respon.getEntry()) 
		 {
			 if(com.getResource() instanceof Observation) 
			 {
				 Observation obser=(Observation) com.getResource();
				 Signal sig=new Signal();
				 sig.setObservation(obser);
				 signals.add(sig);
			 }
			 
		 }
		try 
		{
		IParser par=FhirContext.forR4().newJsonParser();
		for(Signal sig:signals) 
		{
			System.out.println(par.encodeResourceToString(sig.getObservation()));
		}
		
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}

}
