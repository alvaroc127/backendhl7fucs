package co.fucs.APIWeb.services;

import static org.junit.Assert.*;

import java.util.Base64;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fucs.commo.web.excepcion.BussinesExcepcion;
import com.fucs.commo.web.util.Constans;

public class TestEncrypting {
	private static String cad="";
	private static String secretkey=""; 

	@Before
	public void setUp() throws Exception 
	{
		 cad="b+Eh1VX0vZmnd/jybB8zmw==FFF_FFF_FFFVB5MAQcL8wogZDlpsYBXeA==";
		 secretkey="F0C5C1FR3DK3Y123";
	}

	@After
	public void tearDown() throws Exception {
	}

	
	public void testDecrytp()
	{

		String varVect;
		int i=0;
		String[] cifredvec=new String[2];
		Scanner scan=new Scanner(cad);
		scan.useDelimiter("F{3}_F{3}_F{3}");
		try 
		{
			while(scan.hasNext() && i< 2) 
			{
				cifredvec[i]=scan.next();
				System.out.println("se desifrara el"+cifredvec[i]);
				i++;
			}
			scan.close();
			
		}catch (Exception e) 
		{
			BussinesExcepcion buss=new BussinesExcepcion(e);
			buss.setCodigo(Constans.ERROR.ERROR_BUSSINES.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_BUSSINES.getMensaje());
			e.printStackTrace();
		}
		cad=cifredvec[0];
		varVect=cifredvec[1];
		System.out.println("se desifrara "+cad);
		System.out.println("se desifrara "+varVect);
		
		byte[]keyrand=Base64.getDecoder().decode(varVect);
		byte[]crypvar=Base64.getDecoder().decode(cad);
		byte[] secretk=secretkey.getBytes();
		SecretKeySpec secrepKey=new SecretKeySpec(secretk,"AES");
		IvParameterSpec randVec=new IvParameterSpec(keyrand);
		try 
		{
			Cipher cip=Cipher.getInstance("AES/CBC/PKCS5Padding");
			cip.init(Cipher.DECRYPT_MODE,secrepKey,randVec);
			byte[] decrip=new byte[cip.getOutputSize(crypvar.length)];
			int descrypsize=cip.update(crypvar, 0, crypvar.length, decrip, 0);
			descrypsize+=cip.doFinal(decrip, descrypsize);
			System.out.println("la cadena desicfrada"+new  String(decrip));
		}catch (Exception e) 
		{
			BussinesExcepcion buss=new BussinesExcepcion(e);
			buss.setCodigo(Constans.ERROR.ERROR_BUSSINES.getCodigo());
			buss.setMensaje(Constans.ERROR.ERROR_BUSSINES.getMensaje());
			e.printStackTrace();
		}
		
	
	}

}
