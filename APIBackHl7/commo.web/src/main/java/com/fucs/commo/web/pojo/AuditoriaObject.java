package com.fucs.commo.web.pojo;

import java.util.Calendar;

public class AuditoriaObject 
{
	
	
	private String url;
	
	private Calendar date;
	
	private String ip;
	
	private String evento1;
	
	private String evento2;
	
	private String eventUsu;
	
	public AuditoriaObject() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Calendar getDate() {
		return date;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}


	
	

	public String getEvento1() {
		return evento1;
	}



	public void setEvento1(String evento1) {
		this.evento1 = evento1;
	}



	public String getEvento2() {
		return evento2;
	}



	public void setEvento2(String evento2) {
		this.evento2 = evento2;
	}



	public String getEventUsu() {
		return eventUsu;
	}



	public void setEventUsu(String eventUsu) {
		this.eventUsu = eventUsu;
	}



	public AuditoriaObject(String url, Calendar date, String ip) {
		super();
		this.url = url;
		this.date = date;
		this.ip = ip;
	}



	public AuditoriaObject(String url, Calendar date, String ip, String evento1, String evento2, String eventUsu) {
		super();
		this.url = url;
		this.date = date;
		this.ip = ip;
		this.evento1 = evento1;
		this.evento2 = evento2;
		this.eventUsu = eventUsu;
	}
	

}
