package com.fucs.commo.web.pojo;

import java.io.Serializable;

public class Alertas extends ECG implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6667086107999139871L;
	
	private String horaAlerta;
	
	private Integer severidad;
	
	private String descripcion;
	
	private Integer idalert;

	public String getHoraAlerta() {
		return horaAlerta;
	}

	public void setHoraAlerta(String horaAlerta) {
		this.horaAlerta = horaAlerta;
	}

	public Integer getSeveridad() {
		return severidad;
	}

	public void setSeveridad(Integer severidad) {
		this.severidad = severidad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getIdalert() {
		return idalert;
	}

	public void setIdalert(Integer idalert) {
		this.idalert = idalert;
	}



}
