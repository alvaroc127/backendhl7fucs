package com.fucs.commo.web.pojo;

import java.io.Serializable;

import org.hl7.fhir.r4.model.Observation;





public class Signal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9068910751406361721L;
	
	private Observation observation;
	
	
	
	public Signal() {
		// TODO Auto-generated constructor stub
	}



	public Observation getObservation() {
		return observation;
	}



	public void setObservation(Observation observation) {
		this.observation = observation;
	}



	public Signal(Observation observation) {
		super();
		this.observation = observation;
	}
	
	

}
