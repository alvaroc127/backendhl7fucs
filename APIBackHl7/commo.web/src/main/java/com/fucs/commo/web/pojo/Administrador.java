package com.fucs.commo.web.pojo;

import java.io.Serializable;

import org.hl7.fhir.r4.model.Practitioner;

public class Administrador extends Usuario implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1958970651605244760L;
	private Practitioner adminfhir;
	
	public Practitioner getAdminfhir() {
		return adminfhir;
	}
	
	public void setAdminfhir(Practitioner adminfhir) {
		this.adminfhir = adminfhir;
	}
	
	
	
	public Administrador() {
		// TODO Auto-generated constructor stub
	}

	public Administrador(Practitioner adminfhir) {
		super();
		this.adminfhir = adminfhir;
	}
	

}
