package com.fucs.commo.web.pojo;

public class Sesion 
{
	
	private Integer idsesion;
	
	private Integer idUsuario;
	
	private java.util.Calendar dateCreated;
	
	private Integer status;
	
	private String accestoken;
	
	private java.util.Calendar dateEnd;
	
	private Integer expire;
	
	private Usuario us;
	
	
	
	public Sesion() 
	{
		
	}



	public Integer getIdsesion() {
		return idsesion;
	}



	public void setIdsesion(Integer idsesion) {
		this.idsesion = idsesion;
	}



	public Integer getIdUsuario() {
		return idUsuario;
	}



	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}



	public java.util.Calendar getDateCreated() {
		return dateCreated;
	}



	public void setDateCreated(java.util.Calendar dateCreated) {
		this.dateCreated = dateCreated;
	}



	public Integer getStatus() {
		return status;
	}



	public void setStatus(Integer status) {
		this.status = status;
	}



	public String getAccestoken() {
		return accestoken;
	}



	public void setAccestoken(String accestoken) {
		this.accestoken = accestoken;
	}



	public java.util.Calendar getDateEnd() {
		return dateEnd;
	}



	public void setDateEnd(java.util.Calendar dateEnd) {
		this.dateEnd = dateEnd;
	}



	public Usuario getUs() {
		return us;
	}



	public void setUs(Usuario us) {
		this.us = us;
	}



	public Integer getExpire() {
		return expire;
	}



	public void setExpire(Integer expire) {
		this.expire = expire;
	}
	

	

}
