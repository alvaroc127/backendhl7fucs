package com.fucs.commo.web.excepcion;

public class PercistenExcepcion extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7736918748090481746L;
	
	
	
	private String mensaje;
	private Integer codigo;
	
	
	public PercistenExcepcion() {
		// TODO Auto-generated constructor stub
		
	}
	
	public PercistenExcepcion(Throwable exp) 
	{
		super(exp);
	}


	public PercistenExcepcion(String mensaje, Integer codigo) {
		super();
		this.mensaje = mensaje;
		this.codigo = codigo;
	}
	
	
	public PercistenExcepcion(Integer codigo) {
		super();
		this.codigo = codigo;
	}


	public String getMensaje() {
		return mensaje;
	}


	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	public Integer getCodigo() {
		return codigo;
	}


	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

}
