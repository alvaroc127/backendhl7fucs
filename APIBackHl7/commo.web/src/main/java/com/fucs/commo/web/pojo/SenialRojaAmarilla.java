package com.fucs.commo.web.pojo;

import java.io.Serializable;

public class SenialRojaAmarilla extends Signal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7797571050340204965L;
	
	private Integer tipoSignal;

	public Integer getTipoSignal() {
		return tipoSignal;
	}

	public void setTipoSignal(Integer tipoSignal) {
		this.tipoSignal = tipoSignal;
	}
	
	

}
