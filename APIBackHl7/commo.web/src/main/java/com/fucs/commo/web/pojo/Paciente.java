package com.fucs.commo.web.pojo;

import java.io.Serializable;

import org.hl7.fhir.r4.model.Patient;

public class Paciente extends Usuario implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1349137507558050502L;

	private Patient pacientfhir;
	
	private java.util.List<Signal>signals;

	public Patient getPacientfhir() {
		return pacientfhir;
	}

	public void setPacientfhir(Patient pacientfhir) {
		this.pacientfhir = pacientfhir;
	}

	public Paciente(Patient pacientfhir) {
		super();
		this.pacientfhir = pacientfhir;
	}
	

	
	public java.util.List<Signal> getSignals() {
		return signals;
	}

	public void setSignals(java.util.List<Signal> signals) {
		this.signals = signals;
	}

	public Paciente() {
		// TODO Auto-generated constructor stub
	}
	

}
