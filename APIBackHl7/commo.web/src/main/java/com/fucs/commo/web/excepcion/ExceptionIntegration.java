package com.fucs.commo.web.excepcion;

public class ExceptionIntegration  extends Exception
{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2621294118048785873L;
	private String mensaje;
	private Integer codigo;
	
	public ExceptionIntegration() {
		// TODO Auto-generated constructor stub
	}
	
	
	public ExceptionIntegration(Throwable esp) {
		super(esp);
		// TODO Auto-generated constructor stub
	}
	

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	

}
