package com.fucs.commo.web.rosource;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fucs.commo.web.util.Constans;

public class ResourceInternal implements Serializable
{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4425164401980821434L;
	private  static ResourceInternal resInt=null;

	
	public static Logger log=LoggerFactory.getLogger(ResourceInternal.class); 
	
	private ResourceInternal() 
	{
		if(null==propsBean ||  null == propsBase || null == propsWeb) 
		{
			ResourceBundle resb=ResourceBundle.getBundle(Constans.BUNDLEFILE);
			Enumeration<String> keys= resb.getKeys();
			while(keys.hasMoreElements()) 
			{
				String key= keys.nextElement();
				log.debug("se lee la llave "+key);
				String value=resb.getString(key);
				try 
				{
				LoadProperties(key, value);
				}catch (IOException e) 
				{
					log.debug("este es el error ", e);
					log.debug("un error cargando las propiedades de %s ",key);
				}
			}
		}
		
	}
	
	
	private void LoadProperties ( final String  key,String value)
	throws IOException
	{
		log.debug("inicia la carga de  la propiedad  %s ",key);
		
		switch (key) 
		{
		case Constans.KEY_BASE:
			this.propsBase=new Properties();
			this.propsBase.load(new FileInputStream(new File(value)));
			map.put(Constans.KEY_BASE, this.propsBase);
		break;
			
		case Constans.KEY_BEAN:
			this.propsBean=new Properties();
			this.propsBean.load(new FileInputStream(new File(value)));
			map.put(Constans.KEY_BEAN, this.propsBean);
		break;
			
			
		case Constans.KEY_WEB:
			this.propsWeb=new Properties();
			this.propsWeb.load(new FileInputStream(new File(value)));
			map.put(Constans.KEY_WEB, this.propsWeb);
		break;
		

		default:
			this.propsWeb=new Properties();
			this.propsWeb.load(new FileInputStream(new File(value)));
			map.put(Constans.KEY_WEB, this.propsWeb);
		break;
		}
		
		
	}
	
	
	
	private Properties propsBean;
	private Properties propsBase;
	private Properties propsWeb;
	private Map<String,Properties>map=new HashMap<String,Properties>();
	
	
	
	
	
	public String getProperty(String source,String key) 
	{
		return (String)((Properties)map.get(source)).get(key);
		
	}
	
	public static ResourceInternal getInstance() 
	{
		if(null== resInt) 
		{
			resInt=new ResourceInternal();
		}
		return resInt;
	}
	


}
