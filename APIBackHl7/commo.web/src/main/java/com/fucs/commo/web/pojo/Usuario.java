package com.fucs.commo.web.pojo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Hashtable;



public class Usuario implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8264351714758998852L;
	
	protected Long idusu;
	protected String email; 
	protected Integer genero;
	protected java.util.Date fechaNacimiento;
	protected String direccion; 
	protected String numeroCel;
	protected String password;
	protected Integer status;
	protected Calendar dateCreated;
	protected Calendar dateUpdate;
	protected TipoDocumento tipoDoc;
	protected String numerodoc;
	protected String name;

	protected java.util.Map<Integer,Rol> roles;
	
	
	
	
	
	public String getName() 
	{
		return name;
	}





	public void setName(String name) {
		this.name = name;
	}





	public String getNumerodoc() {
		return numerodoc;
	}





	public void setNumerodoc(String numerodoc) {
		this.numerodoc = numerodoc;
	}





	public Long getIdusu() {
		return idusu;
	}





	public void setIdusu(Long idusu) {
		this.idusu = idusu;
	}





	public String getEmail() {
		return email;
	}





	public void setEmail(String email) {
		this.email = email;
	}





	public Integer getGenero() {
		return genero;
	}





	public void setGenero(Integer genero) {
		this.genero = genero;
	}





	public java.util.Date getFechaNacimiento() {
		return fechaNacimiento;
	}





	public void setFechaNacimiento(java.util.Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}





	public String getDireccion() {
		return direccion;
	}





	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}





	public String getNumeroCel() {
		return numeroCel;
	}





	public void setNumeroCel(String numeroCel) {
		this.numeroCel = numeroCel;
	}





	public String getPassword() {
		return password;
	}





	public void setPassword(String password) {
		this.password = password;
	}





	public Integer getStatus() {
		return status;
	}





	public void setStatus(Integer status) {
		this.status = status;
	}





	public Calendar getDateCreated() {
		return dateCreated;
	}





	public void setDateCreated(Calendar dateCreated) {
		this.dateCreated = dateCreated;
	}





	public Calendar getDateUpdate() {
		return dateUpdate;
	}





	public void setDateUpdate(Calendar dateUpdate) {
		this.dateUpdate = dateUpdate;
	}





	public TipoDocumento getTipoDoc() {
		return tipoDoc;
	}





	public void setTipoDoc(TipoDocumento tipoDoc) {
		this.tipoDoc = tipoDoc;
	}





	public java.util.Map<Integer,Rol> getRoles() {
		return roles;
	}





	public void setRoles(java.util.Map<Integer,Rol> roles) {
		this.roles = roles;
	}


	public void usuarioaddRol(Integer i, Rol rol) 
	{
		this.roles.put(i, rol);
	}



	public Usuario() {
		roles=new Hashtable<Integer, Rol>();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
