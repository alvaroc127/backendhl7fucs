package com.fucs.commo.web.util;

public final class Constans 
{
	
	
	public static final String BUNDLEFILE="APIproperties";
	
	public static final String KEY_BASE="propertiesBase";
	public static final String KEY_BEAN="propertiesBean";
	public static final String KEY_WEB="propertiesWeb";
	
	
	
	public static final String KEY_BASE_JNDI_DEFAULT="jdbclookup";
	public static final String KEY_SECRETCIFRED="cifredkey";
	
	public static final Integer FUCSCONECT=1;
	public static final Integer DEFAULTCONECT=2;
	
	public static final String CONST_CORS_QA="http://127.0.0.1:8080";
	//public static final String CONST_CORS_PROD="https://fucs.sdsklsdsd";
	
	public static final String VALID_SOURCES1="ANG-S001";
	public static final String VALID_SOURCES2="ANG-S002";
	
	public static final String SCOPE="fucslogin";
	
	public static final String URLBASEKEYCLOACK="httpkeycloack";
	public static final String URLCERTIFKEYS="httpkeyscertiend";
	
	public static final String INTERKEY="NOW_ALLOWED";
	
	public static final String DEFAULTROL="DEFAULT";
	
	public static final String JMSCONFACT="connect.Fact";
	
	public static final String JMSQUEUEALER="jms.queue.alert";
	
	public static final String LOCALBASEURL="http.local.base.url";
	
	public static final String USERJMS="user.jms";
	
	public static final String USERJMSPASS="user.jms.password";
	
	public static final String ACTUALIZARECPTOR="user.receptor.number";
	
	
	public static final String BASECLIENTOBSE="http.observation.client";
	
	public static final String FIREBASEKEY="fire.base.key";
	
	public static final String COLLAPKEY="fucsmss";
	
	public static final String TOPICSFUCS="fucstopic";
	
	


	
	
	public enum ERROR 
	{
		ERROR_BD(-10,"error interno"),
		ERROR_BUSSINES(-50,"error en la validación negocio"),
		ERROR_SESSION_INVALIDA(-40,"error en la validación negocio"),
		ERROR_SESSION_TERMINADA(-60,"error sesion finalizada"),
		ERROR_AUTENTICACION(-80,"Autenticacion fallida"),
		ERROR_DATOS_INCORRECTOS(-150,"Datos Incorrectos accion fallida"),
		ERROR_GENERAL_NEGOCIO(500,"ERROR"),
		ERROR_SESSION_ACTIVA(-90,"la sesion ya esta iniciada"),
		ERROR_NEGOCION_CONFIG(-30,"La configuracio no es valida"),
		ERROR_ALERTA_NOCONFIG(-300,"La Alerta  no se puede almacenar"),
		ERROR_CLIENTE_INTEGRA(-150,"el cliente no pude traer informacion"),
		ERROR_CONFIG_NOLOAD(-180,"No se pudo cargar la configuracion"),
		ERROR_PAG_NO_FOUND(-404,"la pagina no se encontro"),
		ERROR_PAG_NO_AUT(-403,"la pagina no es AUT"),
		ERROR_FUENTE_INVALIDA(-501,"la fuente es invalida");
		
		
		private Integer codigo;
		private String mensaje;
		
		
		private ERROR(Integer codigo, String mensaje) 
		{
			this.codigo=codigo;
			this.mensaje=mensaje;
		}
		
		
		public Integer getCodigo() {
			return codigo;
		}


		public void setCodigo(Integer codigo) {
			this.codigo = codigo;
		}


		public String getMensaje() {
			return mensaje;
		}


		public void setMensaje(String mensaje) {
			this.mensaje = mensaje;
		}
		
	}
	

}
