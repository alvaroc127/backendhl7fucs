package com.fucs.commo.web.excepcion;

public class BussinesExcepcion extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5224196354803380384L;
	
	private String mensaje;
	private Integer codigo;
	
	
	public BussinesExcepcion() {
		// TODO Auto-generated constructor stub
	}
	
	
	public BussinesExcepcion(Throwable ex) {
		super(ex);
	}
	
	public BussinesExcepcion(Throwable ex,String mensaje, Integer codigo) {
		super(ex);
		this.mensaje = mensaje;
		this.codigo = codigo;
	}
	
	public BussinesExcepcion(Throwable ex,Integer codigo)
	{
		super(ex);
		this.codigo = codigo;
	}
	
	


	public BussinesExcepcion(String mensaje, Integer codigo) {
		super();
		this.mensaje = mensaje;
		this.codigo = codigo;
	}
	
	
	public BussinesExcepcion(Integer codigo) {
		super();
		this.codigo = codigo;
	}


	public String getMensaje() {
		return mensaje;
	}


	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	public Integer getCodigo() {
		return codigo;
	}


	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	

}
