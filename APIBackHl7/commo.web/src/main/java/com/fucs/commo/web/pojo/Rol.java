package com.fucs.commo.web.pojo;

import java.util.Hashtable;

public class Rol 
{
	private Integer id;
	private Integer estado;
	private String descrip;
	
	private java.util.Map<Integer,Permiso> permisos;
	
	
	
	
	public Integer getId() {
		return id;
	}




	public void setId(Integer id) {
		this.id = id;
	}




	public Integer getEstado() {
		return estado;
	}




	public void setEstado(Integer estado) {
		this.estado = estado;
	}




	public String getDescrip() {
		return descrip;
	}




	public void setDescrip(String descrip) {
		this.descrip = descrip;
	}




	public java.util.Map<Integer,Permiso> getPermisos() {
		return permisos;
	}




	public void setPermisos(java.util.Map<Integer,Permiso> permisos) {
		this.permisos = permisos;
	}


	public void addPermis(Integer i, Permiso per) 
	{
		this.permisos.put(i, per);
	}


	public Rol() {
		this.permisos=new Hashtable<Integer, Permiso>();
		// TODO Auto-generated constructor stub
	}
}
